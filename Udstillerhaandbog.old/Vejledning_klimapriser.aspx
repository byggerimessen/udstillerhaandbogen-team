﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Vejledning_klimapriser.aspx.vb" Inherits="Udstillerhaandbog.Vejledning_klimapriser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vejledning til Klimapriser</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div> </div>
<div class="page">
<div class="sitemap">Forside - Afsnit 1 - Nominering til Klimapriser - Vejledning i ansøgning af klimapriser</div>
    <h2>Ansøgningsvejledning</h2>

    <br />
    <h4>Hvilken pris søges ?</h4>

Angiv som overskrift:
    <ol class="krav">
<li>Byggeriets Klimapris</li>
<li>Byggeriets Energipris</li>
<li>Byggeriets Miljøpris</li>
</ol>
<h4>Ved produkt eller løsning til byggeriet</h4>
    <ol class="overskrift">
<li>Giv en kort beskrivelse af egenskaberne ved din virksomheds produkt eller løsning</li>
<li>Giv en kort beskrivelse af, hvilke af de stillede krav dit produkt eller løsning opfylder</li>
<li>Dokumentér de krav, dit produkt eller løsning opfylder</li>
</ol>
<h4>Ved klima-, energi- eller miljøstrategi og/eller -indsats</h4>
    <ol class="overskrift">
<li>Giv en kort beskrivelse af din virksomhed</li> 
<li>Præsenter din virksomheds strategi og/eller indsats</li> 
<li> Giv en kort beskrivelse af opnåede resultater</li>
</ol>
<h4>Afsender af ansøgningen</h4>

Angiv firmanavn, kontaktperson, tlf.nr., e-mail-adresse og standnr.

<h4>Deadline: Senest 4. november 2013</h4>


<h4>NB! </h4>

Vi gør opmærksom på, at nominering til de tre priser kun er mulig for produkter og løsninger, der udstilles på BYGGERI ´14. 
    <br />
    Husk derfor at anføre standnr. på ansøgningen. De pågældende produkter og løsninger skal desuden være mindre end to år gamle.


    <br />
    <br />
    Skulle du have problemer med ansøgningen, er du velkommen til at kontakte Ingelise Alsø på <a href="http://">ia@danskebyggcentre.dk</a> eller 45 80 78 77.



    </div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <h1>BYGGERI'14</h1>
        <p >DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
