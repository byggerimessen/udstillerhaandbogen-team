﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Krav_nominering.aspx.vb" Inherits="Udstillerhaandbog.Krav_nominering" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Krav til nominering</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div> </div>
<div class="page">
<div class="sitemap">Forside - Afsnit 1 - Nominering til Klimapriser - Krav til nominering</div>
    
<h2>Krav til nominering</h2>


    <br />
    <h4><strong>Byggeriets Klimapris</strong></h4>
    <br />
    Klimaløsning / -teknologi, der opfylder et eller flere af nedenstående krav: 

    <br />
    <ul>
        <li>Dokumenteret markant nedsættelse af CO2-udslip ved anvendelse af produktet eller løsningen. </li>
    <li>Dokumenteret markant nedsættelse af CO2-udslip ved fremstillingen af produktet.</li>
</ul>
    Virksomheder, der:
    <br />
    yder en dokumenteret klimaindsats   og/eller
    <br />
    har iværksat en klimastrategi

    <br />
    <br />
    <h4><strong>Byggeriets Energipris</strong></h4>
    <br />
    Energibesparende bygningsdele og komponenter til byggeriet, der opfylder et eller flere af nedenstående krav:

    <br />
    <ul>
    <li>Dokumenteret energieffektive bygningskomponenter og løsninger.</li>
    <li>Dokumenterede markante energibesparelser (el- og varmeforbrug) ved anvendelse af produktet.</li>
    <li>Dokumenteret minimering af energiforbrug i byggeprocessen ved anvendelse af produktet. </li>
    <li>Benyttelse af alternativ/vedvarende energi ved fremstilling og/eller anvendelse af produktet.</li>
</ul>
    Virksomheder, der har:
    <br />
    en iværksat energistrategi

    <br />
    <br />
    <h4><strong>Byggeriets Miljøpris</strong></h4>
    <br />
    Dokumenteret bæredygtige produkter eller løsninger, der opfylder et eller flere af nedenstående krav:

    <br />
    <ul>
    <li>Produkter, der kan genanvendes eller er produceret af genanvendt materiale.</li>
    <li>Dokumenteret markant reduktion af produktets miljøbelastning ved produktion, anvendelse og bortskaffelse.</li>
    <li>Produkter, der bidrager til et bedre indeklima.</li>
    <li>Produkter, der bidrager til et bedre arbejdsmiljø. </li>
    </ul>
    Virksomheder, der har:
    <br />
    iværksat en miljøstrategi     og/eller
    <br />
    iværksat en arbejdsmiljøstrategi
    

</div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <h1>BYGGERI'14</h1>
        <p >DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
