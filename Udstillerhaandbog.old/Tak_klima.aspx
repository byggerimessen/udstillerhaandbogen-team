﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Tak_klima.aspx.vb" Inherits="Udstillerhaandbog.Tak_klima" %>

<%@ Register assembly="APNSoftControls" namespace="APNSoft.WebControls" tagprefix="CN" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tak for indberetningen af klimapris</title>
     <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
      <div>
         <div class="header">
            <div class="header-container">
                <div class="header-text">
        <h1 class="auto-style2">Tak for jeres indberetning</h1>
               </div>
                   <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
            </div>
           </div>
        <div class="red"></div> </div>
          

          <div class="page">
              <CN:APNSoftDataGrid ID="MyDatagrid" runat="server" Height="700px" PageSize="30" Width="1300px">
              </CN:APNSoftDataGrid>
        </div>
                 <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <h1>BYGGERI'14</h1>
        <p >DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>

          </div>
       

    </form>
</body>
</html>
