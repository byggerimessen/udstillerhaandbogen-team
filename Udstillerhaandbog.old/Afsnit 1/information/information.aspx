﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <title>Messehåndbogen</title>
    <link rel="stylesheet" href="../../Styles/Site.css" type="text/css" media="screen" />

</head>
<body>
<div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1>Udstillerhåndbogen</h1>
        <br/>Informationer og praktiske oplysninger
        </div>
    <img src="../../billeder/logo.png" width="240px" height="191px"/>
</div>
</div>
<div class="red"></div>

<!--search box-->
<div style="position:fixed;right:5%;margin-left:10px;margin-top:10px;font-size:0.9em;color:#000000; background-color:#fff">
Gå til følgende punkt <br />
<select name="url" onchange="document.location.href = this.value">
    <option value="#adgangskort">Adgangskort</option>
    <option value="#aflaesning">Aflæsning</option>
    <option value="#afskærmningAfMaskiner">Afskærmning Af Maskiner</option>
    <option value="#arbejdskort">Arbejdskort</option>
</select>
</div>

<div class="page">
<div class="sitemap">Forside : Afsnit 1 : Info om Byggeri'14 og MESSE C</div><br />
<button type="button" style="float:right;font-weight:bold;padding-left:7px;padding-right:7px">Udskriv <img style="vertical-align:middle;" src="../billeder/print.png"  width="35px"/></button>
<br/>

    <h1>
        Information og praktiske oplysninger om BYGGERI'14 og MESSE C
    </h1>
<div class=info>
<h3 id="adgangskort">ADGANGSKORT</h3>

Adgangskort til besøgende på messen kan rekvireres uden beregning og i ubegrænset antal af udstillere til levering efter den 9. januar 2012. Se bestillingsskema under afsnit 3.
Se også under ”Besøgende/Elektronisk registrering”.<br />

Vigtigt!
Benyt disse gratis adgangskort i alle udsendelser fra firmaet, lad sælgerne få en stak med på turen rundt til kunder og byggepladser, så du på alle måder gør opmærksom på dit firmas deltagelse på TUN BYG 2012 !! 
</div>
<div class=info>
<h3 id="aflaesning">AFLÆSNING</h3>

Der er porte i alle haller, som du kan køre direkte til. Der er ingen trin eller forhindringer, så du kan rulle alt materiel direkte ind på standen. <br />
Bemærk hvor portene er ind til den hal, din stand er placeret, så du kan køre direkte til porten tættest på din stand. Se oversigtsplan under afsnit 4, der også oplyser portenes højde og bredde. <br />
Se også ”Kørsel i messecentret”
</div>
<div class=info>
<h3>AFSKÆRMNING AF MASKINER</h3>
Udstillere er forpligtede til at overholde samtlige lovgivningsmæssige krav, herunder at forsyne de udstillede genstande (maskiner, anlæg og apparater af enhver art) med de i henhold til lovgivningen foreskrevne sikringsdele, uanset om det udstillede vises i drift eller ej. 
</div> 
<div class=info>
<h3> <a id="arbejdskort">ARBEJDSKORT </a></h3>

Der benyttes ikke arbejdskort i Fredericia Messecenter/MESSE C under opbygning og nedtagning.
Der benyttes Udstillerkort i selve messeperioden, som giver adgang til messen for standpersonale – se bestillingsskema i Afsnit 3. 
Retur til top 
</div> 
<div class=info>
ARRANGØRKONTOR

Arrangørkontoret er beliggende i indgangspartiet ved hovedindgangen. Se oversigtsplan under Afsnit 4. Her vil repræsentanter fra Trælasthandlerunionen - TUN stå til rådighed i hele messeperioden. 
Retur til top 

AV-UDSTYR

Du kan leje professionelt AV-udstyr i Fredericia Messecenter/MESSE C. Se skema 11 i afsnit 4. Kontakt Teknisk Afdeling på tlf. 75 92 25 66 og få et tilbud. 
Retur til top 

BESTILLINGSSKEMAER

Adgangskort, PR-materiale og optagelse i messekatalog m.m.
Bestillinger til TUN, herunder adgangskort og andet PR-materiale, samt optagelse i messekataloget – se afsnit 3 – bedes venligst afsendt til TUN: 

Snarest og SENEST 25. november 2011 

Ydelser i Fredericia Messecenter/MESSE C 
Samtlige bestillingsskemaer på serviceydelser i messecentret samt standskitse – afsnit 4 – bedes venligst udskrevet og returneret pr. post, fax eller indscannet og mailet til Fredericia Messecenter/MESSE C: 

Snarest og SENEST 10. februar 2012

NB! 
Vi gør venligst opmærksom på, at fakturaen pålægges 10% ved bestilling efter denne dato.
For ydelser, der først bestilles efter den officielle indrykningsdag, beregner messecentret sig et tillæg på 25% på alle tekniske ydelser. Se i øvrigt Fredericia Messecenters/MESSE C Generelle Regler i afsnit 4.  

For yderligere information om opbygning, teknik, messecentret, regler og standskitse se afsnit 4. 
Retur til top 

BESØGENDE/ELEKTRONISK REGISTRERING

På messens hjemmeside, www.tunbyg.dk, vil det primo januar 2012 være muligt for besøgende at registrere sig elektronisk og udskrive deres personlige adgangskort til messen. Besøgende, som er registreret elektronisk, vil have nem og hurtig adgang til messen via egne indgange. 

Benyt den elektroniske registrering som en ekstra service over for dine kunder. Registrer kunderne, udskriv deres personlige adgangskort og send det til dem sammen med din invitation til messen. 
Retur til top
 
BLOMSTER

Du kan købe eller leje grønne planter og blomster til udsmykning af din stand i messecentret. 
Se bestillingsskema og prisliste i afsnit 4. 
Retur til top 

BRANDVÆSENET

Brandvæsenet kræver, at flaskegasanlæg på standen skal godkendes, og at der ikke må anvendes papir, lærred, hessian eller andre letantændelige eller brandbare stoffer, med mindre disse er imprægnerede mod brand samt godkendt af Brandvæsenet. Brandfarlige væsker og celluloidvarer må kun forefindes efter Brandvæsenets særlige godkendelse. 
Godkendelse og yderligere oplysninger kan fås ved henvendelse til Teknisk Afdeling i messecentret eller på tlf. 75 92 25 66. 
Retur til top 

CAFETERIER/RESTAURANTER


For udstillere: 

Udstillercaféer:

Der findes to udstillercaféer på TUN BYG 2012. 
Udstillercaféerne er udelukkende forbeholdt udstillere og deres standpersonale. Adgang til Udstillercaféerne er kun mulig mod forevisning af udfyldt udstillerkort. 

Udstillercaféerne er beliggende: 

Forbeholdt udstillere i Hal A og C: 
Cafeteriet med indgang i øverste venstre del af Hal A (som tidligere). 

Forbeholdt udstillere i Hal D og E:
Venstre del af Restauranten i stueplan i Konferencecentret.

I Udstillercaféerne serveres en buffet med fisk, pålæg, tre lune retter, der veksler hver dag, samt ost og kage til en pris af kr. 108,00 pr. person, inkl. 1 øl eller vand og kaffe/te, såfremt spisebilletten hertil er købt på forhånd. Købes spisebilletten under messen koster den kr. 118,00 pr. person. Se bestillingsskema i afsnit 4. 

Spisebilletterne udfærdiges pr. dag og gælder kun til den pågældende dag.
Spisebilletterne faktureres efter messen, og vi gør venligst opmærksom på, at ubenyttede spisebilletter ikke refunderes. 

Udstillercaféerne har åbent:

13. – 16. marts kl. 11.30 – 16.00 
I opbygnings- og nedtagningsperioden vil der være åbent i Cafeteriet Panorama på 1. sal i indgangspartiet i hovedindgangen. Se oversigtsplan under afsnit 4. 

Cafeteriet Panorama har åbent: 
5. - 11. marts: kl. 8.30 - 15.00 
12. marts: kl. 8.30 - 19.00 (aftensmad) 
13. - 16. marts: kl. 9.00 - 16.00 (messeperioden) 
16. marts desuden: kl. 17.00 - 19.00 (aftensmad) 
17. - 20. marts: kl. 8.30 - 15.00 

Uden for ovennævnte åbningstider kan udstillere altid henvende sig i Teknisk Afdeling for oplysning om dagens spisemuligheder. 
I Messeperioden kan udstillere frit benytte hallens bistroer, cafeterier, restaurant m.v. – se oversigtsplan i afsnit 4 

Yderligere oplysninger om Fredericia Messecenter/MESSE C's mange bespisningsmuligheder finder du under afsnit 4. Har du spørgsmål til messecentrets cafeterier, restaurant, spisebilletter m.v., kan disse rettes til Bettina Andersen eller Brian Therp på tlf. 75 92 25 66. 

For besøgende: 

Åbningstider i messeperioden 13.- 16. marts: kl. 10.00 - 17.00
(Se oversigtsplanen over spisesteder under afsnit 4)

Restauranten 
I stueplan i konferencecentret: kl. 11.30 - 15.00 

Cafeteriet Panorama
På 1. sal i indgangspartiet ved hovedindgangen: kl. 9.00 - 16.00 

Bistro 1, Lille C, Snackbaren og Carl´s Snack 
Kl. 10.00 - 17.00 

Carl´s Corner
Kl. 9.00 - 17.00 

Quick Lunch 
I indgangspartiet ved hovedindgangen: kl. 11.30 - 15.00 

”Håndværker Spisevogn” 
Udendørs ved Busindgangen mellem Hal A og D: kl. 11.30 - 15.00 

Retur til top

DEMONSTRATIONER

Udstilleren er pligtig til, efter evt. demonstrationer der efterlader affald, grundigt og efter hver demonstration at fjerne dette, så medudstillere og deres stande og besøgende ikke genereres heraf. Udstilleren skal også være opmærksom på, at begrænse støj og støv både under og efter eventuelle demonstrationer af maskiner, udstyr m.v. Udvikles der støv, spåner eller lignende ved demonstrationen, skal maskinen forsynes med udsugning. 
Retur til top

EL

Er overalt 400/230 volt. Se bestillingsskema i afsnit 4. 
Retur til top 
EMBALLAGE/TOMGODS

Emballage og tomgods kan mod betaling opbevares i hallen, der påtager sig bortkørsel og tilbagelevering af emballage. Kontakt Dueholm Messespedition på tlf. 22 60 81 02 / 70 20 42 55 – se bestillingsskema i afsnit 4. Priserne for opbevaring af emballage er inkl. afhentning og udbringning. Benyttes truck, betales der separat for denne. 

OBS! 
Emballage skal mærkes med udfyldt emballagemærke. 
Mærkerne kan afhentes hos Messespeditøren ved Teknisk Afdeling og i Informationen i indgangspartiet ved hovedindgangen. 
Retur til top 
FADØLSANLÆG

Bestilling af fadølsanlæg bedes foretaget forud for messen. 
Se bestillingsskema i afsnit 4. 
Retur til top

FAX/TELEFON

Du kan under messen modtage besked via Fredericia Mestercenter/MESSE C's tlf.: 75 92 25 66 eller fax 75 93 21 49. 
Besked til udstillere videregives hver hele time af hallens servicepersonale. Retur til top 

FORSENDELSER

Forsendelser til din stand på messen bedes mærket/adresseret, som følger: 

Standnr. Firmanavn 
TUN BYG 2012 
Fredericia Messecenter/MESSE C 
Vestre Ringvej 101 7000 Fredericia 

Forsendelser i løbet af messeperioden kan afhentes i Informationen i indgangspartiet ved hovedindgangen. 
Ved større forsendelser anvises fragtføreren til den pågældende stand. Vi gør opmærksom på, at Fredericia Messecenter/MESSE C ikke kvitterer for modtagelsen. 
Retur til top 

FORSIKRING

Vi gør opmærksom på, at hverken TUN eller Fredericia Messecenter/MESSE C påtager sig noget ansvar – hverken direkte eller via forsikring – for det udstillede materiel. 

Vi henviser i denne forbindelse venligst til standkontraktens § 9 og § 12 og anbefaler dig at tage kontakt til dit firmas forsikringsselskab med hensyn til forsikring af egne eller lejede udstillingsgenstande, møbler og andet standinventar. Transport og simpelt tyveri under messen: 
Vi gør opmærksom på, at det enkelte firmas erhvervsansvars-, virksomheds- og transportforsikringer kun efter særlig aftale dækker skader under transport, af- og pålæsning samt simpelt tyveri under messen. 

Vær desuden opmærksom på, at det er et krav i alle erhvervsansvarsforsikringer, at der foretages en særlig afskærmning af arbejdende (farlige) udstillingsgenstande, jf. standkontraktens § 7. Se også ”Afskærmning af maskiner”. 
Retur til top 
FOTOGRAF

Der er mulighed for at bestille professionel fotografering af din stand. Du skal blot henvende dig i Informationen i indgangspartiet ved hovedindgangen, der kan formidle kontakt til en fotograf. 

NB! Det er ikke tilladt at fotografere andre stande end din egen, uden forudgående aftale med den pågældende udstiller. 
Retur til top 
FREDERICIA MESSECENTER/MESSE C

Adresse: 

Vestre Ringvej 101 
7000 Fredericia 
Tlf. 75 92 25 66 
Fax 76 20 26 98 (Teknisk Afdeling) / 75 93 21 49 
Retur til top 
FRI HØJDE I HALLERNE

Hal A:
9 m. Mellem dragerne op til 12,5 m. 
Hal C:
4,5 m. Mellem dragerne op til 5,5 m. 
Hal D og E:
6,5 m. Mellem dragerne op til 7,5 m. 
NB! Kun 5,5 m på langs af hallen mellem søjlerne. 
Retur til top 
FØRSTE SAL, ETABLERING AF

1. sal kan kun etableres på fritliggende stande på min 54 kvm. 
Se bestillingsskema i afsnit 3 samt regler for opbygning i afsnit 4. 
Retur til top 
GENERELLE REGLER I FREDERICIA MESSECENTER/MESSE C

FREDERICIA MESSECENTER/MESSE C's Generelle Regler for messedeltagelse skal overholdes. Se venligst afsnit 4.
Retur til top 
GODS

Gods til stande uden opbygning kan modtages i Fredericia Messecenter/MESSE C: 

2. marts 2012 kl. 8.00 - 17.00 

Har du behov for levering tidligere, skal dette aftales med Teknisk Afdeling, som du kan kontakte på tlf. 75 92 25 66 for at afdække mulighederne herfor. 
Retur til top 
HALGULVET

Halgulvet i alle haller er betongulv. 
NB! Det er ikke tilladt at bore, sømme eller på anden måde beskadige gulvet. På grund af gulvets beskaffenhed må udstillere kun benytte speciel dobbeltklæbende tape, som kan købes i Teknisk Afdeling. Eventuel overtrædelse medfører en regning på rengøring eller istandsættelse af gulvet. 
Retur til top 
HOTELLER

Der er 5.000 sengepladser inden for max. 20 minutters kørsel fra messecentret, men husk alligevel at booke dit hotelværelse i god tid. Se hotellisten i afsnit 1 
Retur til top 
INTERN TRANSPORT

Der er både sækkevogne og palleløftere, som udstillere kan benytte til den interne transport, men ikke nok til alle på én gang. Derfor opfordrer vi udstillere til – så vidt muligt – selv at medbringe sækkevogne og palleløftere. 
Retur til top 
HJEMMESIDE

TUN BYG 2012 har egen hjemmeside, som løbende opdateres med de seneste pressemeddelelser, produktomtaler, udstillerliste, nye tiltag m.v. Alle udstillende firmaer vil være at finde her, og man kan indhente informationer om blandt andet transport- og overnatningsmuligheder i området, samt udskrive sit adgangskort og bestille messekataloget. 
I forbindelse med opstart af annoncering og markedsføring målrettes messens hjemmeside primo januar 2012 de besøgende og aktiviteten på hjemmesiden intensiveres. 

Adressen på TUN BYG 2012´s hjemmeside er: www.tunbyg.dk 
Retur til top 
INTERNET

Internetforbindelse på standen er muligt via Fredericia Messecenter/MESSE C's dynamiske netværk. Se bestillingsskema i afsnit 4. 
Retur til top 
KOPI/FAX SERVICE

Er mulig i Arrangørkontoret eller Teknisk Afdeling. 
Se oversigtsplan i afsnit 4.  
Retur til top 
KØBMANDSBUTIK

Købmandsbutikken findes i mellemgangen mellem Hal A og D. Se oversigtsplan i afsnit 4. 
Her har du mulighed for at købe diverse købmandsvarer. Se bestillingsskema i afsnit 4. 

I messeperioden leveres bestillingerne inden for 2 timer ud på standene. Bestillinger afleveret efter kl. 16.00 leveres på standene næste dag. 

Daglige åbningstider: 
12. marts: kl. 8.00 - 18.00 
13. – 16. marts: kl. 9.00 - 11.00 
16. marts desuden: kl. 15.00 - 17.00 

Uden for ovennævnte åbningstider afleveres bestillingsskemaerne i brevsprækken ved købmandsbutikken. Retur til top 
KØRSEL I MESSECENTRET

Kørsel med bil/lastbil i messecentret kan kun ske efter aftale med hallen. Ved henvendelse til Teknisk Afdeling kan udstillere få udleveret indkørselsmærker, som skal placeres tydeligt i bilens forrude påtegnet firmaets standnr. og mobilnummer til bilens fører. 

Parkering i hallerne er absolut forbudt. Kun kortvarig af- og pålæsning er tilladt. 
NB! at sikre et ordentligt arbejdsmiljø for personalet, der arbejder med standopbygning, skal biler/kraner der arbejder med aflæsning etablere aftræksslange til det fri – venligst vær opmærksom på afstanden fra din stand til nærmeste portåbning. 
Retur til top 
LINK TIL EGEN HJEMMESIDE

I udstillerlisten på TUN BYG 2012´s hjemmeside er det muligt for udstillere på messen gratis at bestille link til egen hjemmeside – se afsnit 4. 

Vi opfordrer desuden alle udstillere til at benytte messens logo som bannerlogo på egen hjemmeside, således at de besøgende her kan linke sig videre til TUN BYG 2012 ´s hjemmeside. På den måde støtter vi hinanden i markedsføringen af messen til gavn for alle. 

Adressen til messens hjemmeside er: www.tunbyg.dk 
Retur til top 
LOGO

På messens hjemmeside www.tunbyg.dk finder du messens logo, som du frit kan downloade. Du kan også finde logoet i afsnit 1. 
Læs også ”Link til egen hjemmeside”. 
Retur til top 
MANDSKABSASSISTANCE

Det er muligt at bestille hjælp til af- og pålæsning, udpakning/pakning m.v. 
Se priser og bestillingsskema i afsnit 4. 
Retur til top 
MEDUDSTILLERE AGENTURER/PRODUKTER

Der må ikke på standen reklameres for firmaer eller firmaers agenturer, produkter og løsninger, som ikke er tilmeldt som udstillere/medudstillere og godkendt som sådanne af TUN. Du kan tilmelde medudstillere i afsnit 3 
Retur til top 
MESSEAVIS

I forbindelse med TUN BYG 2012 udarbejdes en messeavis til potentielle besøgende, indeholdende information om messen og messens tema, halplan og udstillerliste, produktnyheder, adgangskort til at klippe ud m.v. 
Messeavisen udsendes af TUN til relevante potentielle besøgende, ligesom den uddeles gratis i TUN´s mere end 450 tømmerhandler og byggemarkeder landet over. 
Udstillere på messen kan rekvirere Messeavisen til udsendelse til kunder og kundepotentiale uden beregning og i ubegrænset antal til levering efter 9. januar 2012. 
Se bestillingsskema i afsnit 3. 
Retur til top 
MESSEKATALOG

Messekataloget udarbejdes, så det foreligger færdigtrykt senest en uge før messen. Optagelse i messekataloget er gratis og omfatter firmafortegnelse og alfabetisk varegruppefortegnelse. Katalogoplysningerne udfyldes i afsnit 3. 
Retur til top 
MESSEKATALOG/FIRMALOGO

Der er mulighed for udstillere at indrykke firmalogo i messekataloget. Firmalogoerne placeres umiddelbart over firmanavnene i den alfabetiske firmafortegnelse. 
Se bestillingsskema i afsnit 3. 
Retur til top 
MESSENYT

Som udstiller vil du under messen blive informeret om aktiviteter, nyheder m.v. i ”Messenyt”, som vil blive runddelt på standene som et dagligt nyhedsbrev. 
Retur til top 
NATTEVAGT

I messeperioden vil der uden for åbningstiderne være nattevagt i Fredericia Messecenter/MESSE C. 
Retur til top 
NEDTAGNNG

Nedtagning af standene skal være afsluttet: 

Senest 20. marts 2012 kl. 21.00 

Den dag messen lukker, fredag den 16. marts, kan der arbejdes til kl. 24.00. Herefter er åbningstiderne i nedtagningsperioden: 
17. – 18. marts: kl. 8.00 – 17.00 
19. – 20. marts: kl. 8.00 – 21.00 

Materialer, der ikke er fjernet på det angivne tidspunkt, vil blive fjernet af messeledelsen for udstillerens egen regning. Fredericia Messecenter/MESSE C tilbyder efter aftale at opbevare maskiner, gods m.v. Kontakt venligst Teknisk Afdeling 

NB! 
Jf. standkontrakten må fjernelse af udstillede varer, sammenpakning og nedtagning ikke ske før efter lukketid den sidste messedag. 
Retur til top  
OPBYGNING

Firmaer med uopbyggede stande kan efter forudgående skriftlig tilmelding starte opbygningen: 

Lørdag den 3. marts 2012 kl. 8.00 

Der kan 3. og 4. marts arbejdes kl. 8.00-16.00

Samtlige udstillere kan påbegynde opbygningen: 

Mandag den 5. marts 2012 kl. 8.00 

Der kan arbejdes alle dage kl. 8.00 - 17.00. Sidste aften inden messen åbner, kan der arbejdes i døgndrift, efter kl. 18.00 dog kun med lettere montage og opstilling på standen og ikke arbejde, der sviner og støver omkringliggende stande. Dit firma vil i givet fald blive opkrævet for rengøring af disse. 

OBS! 
Sidste aften før messen åbner, skal alle gangarealer være ryddede senest kl. 18.00 af hensyn til tæppepålægning. Alle vogne skal derfor være ude af messecentret før dette tidspunkt. Der kan vederlagsfrit arbejdes med lettere montage og opstilling efter dette tidspunkt på standen. 

Hvis du ønsker at arbejde på tidspunkter før kl. 8.00 eller efter kl. 17.00, kan dette aftales mod betaling med Teknisk Afdeling, der træffes dagligt i messecentret eller på tlf. 75 92 25 66. 

Jr. Standkontraktens bestemmelser § 6 gør vi opmærksom på, at de udstillede genstande skal være opstillede, og standene være bragt fuldstændig i orden før åbningen af messen, tirsdag den 13. marts 2012 kl. 10.00. 

Desuden skal de udstillede genstande forblive på plads i den tid, messen varer. Udstillere på messen er ligeledes pligtige til, jf. § 5, at holde standen åben samt have denne forsvarligt bemandet i hele messeperioden.
 
1. sal kan kun etableres på fritliggende på min. 54 kvm. 
Se bestillingsskema i afsnit 3 samt regler for standopbygning i afsnit 4. 
OPBYGNINGSREGLER

Af hensyn til andre udstillere, de besøgende samt myndighedernes krav, er der visse regler for opbygningen af standen, der skal overholdes på TUN BYG 2012 – Se venligst ”Regler for standopbygning” i afsnit 4. 
Retur til top 
PARKERING FOR UDSTILLERE (KUN FOR BILER UNDER 3.500 KG)

Alle udstillere modtager automatisk et P-kort. der skal benyttes ved parkering bag ved messecentret. P-kortet skal påføres standnr. og mobilnummer til bilens fører for at være gyldigt og bedes venligst placeret synligt i bilens forrude. P-kortet er eneste gyldige adgangstegn til udstillerparkeringen. 

Alle udstillere bliver tildelt min. ét P-Kort, som tilsendes senest 14 dage, før messen åbner. Samtlige P-kort bliver fordelt blandt udstillere. Antallet af P-kort beregnes ud fra standens størrelse. 

Udvidet Udstillerparkering 
Arealet til Udstillerparkeringen er udvidet med den tidligere busparkering i forbindelse med TUN BYG 2012, således at udstillere kan disponere over samtlige 800 parkeringspladser bag ved messecentret. Der vil således være rigeligt med plads til at efterkomme den store efterspørgsel på parkeringspladser på Udstillerparkeringen. 
Busparkeringen er flyttet til en separat parkeringsplads til højre for messecentret. versigtsplan i afsnit 4. 

Politihjemmeværnet styrer trafik og parkering om morgenen 
For at undgå kø ved indkørslen til Fredericia Messecenter/MESSE C om morgenen vil politihjemmeværnet stå klar til at styre trafik og parkering. Det indebærer bl.a., at besøgende ledes til parkeringspladser og ad andre veje end indkørslen til hovedindgangen. Denne indkørsel vil mellem kl. 9.00 og 10.00 i videst mulige omfang være forbeholdt udstillere. 

Vi gør dog samtidig opmærksom på, at udstillere har adgang til TUN BYG 2012 fra kl. 9.00 og opfordrer fortsat alle udstillere at møde tidligere, såfremt de ønsker at benytte denne indkørsel til messen, da det ikke kan undgås, at der er kø her kort før kl. 10.00. 

Vil du undgå kø om morgenen, så benyt indkørslen bagved messecentret 
For at undgå kø og ventetid til messen om morgenen anbefaler vi, at udstillere benytter ”bagindgangen” til Udstillerparkeringen bag ved Fredericia Messecenter/MESSE C – Se oversigtsplan i  afsnit 4. Har du GPS, så tast adressen: Fredericia, Stenhøjvej 66. 

OBS! Sidste messedag, fredag den 16. marts 2012, skal alle personbiler, der er parkeret bag ved messecentret flyttes til P-pladserne foran hallen senest kl. 17.30 for at give plads til vare- og lastbiler ved nedtagningen. Vi gør desuden opmærksom på, at tæpperne i messecentret skal være fjernede, før portene bliver åbnet for indkørsel. 
Læs venligst også ”Varebiler, trailere og lastvogne”. 
Retur til top 
PLAKATER

Plakater med information om messen til ophængning i f.eks. receptionen, mødelokaler m.m. som markedsføring for messen, kan rekvireres uden beregning til levering efter 9. januar 2012. 
Se bestillingsskema i afsnit 3. 
Retur til top 
PORTE OG DØRE

Porte og døre i messecentret må ikke spærres
Retur til top
PORT HØJDE/PORT BREDDE

Hal A 
Port 1: 3,9 m høj – 3,9 m bred 
Port 2: 5,9 m høj – 3,9 m bred 
Port 3: 5,9 m høj – 5,9 m bred 
Port 4: 5,9 m høj – 3,9 m bred 
Port 5: 3,9 m høj – 3,9 m bred 
Port 8: 5,9 m høj – 5,9 m bred 

Porte i mellemgang mellem Hal A og D: 
Alle porte: 3,9 m høje – 3,9 m brede

Hal C 
Port 6: 3,9 m høj – 4,9 m bred 
Port 7: 3,9 m høj – 3,9 m bred

Hal D og E: 
Port 9: 5,9 m høj – 5,9 m bred 
Port 10: 3,9 m høj – 3,9 m bred 
Port 11: 5,9 m høj – 5,9 m bred 
Port 12: 3,9 m høj – 3,9 m bred 

Se desuden oversigtsplan i afsnit 4. 
Retur til top 
PORTVAGT

Der vil være vagt ved porten ind til messeområdet. 
Retur til top 
POSTLABELS

Postlabels med information om messen, som kan anvendes af udstillere på al udgående post m.v. kan uden beregning rekvireres i ubegrænset antal til levering efter den 9. januar 2012. 
Se bestillingsskema i afsnit 3. 
Retur til top 
PRESSE

TUN og messens pressechef, Camilla Løjtved, vil løbende holde kontakt til alle relevante fag- og dagblade og foranledige, at repræsentanter fra pressen modtager alt relevant information og materiale vedrørende messen. 

Vi opfordrer derfor alle udstillere til at informere messens pressesekretariat om de spændende produkter og løsninger, der præsenteres på standene, ved at udarbejde pressemeddelelser og produktomtaler og sende disse til: 

Trælasthandlerunonen-TUN 
Pressesekretariatet 
Egebækgård, Egebækvej 98 
2850 Nærum 
e-mail: tunbyg@tun.dk 
Tlf. 45 80 78 77 

Læs mere om pressearbejdet i afsnit 2, hvor du også kan finde en checkliste samt skrive din produktomtale on-line. Se afsnit 2. 

Deadline for indsendelse af pressemateriale er 25. november 2011
Kan du ikke overholde denne deadline, så kontakt os på tlf. 45 80 78 77 
Det er vigtigt, at vi hører fra dig ! 
Retur til top 
PRESSEKONTOR

Messens pressekontor er beliggende i indgangspartiet ved hovedindgangen – se oversigtsplan i afsnit 4. 

Her vil messens pressechef, Camilla Løjtved, fra den 12. marts 2012 og i hele messeperioden stå til rådighed for både pressen og udstillere. Det er samtidig samlingspunktet for pressens repræsentanter, som her vil blive informeret om de udstillede produkter og aktiviteter på messen. 

Har du en god historie fra messen, en speciel aktivitet på standen, så kom ind på Pressekontoret og fortæl om det. 

Det vil desuden være muligt på Pressekontoret at fremlægge nye pressemeddelelser, der ikke kunne offentliggøres inden messen. 
Retur til top 
PRISUDDELING

Byggeriets Klimapris, Byggeriets Energipris og Byggeriets Miljøpris vil blive uddelt under TUN BYG 2012. Nærmere information om hvor og hvornår samt invitation til at overvære prisuddelingen vil tilgå alle udstillere direkte. 
Retur til top 
REKLAMER

Vi gør venligst opmærksom på, at udstillere, iflg. standkontraktens bestemmelser § 6 under ingen omstændigheder må disponere over andet end den tildelte stand, hverken med reklamer, sandwichstandere, borde eller på anden måde. 

Det er således ikke tilladt at omdele reklamer uden for standen eller at anbringe produkter, dekorationer, inventar eller lignende på gang- og/eller friarealer. 

Forbuddet gælder såvel indendørs i messecentret som på alt udendørs areal samt parkeringspladsen og omfatter dermed også parkering af reklamebiler på parkeringspladsen foran hallen. 

Desuden må brug af højttalere, forevisning af film, lysbilleder, lysshow m.v. kun finde sted, såfremt det ikke er til gene for omkringliggende stande. Se i øvrigt Regler for standopbygning i afsnit 4. 

Læs også ”Reklamebiler”. 
Retur til top 
REKLAMEBILER

Som nævnt under ”Reklamer” er al tænkelig PR-aktivitet uden for standen ikke tilladt. Det gælder også på alt udendørs areal og på parkeringspladsen foran hallen. 

Vi gør i den forbindelse opmærksom på, at der vil være mandskab på parkeringen foran hallen i morgentimerne – samt check i løbet af dagen – for at holde øje med ulovlige parkeringer på græsarealer og lignende. 

Retur til top 
RYGNING

Med henvisning til Rygeloven, gør vi venligst opmærksom på, at de indendørs udstillings- og restaurantområder på TUN BYG 2012 vil være røgfri og al rygning skal finde sted udendørs. 

Udendørs rygeområder 

Primært rygeområde for besøgende vil være udenfor ved mellemgangen mellem Hal A og Hal D. Endvidere vil der ved udgangen fra Udstillercaféen ved Hal A og ved hovedindgangen blive etableret rygeområder. 
Alle steder vil der blive holdt opsyn med de udendørs askebægre og tilknyttet rengøring. 

NB! Porte og branddøre i hallen må kun anvendes i tilfælde af brand og vil derfor bliver dækket af med afspærringsbånd. 
Retur til top 
SKILTE

Det er muligt at bestille tekst til friser, skilte m.v. 
Se bestillingsskema i afsnit 4. 
I Teknisk Afdeling er det desuden muligt at købe diverse dekorationsmaterialer til almindelige butikspriser. 
Retur til top 
SPEDITION

Har du behov for assistance ved fragtspedition, kan du kontakte nedenstående speditionsfirma, som er bekendte med forholdene i Fredericia Messecenter/Messe C og har kontor i centret: 

Dueholm Messespedition 
Nørrebrogade 121 
7000 Fredericia 
Tlf. 22 60 81 02 / 70 20 42 55 
Fax 75 93 41 44 
Retur til top 
SPISE- OG DRIKKEVARER

Udlevering af spise- og drikkevarer fra standen må kun finde sted efter skriftlig tilladelse fra Fredericia Messecenter/MESSE C. Se venligst de Generelle Regler i Fredericia Messecenter/MESSE C samt information om bespisningsmuligheder i centret i afsnit 4. 
Retur til top 
SPISEBILLETTER

Der kan købes spisebilletter til de to Udstillercaféer på messen. I Udstillercaféerne serveres en buffet med fisk, pålæg og tre lune retter, der veksler hver dag, samt ost og kage til en pris af kr. 108,00 pr. person, inkl. 1 øl eller vand og kaffe/te, såfremt spisebilletten hertil er købt på forhånd. Købes spisebilletten under messen koster den kr. 118,00 pr. person.
Se bestillingsskema under afsnit 4.  

Spisebilletterne udfærdiges pr. dag og gælder kun for den pågældende dag
Spisebilletterne faktureres efter messen, og vi gør venligst opmærksom på, at ubenyttede spisebilletter ikke refunderes. 

Adgang til Udstillercaféerne er kun mulig mod forevisning af udstillerkort. 
Retur til top 
STANDOPBYGNING

Alle stande under 54 kvm. leveres med opbygning. 
Opbygningen består af hvide 250 cm høje side- og bagvægge og en 30 cm høj skiltefrise i standens forkant(er) 220 cm over gulv, hvorpå standnummeret. er placeret. 
Ønsker du ikke denne opbygning, bedes dette anført på standskitsen. Se afsnit 4. 

Stande langs hallens ydervægge leveres med hvidmalet bagvæg (spånplade). 
Stande lejet uden opbygning er markeret alene med standnr. samt markering af areal på gulvet. 

Ønsker du opbygning, kan side- og bagvægge til nabostand samt skiltefrise m.m. bestilles hos Fredericia Messecenter/MESSE C. – se bestillingsskema i afsnit 4. 

NB! vær mærksom på, at de lejede standvægge og aluminiumsprofiler skal afleveres i samme stand som ved levering. Såfremt vægge eller profiler beskadiges, males, påklæbes tape og lignende, vil du blive faktureret for rengøring og udbedring. 

NB! Opbygningen af standene skal være i overensstemmelse med reglerne for standopbygning på TUN BYG 2012 samt Fredericia Messecenter/MESSE C Generelle Regler – se venligst afsnit 4. 

Demonstrationer på standen 
Udstilleren er pligtig til, efter evt. demonstrationer der efterlader affald, grundigt og efter hver demonstration at fjerne dette, så medudstillere og besøgende ikke genereres heraf. Udstilleren skal også være opmærksom på, at begrænse støj og støv både under og efter eventuelle demonstrationer af maskiner, udstyr m.v. Udvikles der støv, spåner eller lignende ved demonstrationen, skal maskinen forsynes med udsugning. 
Retur til top 

STANDSKILTE

Fredericia Messecenter/MESSE C anbringer et standskilt i A5 format med standnr. vinkelret på standens væg/frise (Gælder for stande købt med opbygning) Bemærk, at dette ikke må forveksles med tekst til skiltefrisen. 
Retur til top 
STANDSKITSE

I Udstillerhåndbogen finder du under afsnit 4/Bestillingsskemaer en standskitse, hvor din stand er indtegnet. Alle monteringsønsker, herunder også specifikationer for el. m.v., bedes venligst angivet på standskitsen, som skal returneres til Fredericia Messecenter/MESSE C sammen med bestillingsskemaerne senest 10. februar 2012.

HUSK at tage kopi af alle skemaer til eget brug. 
Retur til top 
STORE UDSTILLINGSGENSTANDE

Store udstillingsgenstande, der ikke kan transporteres ad de på halplanen indtegnede gange, skal anmeldes til Fredericia Messecenter/MESSE C senest 3 uger før messens afholdelse. 
Retur til top 
SØJLERNE I MESSECENTRET

Søjlerne i hallerne er dimensionerede, som følger: 
I Hal A: 70 cm x 70 cm 
I Hal C: 42 cm x 42 cm 
I Hal D og E: 48 cm x 48 cm 
Retur til top 
TEKNISK AFDELING

Teknisk Afdeling vil være dig behjælpelig med alle spørgsmål og ønsker i forbindelse med opbygning og afvikling af messen. 
Teknisk Afdeling er placeret i Hal A ved Bistro 1 – se oversigtsplan i afsnit 4. 

Åbningstider: 

3. – 4. marts: Kun efter skriftlig aftale 
5. – 11. marts: kl. 8.00 – 17.00 
12. marts: kl. 8.00 – 20.00 
13. – 15. marts: kl. 9.00 – 18.00 
16. marts: kl. 9.00 – 20.00 
17. – 20. marts: kl. 8.00 – 17.00 
Retur til top 
TRYKFLASKER

Der må ikke forefindes nogen form for trykflasker på standene uden forudgående aftale med Teknisk Afdeling i Fredericia Messecenter/MESSE C. 
Retur til top 
UDSKÆNKNING

Vi gør opmærksom på følgende regler for udskænkning af alkohol på TUN BYG 2012, som gælder for alle udstillere: 

- Der må ikke forefinde rene ”barstande”. Standene skal på seriøs vis præsentere leverandørernes produkter. 
- Udskænkning af alkohol må kun foregå bag aflukke, på en eventuel 1. sal eller andet sted, hvor de besøgende ”inviteres ind”. 

- Umådeholden udskænkning af alkohol på standen vil blive stoppet omgående. 

- Udskænkning af øl og spiritus til unge under 18 år er naturligvis ikke tilladt. 

Vi henstiller samtidig til alle udstillere at begrænse udskænkningen på standene og i videst muligt omfang at udskænke ikke-alkoholiske drikke som f.eks. lyst fadøl. 

Messeudvalget har desuden besluttet, at der på messen kun vil blive udleveret 0,25 liter bægre sammen med bestilte fadølsanlæg, ligesom det vil være den eneste type bæger, der kan købes i Købmandsbutikken. 

Reglerne skal ses som et udtryk for, at arrangøren af messen ønsker at fastholde, at TUN BYG-messen i Fredericia er en seriøs fagmesse for fagfolk, hvilket alle udstillere på messen kun kan være interesserede i. 

Vi gør samtidig opmærksom på § 8 på bagsiden af standkontrakten samt Fredericia Messecenter/MESSE C's Generelle Regler – se i afsnit 4. 
Se desuden punktet ”Spise- og drikkevarer”.
Retur til top
UDSTILLERKORT

Et udstillerkort er eneste gyldige adgangskort for standpersonale under messen. Udstillerkortet skal være udfyldt for at være gyldigt. 
Udstillerkortet giver ligeledes adgang til Udstillercaféerne se bestillingsskema under afsnit 3. 

Der benyttes ikke arbejdskort under opbygning og nedtagning. 
Retur til top 
VAREBILER, TRAILERE OG LASTVOGNE M.V.

Varebiler, trailere og lastvogne m.v. må kun befinde sig bagved messecentret i opbygnings- og nedtagningsperioden. Disse skal derfor fjernes herfra senest mandag den 12. marts kl. 22.00 og vil først få adgang igen fredag den 16. marts kl. ca. 17.45. 
Retur til top 
ÅBNING AF MESSEN

Åbningen af messen sker: 

Tirsdag den 13. marts 2012 kl. 9.15 

ved en højtidelighed i Cafeteriet Panorama på 1. sal i indgangspartiet ved hovedindgangen – se oversigtsplan i afsnit 4. 

Samtlige udstillere vil blive inviteret til at deltage i åbningen sammen med fremtrædende repræsentanter fra byggebranchen. Invitation vil tilgå direkte.
Retur til top 
ÅBNINGSTIDER

Opbygning: 
3. – 4. marts: Kun efter skriftlig aftale 
5. – 11. marts: kl. 8.00 – 17.00 
12. marts fra kl. 8.00 

NB! Mandag den 12. marts, sidste opbygningsdag, skal alle gangarealer være ryddede fra kl. 18.00, herefter kan der arbejdes videre med lettere montage på standene. 

TUN BYG 2012´s åbningstider er: 
Tirsdag den 13. marts til fredag den 16. marts 2012 kl. 10.00 – 17.00. 

Vi gør opmærksom på, at der er adgang for udstillere til udstillingsarealet en time før og efter messens åbningstid. 

Nedtagning: 
Fredag den 16. marts kan der efter messens lukning arbejdes til kl. 24.00 
Lørdag den 17. marts og søndag den 18. marts kl. 8.00 – 17.00 
Mandag den 19. marts og tirsdag den 20. marts kl. 8.00 – 21.00 
Retur til top

    </div>
    

    </div>

<div class=footer>
    <div class=footer-container>
        <img style="float:right;" height=106px width=190px src=../../billeder/DB_logo.png/>
        <p style="font-size:16px;font-weight:bold;" >BYGGERI'14</p>
        <p >DANSKE BYGGECENTRE - EGEBÆKVEJ 98 - 2850 NÆRUM
        <br /> TLF. 45 80 78 77 - FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
</div>
 </body>
</html>