﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Upload_nominering.aspx.vb" Inherits="Udstillerhaandbog.Upload_nominering" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload ansøgning</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
    <style type="text/css">
        .auto-style1 {
            width: 70%;
        }
        .auto-style2 {
            width: 60%;
        }
        .auto-style3 {
            width: 759px;
        }
        .auto-style4 {
            height: 22px;
        }
        .auto-style5 {
            height: 22px;
            width: 790px;
        }
        .auto-style6 {
            width: 136px;
        }
        .auto-style7 {
            width: 412px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
               
        <script type="text/javascript">
            function textCounter(field, countfield, maxlimit) {
                if (field.value.length > maxlimit)
                    field.value = field.value.substring(0, maxlimit);
                else
                    countfield.value = maxlimit - field.value.length;
            }
        </script>
    <div>
         <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div> </div>
<div class="page">
<div class="sitemap">Forside - Afsnit 1 - Nominering til Klimapriser - Afsend/upload ansøgning</div>
   <h2>Afsendelse af ansøgning til klimapriser</h2>
   <div style="height: 1096px">
       <br />
       Kontaktperson og email skal udfyldes for hver ansøgning:<br />
       <table class="auto-style2">
           <tr>
               <td class="auto-style6">Kontrakt nummer</td>
               <td class="auto-style7">
                   <asp:TextBox ID="TextBoxKontrakt" runat="server" ReadOnly="True" Width="400px" TabIndex="1"></asp:TextBox>
               </td>
               <td>&nbsp;</td>
           </tr>
           <tr>
               <td class="auto-style6">Firmanavn</td>
               <td class="auto-style7">
                   <asp:TextBox ID="TextBoxFirma" runat="server" ReadOnly="True" Width="400px" TabIndex="2"></asp:TextBox>
               </td>
               <td>&nbsp;</td>
           </tr>
           <tr>
               <td class="auto-style6">Kontaktperson</td>
               <td class="auto-style7">
                   <asp:TextBox ID="TextBoxKontakt" runat="server" Width="400px" TabIndex="3"></asp:TextBox>
               </td>
               <td>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="1" ControlToValidate="TextBoxKontakt"></asp:RequiredFieldValidator>
               </td>
           </tr>
           <tr>
               <td class="auto-style6">Email</td>
               <td class="auto-style7">
                   <asp:TextBox ID="TextBoxEmail" runat="server" Width="400px" TabIndex="4"></asp:TextBox>
               </td>
               <td>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="1" ControlToValidate="TextBoxEmail"></asp:RequiredFieldValidator>
               </td>
           </tr>
       </table>

       <h4>Hvilken pris søges:</h4><br />
       <asp:RadioButton ID="RadioButtonKLima" runat="server" Text="Byggeriets Klimapris" Checked="True" GroupName="1" TabIndex="5" ValidationGroup="1" />
       <br />
       <asp:RadioButton ID="RadioButtonEnergi" runat="server" Text="Byggeriets Energipris" GroupName="1" TabIndex="6" ValidationGroup="1" />
       <br />
       <asp:RadioButton ID="RadioButtonMiljo" runat="server" Text="Byggeriets Miljøpris" GroupName="1" TabIndex="7" ValidationGroup="1" />
       <br />
   <h4>Ved produkt eller løsning til byggeriet</h4>
          <ol class="overskrift">
<li>Giv en kort beskrivelse af egenskaberne ved din virksomheds produkt eller løsning</li>
<li>Giv en kort beskrivelse af, hvilke af de stillede krav dit produkt eller løsning opfylder</li>
<li><strong>Husk!</strong> Upload dokumentation for de krav, dit produkt eller løsning opfylder</li>
            </ol>  
        <table class="auto-style2">
            <tr>
                <td class="auto-style3">   
       <asp:TextBox ID="TextBoxPris" TextMode="MultiLine"  Width="750px" Rows="3" runat="server"  
onkeyup="textCounter(TextBoxPris, this.form.remLen, 3000);" 
            onkeydown="textCounter(TextBoxPris, this.form.remLen, 3000);" 
            Height="80px" MaxLength="3000" TabIndex="8" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ControlToValidate="TextBoxPris" ErrorMessage="*" Font-Size="XX-Large" ForeColor="#CC0000" ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
            </tr>
       </table>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="TextBoxPris" ErrorMessage="Skal udfyldes"></asp:RequiredFieldValidator>
        <br />

<input readonly="readonly" type="text" name="remLen" size="3" maxlength="3" value="3000" 
            style="width: 36px" /><br />

        Tegn tilbage

       <br />
       <table class="auto-style1">
           <tr>
               <td>Venligst upload produkt dokumentation</td>
               <td>
       <asp:FileUpload ID="FileUpload1" runat="server" Font-Overline="False" TabIndex="8" />
               &nbsp;Max 20MB</td>
           </tr>
           <tr>
               <td></td>
               <td>
                   <asp:Button ID="ButtonUpload" runat="server" Text="Upload Filen" Width="90px" />
               </td>

           </tr>
       </table>
              
              

   <h4>Ved klima-, energi- eller miljøstrategi og/eller -indsats</h4>
       <ol class="overskrift">
<li>Giv en kort beskrivelse af din virksomhed</li> 
<li>Præsenter din virksomheds strategi og/eller indsats</li> 
<li> Giv en kort beskrivelse af opnåede resultater</li>
</ol>


       <table class="auto-style2">
           <tr>
               <td class="auto-style5">


       <asp:TextBox ID="TextBoxBeskrivelse" runat="server" Height="230px" TextMode="MultiLine" Width="777px" BorderColor="#999999" BorderWidth="1px" TabIndex="9"></asp:TextBox>

               </td>
               <td class="auto-style4">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxBeskrivelse" ErrorMessage="*" Font-Size="XX-Large" ForeColor="#CC0000" ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
           </tr>
       </table>

       <asp:Button ID="ButtonSend" runat="server" Text="Send ansøgning" Width="132px" TabIndex="10" ValidationGroup="1" />
       <br />
       Der sendes en bekræftelses Email til indtastede Email
</div>
    </div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <h1>BYGGERI'14</h1>
        <p >DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>        
    </form>
</body>
</html>
