﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    Protected Sub Page_Load(sender As Object, e As EventArgs)

    End Sub
</script>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <title>Messehåndbogen</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
   
      <style type="text/css">
          .auto-style2 {
              width: 100%;
          }
          </style>
</head>
<body>
<div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div>
<div class="page">
<div class="sitemap">Forside</div>
<br />
    Velkommen til BYGGERI ´14´s digitale Udstillerhåndbog, der indeholder alle relevante informationer og praktiske oplysninger omkring messen. 
    <br />
    Det er også her du kan bestille alt fra gratis adgangskort til lys på standen, ligesom du kan skrive dine produktomtaler og tekst til messekataloget online. Der er mange nye tiltag på BYGGERI´14, så vi anbefaler dig at læse ”Informationer og praktiske oplysninger” i afsnit2.
    <br />
    <br />
    Vi beder dig venligst grundigt checke oplysninger om firmaet samt udfylde personoplysninger i afsnit 1. 
Bemærk venligst de angivne deadlines for indsendelse af information og bestillinger. 
    <br />
    Se deadlineskema i <a href="http://">afsnit 2.</a><br />



    <br />
&nbsp;<table class="menu">
        <tr class="top">
            <td class="menu" style="height: 86px"  >
                <h2>Firmadata</h2> <br />
                 <h5>Afsnit 1</h5>
            </td>
            <td class="menu" style="height: 86px" >
                <h2>Informationer og praktiske oplysninger</h2><br />
                 <h5>Afsnit 2 </h5> 
            </td>
            <td class="menu" style="height: 86px">
                <h2>Pressemateriale</h2>
                <br />
                   <h5>Afsnit 3</h5>
            </td>
            <td class="menu" style="height: 86px">
                <h2>Bestillinger til Danske byggecentre</h2> <br />
                 <h5>Afsnit 4</h5>
            </td>
            <td class="menu" style="height: 86px">
                <h2>Annoncering/</h2>
                <h2>markedsføring</h2><br />
                 <h5>Afsnit 5</h5>
            </td>
            <td class="menu" style="height: 86px" >
                <h2>Bestillinger til MESSE C</h2> <br />
                 <h5>Afsnit 6</h5>
            </td>
        </tr>
        <tr>
            <td class="menu" style="height: 102px"><a href="http://">Oplysninger om firmaet
             </a>
                <br /><a href="http://">Check/Udfyld</a><br />
                <br />
                <br />
                <br />
                <a href="http://">Nominering til Klimapriser</a></td>
            <td class="menu" style="height: 102px"><a href="http://">Oplysninger om BYGGERI ´14 og MESSE C</a><br />
                <br />
                <br />
                <a href="http://">Deadlineskema<br />
                BYGGERI&#39; 14 logo samt halplaner</a><br />
                <br />
                <br />
                <br />
                <a href="http://">Hotelliste</a><br />
                <a href="http://">Spisesteder</a></td>
            <td class="menu" style="height: 102px">Vejledning i udarbejdelse af pressemateriale&nbsp;
Checkliste<br />
                <br />
                Udarbejd din produktomtale<br />
                <br />
                <br />
                <br />
                <br />
                Upload din produktomtale</td>
            <td class="menu" style="height: 102px">Bestilling af adgangskort, PR-materiale, udstillerkort Bestilling af 1. sal<br />
                <br />
                Tilmelding af medudstillere<br />
                <br />
                <br />
                <br />
                <br />
                Optagelse i messekataloget samt varegrupper</td>
            <td class="menu" style="height: 102px"  ></td>
            <td class="menu" style="height: 102px" >Bestillinger til MESSE C (webshop)
                <br />
                <br />
                <br />
                <br />
                Generelle oplysninger om MESSE C<br />
                Oversigtsplaner<br />
                Regler for standopbygning og udskænkning m.m.</td>
        </tr>
        </table>
    <h2>Bekræftelse</h2>
    <br />
    Efter afsendelse af dine online bestillinger og tekster til Danske Byggecentre, modtager du automatisk en mail med en bekræftelse på det, du har afsendt. 
    <br />
    Modtager du ikke en bekræftelse, har vi IKKE modtaget oplysningerne.
    <br />
    Alle dine bestillinger samt tekst vil være at finde i Udstillerhåndbogen. </div>
<div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <h1>BYGGERI'14</h1>
        <p >DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
</div>
 </body>
</html>