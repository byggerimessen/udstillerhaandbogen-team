﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <title>Messehåndbogen</title>
    <link rel="stylesheet" href="../../Styles/Site.css" type="text/css" media="screen" />

</head>
<body>
<div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1>Udstillerhåndbogen</h1>
        <br/>Pressemateriale
        </div>
    <img src="../../billeder/logo.png" width="240px" height="191px"/>
</div>
</div>
<div class="red"></div>



<div class="page">


<!--sitmap-->
<div class="sitemap">Forside : Afsnit 2 : <u>Pressemateriale</u></div><br />


<!--udskriv-->


<!-- Forside-->
    <h1>
        Checkliste for pressemateriale til TUN BYG 2012
    </h1>

    <br />
    <br />

    <div class="sofie">
    <div class=info>
<h3 id="adgangskort">Udvælg</h3>

Udvælg det eller de produkt(er), som er mest interessante blandt de nyheder, du vil vise på firmaets stand. 
</div>
<div class=info>
<h3 id="H1">Beskriv kort</h3>

Beskriv produktet kort – Hvad er det for et produkt, hvori består nyheden, hvorfor og hvordan. 
Har du en god case, er princippet det samme: Hvad er historien, hvad er nyheden 
En hjælp til at komme i gang kan være at besvare spørgsmålene: 
Hvad er nyt ved produktet ? Hvilke problemer løser det ? Og hvis det er en videreudvikling, hvordan adskiller det sig fra tidligere eller andre produkter ? 

</div>

<div class=info>
<h3 id="H2">Hold omtalen inden for ét enkelt A4-ark</h3> 

En god produktomtale og/eller pressemeddelelse slutter, når der ikke er mere konkret og kontant at fortælle. Undgå opsummeringer og afrundinger. 
Ved flere produkter, <b> skal du udarbejde en separat produktomtale for hvert produkt. </b>
</div>


<div class=info>
<h3 id="H3">Tekst: Almindelig word, arial, punkt 11, sort skrift</h3>

Undlad at fremhæve med fed, kursiv eller lignende, ligesom du bør spare på alt for farverige beskrivelser. 

</div>

<div class=info>
<h3 id="H4">Billeder er vigtige</h3>


Formater: 
Af hensyn til trykprocessen skal billedfiler være i jpg-, .tiff-, pdf- eller .eps-format. 
Billeder må <b>ikke</b> indsættes i Word-dokumentet. 
Billeder skal uploades i separate filer. Husk billedtekst. 
Billedkvalitet: 
Skal pressen kunne benytte de medsendte billeder, skal billederne have en høj opløsning på min. 300 dpi. 

    <br />
    <br />

<a href="http://www.byg-e.dk"> Upload produktomtale og billeder her
</a>

    <br />
    <br />

Materialet kan afsendes online i afsnit om ”Presseinformation”

    
    
    </div>
<!--tekst-->

    </div>

</div>

<div class=footer>
    <div class=footer-container>
        <img style="float:right;" height=106px width=190px src=../../billeder/DB_logo.png/>
        <p style="font-size:16px;font-weight:bold;" >BYGGERI'14</p>
        <p >DANSKE BYGGECENTRE - EGEBÆKVEJ 98 - 2850 NÆRUM
        <br /> TLF. 45 80 78 77 - FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
</div>
 </body>
</html>