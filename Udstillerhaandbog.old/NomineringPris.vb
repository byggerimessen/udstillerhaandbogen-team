﻿Public Class NomineringPris

    Dim _KontaktPerson As String
    Dim _Email As String
    Dim _PrisType As String
    Dim _BeskrivelsePris As String
    Dim _BeskrivelseVirksomhed As String
    Dim _FilNavn As String

    Public Sub New(ByVal KontaktPerson As String, ByVal Email As String, ByVal PrisType As String, ByVal BeskrivelsePris As String, ByVal beskrivelsevirksomhed As String, _
                   ByVal FilNavn As String)
        _KontaktPerson = KontaktPerson
        _Email = Email
        _PrisType = PrisType
        _BeskrivelsePris = BeskrivelsePris
        _BeskrivelseVirksomhed = beskrivelsevirksomhed
        _FilNavn = FilNavn
    End Sub
    Public ReadOnly Property KontaktPerson As String
        Get
            Return _KontaktPerson
        End Get
    End Property
    Public ReadOnly Property Email As String
        Get
            Return _Email
        End Get
    End Property
    Public ReadOnly Property PrisType As String
        Get
            Return _PrisType
        End Get
    End Property
    Public ReadOnly Property BeskrivelsePris As String
        Get
            Return _BeskrivelsePris
        End Get
    End Property
    Public ReadOnly Property Beskrivelsevirksomhed As String
        Get
            Return _BeskrivelseVirksomhed
        End Get
    End Property
    Public ReadOnly Property Filnavn As String
        Get
            Return _FilNavn
        End Get
    End Property
End Class
