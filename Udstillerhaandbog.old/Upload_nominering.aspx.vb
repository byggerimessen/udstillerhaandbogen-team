﻿Public Class Upload_nominering
    Inherits System.Web.UI.Page
    Dim FilnavnDB As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetStamdata()
    End Sub

    Protected Sub ButtonSend_Click(sender As Object, e As EventArgs) Handles ButtonSend.Click

        Dim PrisType As String = ""
        Dim TBNummer As String = HttpContext.Current.Request.LogonUserIdentity.Name
        Dim KontaktPerson As String = TextBoxKontakt.Text
        Dim Email As String = TextBoxEmail.Text
        Dim BeskrivelsePris As String = TextBoxPris.Text
        Dim BeskrivelseVirksomhed As String = TextBoxBeskrivelse.Text
        Dim Filnavn As String = FilnavnDB

        If RadioButtonEnergi.Checked = True Then PrisType = "Energi"
        If RadioButtonKLima.Checked = True Then PrisType = "Klima"
        If RadioButtonMiljo.Checked = True Then PrisType = "Miljø"

        Dim DB As New DB
        Dim Liste As New List(Of NomineringPris)

        Liste.Add(New NomineringPris(KontaktPerson, Email, PrisType, BeskrivelsePris, BeskrivelseVirksomhed, filnavn))

        DB.SendPris(Liste)
        Response.Redirect("Tak_klima.aspx")
    End Sub

    Sub GetStamData()

        Dim KontraktNummer As String = HttpContext.Current.Request.LogonUserIdentity.Name
        Dim DB As New DB
        Dim Firma As String = DB.GetFirmanavn(KontraktNummer)

        TextBoxKontrakt.Text = KontraktNummer
        TextBoxFirma.Text = Firma

    End Sub
    Private Function RandomNumber(ByVal min As Integer, ByVal max As Integer) As Integer
        Dim random As New Random()
        Return random.Next(min, max)
    End Function

    Protected Sub ButtonUpload_Click(sender As Object, e As EventArgs) Handles ButtonUpload.Click
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim NumInt As Integer = RandomNumber(100, 999999)
        Dim NumString As String = CStr(NumInt)

        ' Specify the path on the server to
        ' save the uploaded file to.
        Dim savePath As String = "D:\Data\ByggeriMesse\Priser\"

        ' Before attempting to perform operations
        ' on the file, verify that the FileUpload 
        ' control contains a file.
        Dim GodkendtFil As Boolean = False

        If (FileUpload1.HasFile) Then
            ' Get the name of the file to upload.
            Dim fileName As String = FileUpload1.FileName
            Dim extension As String = IO.Path.GetExtension(fileName)
            extension = LCase(extension)

            'If extension = ".eps" Then GodkendtFil = True
            'If extension = ".jpg" Then GodkendtFil = True
            'If extension = ".tiff" Then GodkendtFil = True
            'If extension = ".pdf" Then GodkendtFil = True
            'If extension = ".tif" Then GodkendtFil = True

            'If GodkendtFil = True Then
            Dim FilNavnMail As String = fileName
            fileName = KontraktNummer + "." + NumString + "." + fileName
            FilnavnDB = fileName

            ' Append the name of the file to upload to the path.
            savePath += fileName
            ' Call the SaveAs method to save the 
            ' uploaded file to the specified path.
            ' This example does not perform all
            ' the necessary error checking.               
            ' If a file with the same name
            ' already exists in the specified path,  
            ' the uploaded file overwrites it.
            FileUpload1.SaveAs(savePath)
            '   LabelForkertFormat.Visible = True
            '   LabelForkertFormat.Text = "fil upload success"
            ' Notify the user of the name the file
            ' was saved under.
            '   MailUpload(FilNavnMail)
            '     Else
            ' If filesize > 20000000 Then
            '   LabelForkertFormat.Visible = True
            '  LabelForkertFormat.Text = "Filer må kun have formattet .eps eller .pdf, filen ikke uploaded"
            '   End If
            '   Else
            '  LabelForkertFormat.Visible = True
            ' LabelForkertFormat.Text = "Der er ikke valgt nogen fil, klik på browse for at finde filen på din computer"
            ' Notify the user that a file was not uploaded.
        End If
    End Sub
End Class