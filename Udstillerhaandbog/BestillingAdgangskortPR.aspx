﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BestillingAdgangskortPR.aspx.vb" Inherits="Udstillerhaandbog.BestillingAdgangskortPR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Bestilling af adgangskort, PR-materiale, udstillerkort</title>
<link rel="stylesheet" href="Site.css" type="text/css" media="screen" />

    <style type="text/css">
        .auto-style1 {
            height: 24px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
         <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
    <h2>Bestilling af adgangskort, PR-materiale, udstillerkort</h2>
    <br />
    Deadline for bestilling: <strong>18. november 2013</strong><br />
&nbsp;<table class="style21">
            <tr>
                <td class="auto-style1">
                    Adgangskort til besøgende</td>
                <td class="auto-style1">
                    <asp:TextBox ID="TextBoxAdgangsKort" runat="server" Width="50px" TabIndex="20" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                </td>
                <td class="auto-style1">
                    stk.</td>
                    <td class="auto-style1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" 
                            ControlToValidate="TextBoxAdgangsKort" ErrorMessage="vælg 0 hvis ingen" 
                            ValidationGroup="10"></asp:RequiredFieldValidator>
                </td>
                <td class="auto-style1">
                    <asp:RangeValidator ID="RangeValidator1" runat="server" 
                        ControlToValidate="TextBoxAdgangsKort" ErrorMessage="kun heltal" 
                        MaximumValue="100000" MinimumValue="0" ValidationGroup="10" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="style22">
                    Postlabels til udgående post</td>
                <td class="style23">
                    <asp:TextBox ID="TextBoxPostLabels" runat="server" Width="50px" TabIndex="21" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                </td>
                <td class="style24">
                    stk.</td><td class="style32">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" 
                        ControlToValidate="TextBoxPostLabels" ErrorMessage="vælg 0 hvis ingen" 
                        ValidationGroup="10"></asp:RequiredFieldValidator>
                </td>
                <td class="style25">
                    <asp:RangeValidator ID="RangeValidator2" runat="server" 
                        ControlToValidate="TextBoxPostLabels" ErrorMessage="kun heltal" 
                        MaximumValue="100000" MinimumValue="0" ValidationGroup="10" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
            <td class="style22">Plakater (40 cm x 60 cm - max. 10 stk.)</td>
            <td><asp:TextBox ID="TextBoxPlakater" runat="server" Width="50px" TabIndex="22" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox></td>
            <td>stk.</td><td class="style32">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" 
                        ControlToValidate="TextBoxPlakater" ErrorMessage="vælg 0 hvis ingen" 
                        ValidationGroup="10"></asp:RequiredFieldValidator>
                    </td>
            <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" 
                        ControlToValidate="TextBoxPlakater" ErrorMessage="kun heltal" 
                        MaximumValue="100000" MinimumValue="0" ValidationGroup="10" 
                    Type="Integer"></asp:RangeValidator>
                    </td>
            </tr>
            <tr>
                <td class="style22">
                    Udstillerkort til standpersonale under messen</td>
                <td class="style23">
                    <asp:TextBox ID="TextBoxUdstillerKort" runat="server" Width="50px" 
                        TabIndex="24" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                </td>
                <td class="style24">
                    stk.</td><td class="style32">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" 
                        ControlToValidate="TextBoxUdstillerKort" ErrorMessage="vælg 0 hvis ingen" 
                        ValidationGroup="10"></asp:RequiredFieldValidator>
                </td>
                <td class="style25">
                    <asp:RangeValidator ID="RangeValidator4" runat="server" 
                        ControlToValidate="TextBoxUdstillerKort" ErrorMessage="kun heltal" 
                        MaximumValue="100000" MinimumValue="0" ValidationGroup="10" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="style22">
                    <asp:Button ID="ButtonSendAdgangsKort" runat="server" Height="21px" TabIndex="25" 
                        Text="Send" style="text-align: left" ValidationGroup="10" />
                </td>
                <td class="style23">
                    &nbsp;</td>
                <td class="style24">
                    &nbsp;</td>
                <td class="style32">
                    &nbsp;</td>
            </tr>
        </table>

    <br />

   <section> <strong>Vigtigt!
</strong>
       <br />
       Benyt disse gratis adgangskort i alle udsendelser fra firmaet, lad sælgerne få en stak med på turen rundt til kunder og byggepladser, så du på alle måder gør opmærksom på dit firmas deltagelse på BYGGERI ´14! 

       <br />
       <br />
       Konstant påvirkning af målgrupperne er et vigtigt element i markedsføringen af messe og dit firmas deltagelse. 
       <br />
       <strong>Jo flere påvirkninger – jo flere besøgende.</strong>
       <br />
       <br />
       <h4>Elekronisk registrering – benyt også den
</h4>
     
       Som tidligere vil det på messens hjemmeside, <a href="http://byggerimessen.dk/">www.byggerimessen.dk</a>, vil være muligt for besøgende at registrere sig elektronisk og udskrive deres personlige adgangskort til messen.
Benyt denne mulighed – link til registreringssiden, når du udsender invitationer, eller registrer kunderne, udskriv deres personlige adgangskort og send det sammen med din invitation til at besøge din stand på messen.
</section>
    <br />
    <a href="Default.aspx">Tilbage til forsiden</a>
    </div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>