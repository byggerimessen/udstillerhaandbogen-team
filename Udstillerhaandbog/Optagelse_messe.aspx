﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Optagelse_messe.aspx.vb" Inherits="Udstillerhaandbog.Optagelse_messe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Optagelse i messekatalog samt varegrupper</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
     <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
    <h2>Optagelse i messekatalog samt varegrupper</h2>
    <p>Nedenstående er de firmadata, vi har registreret, og som vil fremgå i messekataloget. Ret forkerte data.</p>
 <!-- her kommer bokse ind fra 2012 !-->
    <table>
    <tr>
        <td>
            Firmanavn</td>
        <td>
            <asp:TextBox ID="TextBoxFirmaNavn" runat="server" TabIndex="3" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
        <td class="auto-style11">
            Stand nummer</td>
        <td class="auto-style12">
            <asp:TextBox ID="TextBoxStandNummer" runat="server" ReadOnly="True" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Adresse</td>
        <td>
            <asp:TextBox ID="TextBoxAdresse" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
        <td class="auto-style11">
            &nbsp;</td>
        <td class="auto-style12">
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            Postnr.</td>
        <td>
            <asp:TextBox ID="TextBoxPostNummer" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
        <td class="auto-style11">
            By</td>
        <td class="auto-style12">
            <asp:TextBox ID="TextBoxBy" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Telefon</td>
        <td>
            <asp:TextBox ID="TextBoxTelefon" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
        <td class="auto-style11">
            Fax nr.</td>
        <td class="auto-style12">
            <asp:TextBox ID="TextBoxFax" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
    </tr>
   
         <tr>
        <td> Firma Email</td>
         <td>
            <asp:TextBox ID="TextBoxFirmaEmail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
       </td>
              <td class="auto-style11">
                  Firma webside</td>
        <td class="auto-style12">            
            <asp:TextBox ID="TextBoxWebside" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
                        </tr>
       <tr>
       <td>Udfyldt af</td>
       <td>            
            <asp:TextBox ID="TextBoxUdfyldtAf" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
           </td>
           <td>Kontrakt nummer</td>
           <td>
                  <asp:TextBox ID="TextBoxKontraktNummer" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px" ReadOnly="True"></asp:TextBox>
           </td>
              </tr>
        <tr>
            <td>Kontakt Email</td>
            <td>            
            <asp:TextBox ID="TextBoxKontaktEmail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
        </tr>
      
</table>
 <h4>Firmafortegnelse, beskriv dit firma og de produkter, dit firma udstiller </h4>
       Udfyld tekst til firmafortegnelsen her<br />
       
       <asp:TextBox ID="TextBoxFortegnelse" TextMode="MultiLine"  Width="750px" 
        Rows="3" runat="server"  
onkeyup="textCounter(TextBoxFortegnelse, this.form.remLen, 800);" 
            onkeydown="textCounter(TextBoxFortegnelse, this.form.remLen, 800);" 
            Height="80px" MaxLength="800" TabIndex="1" />
          
       
    <br />
   <input readonly="readonly" type="text" name="remLen" size="3" maxlength="3" value="800" 
   style="width: 36px" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
        runat="server" ControlToValidate="TextBoxFortegnelse" 
        ErrorMessage="Udfyld venligst ovenstående boks" ValidationGroup="1"></asp:RequiredFieldValidator>
    <br />
   Tegn tilbage<br />
           <asp:Button ID="ButtonSendOptagelse" runat="server" Height="21px" Text="Send" 
            ValidationGroup="1" />
    <br />
    <br />
    <h2>Varegruppefortegnelse</h2>
I katalogets afdeling for varegrupper kan dit firma angive, hvilke produkter der udstilles på messen. 
    Tryk på&nbsp; 
<a href="VareGrupper.aspx">linket her for at komme til varegruppefortegnelsen</a>, 
    hvor du får mulighed for at vælge varegrupper. <strong>
    <br />
    Deadline 18. november 2013.</strong> 
    <br /><br /><br />
    <a href="Default.aspx">Tilbage til forsiden</a>
    </div>
         <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
