﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class DB

    Function ConnectionString()
        Dim Cstring As String = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=ByggeriMessen;data Source=localhost\SQLEXPRESS"
        'Dim Cstring As String = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=TUNBYG;data Source=172.20.0.5"
        Return (Cstring)
    End Function

    Function GetFirmanavn(ByVal TBNummer As String) As List(Of NomineringPris)
        Dim Cstring As String = ConnectionString()
        Dim Liste As New List(Of NomineringPris)
        Dim cn As New SqlConnection(Cstring)
        Dim cmd As New SqlCommand("SELECT * FROM Leverandorinfo2014 WHERE @KontraktNummer = KontraktNummer", cn)
        Dim reader As SqlDataReader
        Dim Firma As String = ""
        Dim Standnr As String = ""
        Dim KontraktNummerp As New SqlParameter("@KontraktNummer", SqlDbType.NVarChar)
        cmd.Parameters.Add(KontraktNummerp).Value = TBNummer

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While reader.Read()
                If Not reader.IsDBNull(reader.GetOrdinal("Firma")) Then Firma = CStr(reader("Firma"))
                If Not reader.IsDBNull(reader.GetOrdinal("Standnr")) Then Standnr = (CStr(reader("Standnr")))
                Liste.Add(New NomineringPris(Firma, Standnr, Nothing, Nothing, Nothing, Nothing, Nothing))
            End While
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try
        Return Liste
    End Function

    Sub SendPris(ByVal Liste As List(Of NomineringPris))

        Dim TBNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim KontaktPerson As String = ""
        Dim Email As String = ""
        Dim PrisType As String = ""
        Dim BeskrivelsePris As String = ""
        Dim IndberetType As String = ""
        Dim Filnavn As String = ""

        For Each Item As NomineringPris In Liste
            KontaktPerson = Item.KontaktPerson
            Email = Item.Email
            PrisType = Item.PrisType
            BeskrivelsePris = Item.BeskrivelsePris
            IndberetType = Item.IndberetType
            Filnavn = Item.Filnavn
        Next

        Dim Cstring As String = ConnectionString()
        Dim cmd As New SqlCommand
        Dim cn As New SqlConnection(Cstring)
        cmd = New SqlCommand("INSERT INTO NomineringsPriser (Dato, TBNummer, KontaktPerson, Email, PrisType, BeskrivelsePris, IndberetType, Filnavn) VALUES (@Dato, @TBNummer, @KontaktPerson, @Email, @PrisType, @BeskrivelsePris, @IndberetType, @Filnavn)", cn)

        Dim DatoP As New SqlParameter("@Dato", SqlDbType.DateTime)
        Dim TBNummerP As New SqlParameter("@TBnummer", SqlDbType.NVarChar)
        Dim KontaktPersonp As New SqlParameter("@KontaktPerson", SqlDbType.NVarChar)
        Dim Emailp As New SqlParameter("@Email", SqlDbType.NVarChar)
        Dim PrisTypep As New SqlParameter("@PrisType", SqlDbType.NVarChar)
        Dim BeskrivelsePrisp As New SqlParameter("@BeskrivelsePris", SqlDbType.NVarChar)
        Dim BeskrivelseVirksomhedp As New SqlParameter("@IndberetType", SqlDbType.NVarChar)
        Dim Filnavnp As New SqlParameter("@Filnavn", SqlDbType.NVarChar)

        cmd.Parameters.Add(DatoP).Value = DateTime.Now
        cmd.Parameters.Add(TBNummerP).Value = TBNummer
        cmd.Parameters.Add(KontaktPersonp).Value = KontaktPerson
        cmd.Parameters.Add(Emailp).Value = Email
        cmd.Parameters.Add(PrisTypep).Value = PrisType
        cmd.Parameters.Add(BeskrivelsePrisp).Value = BeskrivelsePris
        cmd.Parameters.Add(BeskrivelseVirksomhedp).Value = IndberetType
        cmd.Parameters.Add(Filnavnp).Value = Filnavn

        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try

    End Sub

    Sub InsertFiler(ByVal Filnavn As String)
        Dim TBNummer As String = Web.HttpContext.Current.User.Identity.Name

        Dim Cstring As String = ConnectionString()
        Dim cmd As New SqlCommand
        Dim cn As New SqlConnection(Cstring)
        cmd = New SqlCommand("INSERT INTO NomineringsFiler (Dato, TBNummer, Filnavn) VALUES (@Dato, @TBNummer, @Filnavn)", cn)

        Dim DatoP As New SqlParameter("@Dato", SqlDbType.DateTime)
        Dim TBNummerP As New SqlParameter("@TBnummer", SqlDbType.NVarChar)
        Dim Filnavnp As New SqlParameter("@Filnavn", SqlDbType.NVarChar)

        cmd.Parameters.Add(DatoP).Value = DateTime.Now
        cmd.Parameters.Add(TBNummerP).Value = TBNummer
        cmd.Parameters.Add(Filnavnp).Value = Filnavn

        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try

    End Sub

End Class
