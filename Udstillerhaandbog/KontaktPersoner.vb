﻿Public Class KontaktPersoner
    Dim _Standansvarlig As String = ""
    Dim _Marketingansvarlig As String = ""
    Dim _Presseansvarlig As String = ""

  
    Public Sub New(ByVal Standansvarlig As String, ByVal Marketingansvarlig As String, ByVal Presseansvarlig As String)
        _Standansvarlig = Standansvarlig
        _Marketingansvarlig = Marketingansvarlig
        _Presseansvarlig = Presseansvarlig
    End Sub
    Public ReadOnly Property Standansvarlig() As String
        Get
            Return _Standansvarlig
        End Get
    End Property
    Public ReadOnly Property Marketingansvarlig() As String
        Get
            Return _Marketingansvarlig
        End Get
    End Property
    Public ReadOnly Property Presseansvarlig() As String
        Get
            Return _Presseansvarlig
        End Get
    End Property
End Class
