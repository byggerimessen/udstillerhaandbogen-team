﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Public Class DBStamData
    Function ConnectionString()
        Dim GetCString As New DB
        Dim Cstring As String = GetCString.ConnectionString
        Return (Cstring)
    End Function
    Function HentData()
        Dim Liste As New List(Of GetFirmaData)

        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        Dim cmd As New SqlCommand("Select * FROM LeverandorInfo2014 WHERE @KontraktNummer = KontraktNummer", cn)
        Dim KontraktNummerp As New SqlParameter("@KontraktNummer", SqlDbType.NVarChar)
        cmd.Parameters.Add(KontraktNummerp).Value = KontraktNummer

        Dim FirmaNavn As String = ""
        Dim Adresse As String = ""
        Dim PostNummer As String = ""
        Dim City As String = ""
        Dim Navn As String = ""
        Dim Telefon As String = ""
        Dim Email As String = ""
        Dim StandNr As String = ""
        Dim Webside As String = ""
        Dim FirmaEmail As String = ""
        Dim Fax As String = ""

        Dim reader As SqlDataReader
        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While reader.Read()
                If Not reader.IsDBNull(reader.GetOrdinal("Firma")) Then FirmaNavn = CStr(reader("Firma"))
                If Not reader.IsDBNull(reader.GetOrdinal("Adresse")) Then Adresse = CStr(reader("Adresse"))
                If Not reader.IsDBNull(reader.GetOrdinal("PostNummer")) Then PostNummer = CStr(reader("PostNummer"))
                If Not reader.IsDBNull(reader.GetOrdinal("City")) Then City = CStr(reader("City"))
                If Not reader.IsDBNull(reader.GetOrdinal("Navn")) Then Navn = CStr(reader("Navn"))
                If Not reader.IsDBNull(reader.GetOrdinal("Telefon")) Then Telefon = CStr(reader("Telefon"))
                If Not reader.IsDBNull(reader.GetOrdinal("Email")) Then Email = CStr(reader("Email"))
                If Not reader.IsDBNull(reader.GetOrdinal("StandNr")) Then StandNr = CStr(reader("StandNr"))
                If Not reader.IsDBNull(reader.GetOrdinal("Webside")) Then Webside = CStr(reader("Webside"))
                If Not reader.IsDBNull(reader.GetOrdinal("Firmaemail")) Then FirmaEmail = CStr(reader("Firmaemail"))
                If Not reader.IsDBNull(reader.GetOrdinal("Faxnummer")) Then Fax = CStr(reader("Faxnummer"))
                '     If Not reader.IsDBNull(reader.GetOrdinal("KontraktNummer")) Then KontraktNummerDB As String = CStr(reader("KontraktNummer"))
                Liste.Add(New GetFirmaData(FirmaNavn, Adresse, PostNummer, City, Navn, Telefon, Email, StandNr, KontraktNummer, Webside, FirmaEmail, Fax))
            End While
        Finally
        End Try
        Return (Liste)
    End Function

    Sub SendData(ByVal Liste As List(Of String))

        Dim cmd As New SqlCommand
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        cmd = New SqlCommand("UPDATE LeverandorInfo2014 SET Firma = @Firma,  Adresse = @Adresse, PostNummer = @PostNummer, City = @City, Navn = @Navn, Telefon = @Telefon, Email = @Email WHERE KontraktNummer = @KontraktNummer", cn)

        Dim KontraktNummerp As New SqlParameter("@KontraktNummer", SqlDbType.NVarChar)
        cmd.Parameters.Add(KontraktNummerp).Value = KontraktNummer

        Dim FirmaParam As New SqlParameter("@Firma", SqlDbType.NVarChar, 500)
        Dim AdresseParam As New SqlParameter("@Adresse", SqlDbType.NVarChar, 500)
        Dim PostNummerParam As New SqlParameter("@PostNummer", SqlDbType.NVarChar, 500)
        Dim CityParam As New SqlParameter("@City", SqlDbType.NVarChar, 500)
        Dim NavnParam As New SqlParameter("@Navn", SqlDbType.NVarChar, 500)
        Dim TelefonParam As New SqlParameter("@Telefon", SqlDbType.NVarChar, 500)
        Dim EmailParam As New SqlParameter("@Email", SqlDbType.NVarChar, 500)

        cmd.Parameters.Add(FirmaParam)
        cmd.Parameters.Add(AdresseParam)
        cmd.Parameters.Add(PostNummerParam)
        cmd.Parameters.Add(CityParam)
        cmd.Parameters.Add(NavnParam)
        cmd.Parameters.Add(TelefonParam)
        cmd.Parameters.Add(EmailParam)

        FirmaParam.Value = Liste.Item(0)
        AdresseParam.Value = Liste.Item(1)
        PostNummerParam.Value = Liste.Item(2)
        CityParam.Value = Liste.Item(3)
        NavnParam.Value = Liste.Item(4)
        TelefonParam.Value = Liste.Item(5)
        EmailParam.Value = Liste.Item(6)

        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try
    End Sub
    Function HentDataToProperty() As List(Of GetFirmaData)
        Dim Liste As New List(Of GetFirmaData)
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        Dim cmd As New SqlCommand("Select * FROM LeverandorInfo WHERE @KontraktNummer = KontraktNummer", cn)

        Dim KontraktNummerp As New SqlParameter("@KontraktNummer", SqlDbType.NVarChar)
        cmd.Parameters.Add(KontraktNummerp).Value = KontraktNummer

        Dim FirmaNavn As String = ""
        Dim Adresse As String = ""
        Dim PostNummer As String = ""
        Dim City As String = ""
        Dim Navn As String = ""
        Dim Telefon As String = ""
        Dim Email As String = ""
        Dim StandNummer As String = ""
        Dim KontraktNummerDB As String = ""
        Dim Fax As String = ""
        Dim Webside As String = ""
        Dim FirmaEmail As String = ""

        Dim reader As SqlDataReader
        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While reader.Read()
                If Not reader.IsDBNull(reader.GetOrdinal("Firma")) Then FirmaNavn = CStr(reader("Firma"))
                If Not reader.IsDBNull(reader.GetOrdinal("Adresse")) Then Adresse = CStr(reader("Adresse"))
                If Not reader.IsDBNull(reader.GetOrdinal("PostNummer")) Then PostNummer = CStr(reader("PostNummer"))
                If Not reader.IsDBNull(reader.GetOrdinal("City")) Then City = CStr(reader("City"))
                If Not reader.IsDBNull(reader.GetOrdinal("Navn")) Then Navn = CStr(reader("Navn"))
                If Not reader.IsDBNull(reader.GetOrdinal("Telefon")) Then Telefon = CStr(reader("Telefon"))
                If Not reader.IsDBNull(reader.GetOrdinal("Email")) Then Email = CStr(reader("Email"))
                If Not reader.IsDBNull(reader.GetOrdinal("FaxNummer")) Then Fax = CStr(reader("FaxNummer"))
                If Not reader.IsDBNull(reader.GetOrdinal("Webside")) Then Webside = CStr(reader("Webside"))
                If Not reader.IsDBNull(reader.GetOrdinal("Firmaemail")) Then FirmaEmail = CStr(reader("Firmaemail"))
                If Not reader.IsDBNull(reader.GetOrdinal("StandNr")) Then StandNummer = CStr(reader("StandNr"))
                If Not reader.IsDBNull(reader.GetOrdinal("KontraktNummer")) Then KontraktNummerDB = CStr(reader("KontraktNummer"))
                Liste.Add(New GetFirmaData(FirmaNavn, Adresse, PostNummer, City, Navn, Telefon, Email, StandNummer, KontraktNummer, Webside, FirmaEmail, Fax))
            End While
        Finally
        End Try
        Return (Liste)
    End Function

    Sub SendMesseNyt(ByVal Navn As String, ByVal Email As String)

        Dim TBNummer As String = Web.HttpContext.Current.User.Identity.Name

        Dim Cstring As String = ConnectionString()
        Dim cmd As New SqlCommand
        Dim cn As New SqlConnection(Cstring)
        cmd = New SqlCommand("INSERT INTO MesseNyt (Dato, TBNummer, Navn, Email) VALUES (@Dato, @TBNummer, @Navn, @Email)", cn)

        Dim DatoP As New SqlParameter("@Dato", SqlDbType.DateTime)
        Dim TBNummerP As New SqlParameter("@TBnummer", SqlDbType.NVarChar)
        Dim Navnp As New SqlParameter("@Navn", SqlDbType.NVarChar)
        Dim Emailp As New SqlParameter("@Email", SqlDbType.NVarChar)

        cmd.Parameters.Add(DatoP).Value = DateTime.Now
        cmd.Parameters.Add(TBNummerP).Value = TBNummer
        cmd.Parameters.Add(Navnp).Value = Navn
        cmd.Parameters.Add(Emailp).Value = Email

        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try

    End Sub

    Function HentMesseNyt() As List(Of MesseNyt)
        Dim Liste As New List(Of MesseNyt)

        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name

        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        Dim cmd As New SqlCommand("Select * FROM Messenyt WHERE @KontraktNummer = TBNummer", cn)
        Dim KontraktNummerp As New SqlParameter("@KontraktNummer", SqlDbType.NVarChar)
        cmd.Parameters.Add(KontraktNummerp).Value = KontraktNummer
        Dim Navn As String = ""
        Dim Email As String = ""
        Dim reader As SqlDataReader
        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While reader.Read()
                If Not reader.IsDBNull(reader.GetOrdinal("Navn")) Then Navn = CStr(reader("Navn"))
                If Not reader.IsDBNull(reader.GetOrdinal("Email")) Then Email = CStr(reader("Email"))
                Liste.Add(New MesseNyt(Navn, Email))
            End While
        Finally
        End Try
        Return (Liste)
    End Function

End Class
