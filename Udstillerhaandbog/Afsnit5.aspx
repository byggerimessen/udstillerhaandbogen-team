﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Afsnit5.aspx.vb" Inherits="Udstillerhaandbog.Afsnit5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../Styles/Site.css" type="text/css" media="print" />
</head>
<body>
     <form id="form1" runat="server">
    <div class="header">
    <div class="header-container">
        <div class="header-text">
            <br />
        <h1>Udstillerhåndbogen</h1>
        <br/> 

        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div> 
<div class="page">
    <h2>Annoncering / markedsføring – et tilbud til udstillere</h2>

    <br />
    På BYGGERI ´14 er der en lang række nye tilbud til udstillere om annoncering/ markedsføring i forbindelse med messen, som øger firmaets eksponering før og under messen.
Se de mange muligheder nedenfor.

    <br />
    &nbsp;<br />
    <h2>Bannerannoncering</h2> 
    Hjemmeside – mangler antal og placeringer<br />
    App – mangler antal og placeringer
    <br />

    <br />
    <h2>Annonce og/eller logo i messekatalog – Se 2012</h2>

Besøgsregistrering på standen <br />
BYGGERI ´14 tilbyder udstillere adgang via scanning til de besøgendes data. Det gælder de besøgende på den enkelte stand, der har registreret sig elektronisk og dermed har en stregkode på navneskiltet. 
    <br /><br />

<h2>Reklamepakker - følger</h2>

Pressepakker – følger<br />
For at give udstilleres presseansvarlige de bedste muligheder for et godt udbytte af messen i form af omtale af deres firmas produkter og services, tilbyder Danske Byggecentre i samarbejde med HOLM Kommunikation en række ”pressepakker”, der kan styrke det enkelte firmas eksponering i såvel trykte som on-line medier

    
    <br /><br />
    <h2>Pressepakker – følger</h2>For at give udstilleres presseansvarlige de bedste muligheder for et godt udbytte af messen i form af omtale af deres firmas produkter og services, tilbyder Danske Byggecentre i samarbejde med HOLM Kommunikation en række ”pressepakker”, der kan styrke det enkelte firmas eksponering i såvel trykte som on-line medier

    <br /><br />
    <a href="Default.aspx">Tilbage til forsiden</a>
    <br />

        </div>
    <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
      
        <p >ARRANGØR:<br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
