﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VaregrupperM-P.aspx.vb" Inherits="Udstillerhaandbog.VaregrupperM_P" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Varegruppefortegnelse M-P</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
   <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
     <h2>Varegruppefortegnelse (varegrupper M - P)</h2>
    <br />
        <table class="style1">
            <tr>
                <td>
      <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/VaregrupperI-L.aspx">Forrige side</asp:LinkButton>
    &nbsp;Husk at godkende på side 7</td>
                <td align=right>
                    <asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/VaregrupperR-U.aspx">Næste 
            side</asp:LinkButton>
                </td>
            </tr>
        </table>
     <br />
        <hr />
    
     <table class="style1">
     <tr>
    <td width=300><h4>&nbsp; M</h4></td>
    <td width=300><h4>&nbsp; N</h4></td>
    <td width=300><h4>&nbsp; O</h4></td>
    <td width=300><h4>&nbsp; P</h4></td>
     </tr>
        <tr valign=top>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListM" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListN" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListO" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListP" runat="server">
                </asp:CheckBoxList>
            </td>
        </tr>
    </table>
        <br /><hr />
        <table class="style1">
            <tr>
                <td>
      <asp:LinkButton ID="LinkButton4" runat="server" PostBackUrl="~/VaregrupperI-L.aspx">Forrige side</asp:LinkButton>
    &nbsp;Husk at godkende på side 7</td>
                <td align=right>
                Side 4, <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/VaregrupperR-U.aspx">Næste 
            side</asp:LinkButton>
            </td>
        </tr>
    </table>
    </div>
         <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
