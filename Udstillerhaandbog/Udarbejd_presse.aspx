﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Udarbejd_presse.aspx.vb" Inherits="Udstillerhaandbog.Udarbejd_presse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Udarbejdelse/upload af produktomtale</title>
     <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 693px;
        }
        .auto-style3 {
            width: 134px;
        }
        .auto-style4 {
            width: 67px;
        }
        .auto-style5 {
            width: 177px;
        }
        .auto-style6 {
            width: 145px;
        }
        .auto-style7 {
            color: #FF0000;
        }
        .auto-style8 {
            width: 74px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
<h2>Udarbejdelse/upload af produktomtale</h2>
    <br />
    Vær opmærksom på, at du uploader din produktomtale direkte på messens hjemmeside, så det er vigtigt, at du følger den angivne skabelon. 
    Check derfor din produktomtalte grundigt igennem, inden du uploader, og husk at vedlægge fotos.<br />
    <br />
&nbsp;<table class="auto-style1">
        <tr>
            <td class="auto-style3">Firmanavn</td>
            <td class="auto-style5">
                <asp:TextBox ID="TextBoxFirma" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
            <td class="auto-style4">Standnr.</td>
            <td>
                <asp:TextBox ID="TextBoxStandNr" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Kontaktperson</td>
            <td class="auto-style5">
                <asp:TextBox ID="TextBoxKontaktPerson" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
            <td class="auto-style4">Email</td>
            <td>
                <asp:TextBox ID="TextBoxMail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table class="auto-style1">
        <tr>
            <td class="auto-style6">Overskrift på omtalen:</td>
            <td>
                <asp:TextBox ID="TextBoxOverskriftOmtale" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="317px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    Skriv omtalen her (som det ser ud i nedenstående tekstboks, er ligeledes sådan det kommer til at se ud på messens hjemmesiden):
    <br />
    <asp:TextBox ID="TextBoxOmtale" runat="server" BorderStyle="Inset" BorderWidth="1px" Height="247px" TextMode="MultiLine" Width="773px"></asp:TextBox>
    <br />
    <br />
    <br />
    <br />
    Upload evt. fotos til omtalen. Det er muligt at uploade op til 3 fotos i formaterne <span class="auto-style7">JPEG, ??</span> med en opløsning på min. 300 dpi.<br />

    <asp:FileUpload ID="FileUpload1" runat="server" />

    <br />
    <table class="auto-style1">
        <tr>
            <td class="auto-style8">Billedtekst:</td>
            <td>
                <asp:TextBox ID="TextBoxBilledtekst" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="400px"></asp:TextBox>
            </td>
        </tr>
    </table>

    <br />
    <br />
    Husk! Læs produktomtalen grundigt igennem, da den bliver uploaded direkte til messens hjemmeside.<br />
    <asp:Button ID="ButtonUpload" runat="server" Height="29px" Text="Upload produktomtale" Width="139px" />
    <br />
    <br />
    <br />
    <a href="Default.aspx">Tilbage til forsiden</a>
</div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
