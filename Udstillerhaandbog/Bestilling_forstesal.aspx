﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Bestilling_forstesal.aspx.vb" Inherits="Udstillerhaandbog.Bestilling_forstesal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Bestilling af 1. sal</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
       <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
    <h2>Bestilling af 1. sal</h2>
    <br />
    Deadline for bestilling: <strong>2. december 2013</strong><br />
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
        <ContentTemplate>
            <table class="style20">
                <tr>
                    <td>
                        Standnr.</td>
                    <td class="style30">
                        <asp:TextBox ID="TextBoxForsteStandNummer" runat="server" TabIndex="61" 
                    ReadOnly="True" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" 
                        ControlToValidate="TextBoxForsteStandNummer" ErrorMessage="*" 
                            ValidationGroup="40"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style31">
                        Antal kvm.</td>
                    <td>
                        <asp:TextBox ID="TextBoxForsteKvm" runat="server" TabIndex="62" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" 
                        ControlToValidate="TextBoxForsteKvm" ErrorMessage="*" ValidationGroup="40"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator5" runat="server" 
                            ControlToValidate="TextBoxForsteKvm" ErrorMessage="RangeValidator" 
                            MaximumValue="99999" MinimumValue="0" Type="Integer" ValidationGroup="40">Kun heltal</asp:RangeValidator>
                    </td>
                </tr>
            </table>
            <p>
                Pris pr. kvm. 1. sal: kr. 260,00 ekskl. moms
            </p>
            <p>
                <asp:CheckBox ID="CheckBoxForsteRegler" runat="server" 
            
            
            
                    Text="Vi ønsker at etablere 1. sal på BYGGERI' 14 og er bekendt med reglerne for standopbygning på messen, samt krav i denne forbindelse stillet af Brandvæsenet eller andre myndigheder. " 
                    AutoPostBack="True" />
                (se <a href="http://">regler for standopbygning</a>)</p>
            <p>
                <asp:CheckBox ID="CheckBoxForsteTegninger" runat="server" 
            
            Text="Konstruktionstegninger af 1. sal er sendt til MESSE C. Tegninger skal være messecentret i hænde senest:" 
            TabIndex="63" />
                &nbsp;<b>16. december 2013.</b></p>
            <p>
                <asp:Button ID="ButtonForsteSal" runat="server" 
            Height="21px" TabIndex="64" 
                        Text="Send" style="text-align: left" Enabled="False" 
                        ValidationGroup="40" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <a href="Default.aspx">Tilbage til forsiden</a>
    </div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
