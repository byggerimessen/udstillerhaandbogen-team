﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Logon.aspx.vb" Inherits="Udstillerhaandbog.Logon" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h3>
        Velkommen til udstillerhåndbogen</h3>
<table>
   <tr>
      <td>Brugernavn:</td>
      <td><input id="txtUserName" type="text" runat="server"></td>
      <td><ASP:RequiredFieldValidator ControlToValidate="txtUserName"
           Display="Static" ErrorMessage="*" runat="server" 
           ID="vUserName" /></td>
   </tr>
   <tr>
      <td>Kodeord:</td>
      <td><input id="txtUserPass" type="password" runat="server"></td>
      <td><ASP:RequiredFieldValidator ControlToValidate="txtUserPass"
          Display="Static" ErrorMessage="*" runat="server" 
          ID="vUserPass" />
      </td>
   </tr>
   <tr>
      <td>&nbsp;</td>
      <td><ASP:CheckBox id="chkPersistCookie" runat="server" autopostback="false" Visible="False" /></td>
      <td></td>
   </tr>
</table>
<input type="submit" Value="Logon" runat="server" ID="cmdLogin"><br />
        <asp:Label ID="LabelForkert" runat="server" Text="Forkert kodeord eller brugernavn" Visible="False"></asp:Label>
<asp:Label id="lblMsg" ForeColor="red" Font-Name="Verdana" Font-Size="10" runat="server" />
        <br />
    <a href="http://glemtkodeord.byggerimessen.dk/udstillerhaandbogen.aspx">Kan du ikke få adgang? klik her</a></div>

    </form>
    </body>
</html>
