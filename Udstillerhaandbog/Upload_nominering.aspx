﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="Upload_nominering.aspx.vb" Inherits="Udstillerhaandbog.Upload_nominering" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload ansøgning</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
    <style type="text/css">
        .auto-style1 {
            width: 78%;
        }
        .auto-style2 {
            width: 60%;
        }
        .auto-style6 {
            width: 136px;
        }
        .auto-style7 {
            width: 412px;
        }
        .auto-style8 {
            width: 17%;
        }
        .auto-style9 {
            width: 46px;
        }
        .auto-style10 {
            width: 100%;
        }
        .auto-style11 {
            width: 746px;
        }
        .auto-style12 {
            height: 22px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
               
        <script type="text/javascript">
            function textCounter(field, countfield, maxlimit) {
                if (field.value.length > maxlimit)
                    field.value = field.value.substring(0, maxlimit);
                else
                    countfield.value = maxlimit - field.value.length;
            }
        </script>
    <div>
         <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div> </div>
<div class="page">
<div class="sitemap"></div>
   <h2>Afsendelse af ansøgning til klimapriser</h2>
   <div style="height: 1096px">
       <br />
       Kontaktperson og email skal udfyldes for hver ansøgning:<br />
       <table class="auto-style2">
           <tr>
               <td class="auto-style6">Kontrakt nummer</td>
               <td class="auto-style7">
                   <asp:TextBox ID="TextBoxKontrakt" runat="server" ReadOnly="True" Width="400px" TabIndex="1"></asp:TextBox>
               </td>
               <td>&nbsp;</td>
           </tr>
             <tr>
               <td>Stand nr.</td>
               <td>
                   <asp:TextBox ID="TextBoxStandnr" runat="server" Width="400px" TabIndex="2" ReadOnly="True"></asp:TextBox>
               </td>
               <td></td>
           </tr>
           <tr>
               <td class="auto-style6">Firmanavn</td>
               <td class="auto-style7">
                   <asp:TextBox ID="TextBoxFirma" runat="server" ReadOnly="True" Width="400px" TabIndex="3"></asp:TextBox>
               </td>
               <td>&nbsp;</td>
           </tr>
           <tr>
               <td class="auto-style6">Kontaktperson</td>
               <td class="auto-style7">
                   <asp:TextBox ID="TextBoxKontakt" runat="server" Width="400px" TabIndex="4"></asp:TextBox>
               </td>
               <td>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="1" ControlToValidate="TextBoxKontakt"></asp:RequiredFieldValidator>
               </td>
           </tr>
           <tr>
               <td class="auto-style6">Email</td>
               <td class="auto-style7">
                   <asp:TextBox ID="TextBoxEmail" runat="server" Width="400px" TabIndex="5"></asp:TextBox>
               </td>
               <td>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="1" ControlToValidate="TextBoxEmail"></asp:RequiredFieldValidator>
               </td>
               
           </tr>
        
        
       </table>

       <h4>Hvilken pris søges:</h4><br />
       <asp:RadioButton ID="RadioButtonKLima" runat="server" Text="Byggeriets Klimapris" Checked="True" GroupName="1" TabIndex="6" ValidationGroup="1" />
       <br />
       <asp:RadioButton ID="RadioButtonEnergi" runat="server" Text="Byggeriets Energipris" GroupName="1" TabIndex="6" ValidationGroup="1" />
       <br />
       <asp:RadioButton ID="RadioButtonMiljo" runat="server" Text="Byggeriets Miljøpris" GroupName="1" TabIndex="7" ValidationGroup="1" />
       <br />
       <br />
       <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
     
        
               <h4>Vælg mellem en af de to nedenstående:</h4>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                     <ContentTemplate>
               <asp:RadioButton ID="RadioButtonProdukt" runat="server" AutoPostBack="True" Checked="True" GroupName="2" TabIndex="7" Text="Produkt eller løsning til byggeriet" ValidationGroup="1" />
       <br />
               <asp:RadioButton ID="RadioButtonStrategi" runat="server" AutoPostBack="True" GroupName="2" TabIndex="9" Text="Klima-, energi- eller miljøstrategi og/eller -indsats" ValidationGroup="1" />
               <asp:Label ID="Labelprodukt" runat="server" Text="Label"></asp:Label>
               <table class="auto-style10">
                   <tr>
                       <td class="auto-style11">
                           <asp:TextBox ID="TextBoxPris" runat="server" Height="235px" MaxLength="10000" onkeydown="textCounter(TextBoxPris, this.form.remLen, 10000);" onkeyup="textCounter(TextBoxPris, this.form.remLen, 10000);" Rows="3" TabIndex="8" TextMode="MultiLine" Width="750px" />
                       </td>
                       <td>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ControlToValidate="TextBoxPris" ErrorMessage="*" Font-Size="XX-Large" ForeColor="#CC0000" ValidationGroup="1"></asp:RequiredFieldValidator>
                       </td>
                   </tr>
               </table>
                              <table class="auto-style8">
           <tr>
               <td class="auto-style9">
                    <input readonly="readonly" type="text" name="remLen" size="3" maxlength="3" value="10000" style="width: 36px" /></td>
               <td>Tegn tilbage


       </td>
              </table>
               &nbsp;<asp:FileUpload ID="FileUpload1" runat="server" TabIndex="9" />
                  <asp:Label ID="LabelDokumentation" runat="server">Ingen dokumentation uploaded</asp:Label>
                  <table class="auto-style1">
                   <tr>
               <td class="auto-style12">
                   <asp:Label ID="LabelForkertformat" runat="server" ForeColor="#CC0000"></asp:Label>
                   <asp:Label ID="LabelFilnavn" runat="server"></asp:Label>
               </td>
               <td class="auto-style12">
                       </td>

           </tr>
       </table>
           </ContentTemplate>
       </asp:UpdatePanel>      
                  <asp:Button ID="ButtonUpload" runat="server" CausesValidation="False" Height="21px" TabIndex="10" Text="Upload fil" Width="83px" />
                   &nbsp;(virker kun hvis produkt eller løsning til byggeriet)<br />
       <asp:Label ID="LabelHusk" runat="server" ForeColor="#CC0000"></asp:Label>
       <br />
           <br />            
       <asp:Button ID="ButtonSend" runat="server" Text="Send ansøgning" Width="132px" TabIndex="11" ValidationGroup="1" /><br />
       Der sendes en bekræftelses Email til indtastede Email
<br />
       <br />
       <br />
       <a href="nominering.aspx">Tilbage til nomineringsforsiden</a>
</div>
    </div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
     
         <p> ARRANGØR:<br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>        
    </form>
</body>
</html>
