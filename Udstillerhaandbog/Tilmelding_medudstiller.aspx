﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Tilmelding_medudstiller.aspx.vb" Inherits="Udstillerhaandbog.Tilmelding_medudstiller" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Tilmelding af medudstillere</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
        .auto-style2 {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1>Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
    <h2>Tilmelding af medudstillere</h2>
    <br />
    Deadline for bestilling: <strong>18. november 2013</strong>
    <br />
    <br />
    Vi tilmelder hermed følgende firma(er), der vil udstille på vores stand på BYGGERI' 14 som medudstillere:
    <br />
     <table>
                <tr>
                    <td >
                        Firmanavn</td>
                    <td >
                        <asp:TextBox ID="TextBoxTilFirmaNavn" runat="server" Width="250px" 
                    TabIndex="40" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                        ControlToValidate="TextBoxTilFirmaNavn" ErrorMessage="*" 
                        ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Standnr.</td>
                    <td class="style17">
                        <asp:TextBox ID="TextBoxTilStandnr" runat="server" Width="250px" 
                    ReadOnly="True" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" 
                        ControlToValidate="TextBoxTilStandnr" ErrorMessage="*" 
                        ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Adresse</td>
                    <td>
                        <asp:TextBox ID="TextBoxTilAdresse" runat="server" Width="250px" TabIndex="41" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" 
                        ControlToValidate="TextBoxTilAdresse" ErrorMessage="*" 
                        ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                       </td>
                    <td>
                       </td>
                    <td>
                       </td>
                </tr>
                <tr>
                    <td>
                        Postnummer</td>
                    <td>
                        <asp:TextBox ID="TextBoxTilPostNummer" runat="server" Width="250px" 
                    TabIndex="42" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                        ControlToValidate="TextBoxTilPostNummer" ErrorMessage="*" 
                        ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        By</td>
                    <td>
                        <asp:TextBox ID="TextBoxTilBy" runat="server" Width="250px" TabIndex="43" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" 
                        ControlToValidate="TextBoxTilBy" ErrorMessage="*" ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        Telefon</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBoxTilTelefon" runat="server" Width="250px" TabIndex="44" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td class="auto-style1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" 
                        ControlToValidate="TextBoxTilTelefon" ErrorMessage="*" 
                        ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                    <td class="auto-style1">
                        Fax</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBoxTilFax" runat="server" Width="250px" TabIndex="45" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td class="auto-style1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" 
                        ControlToValidate="TextBoxTilFax" ErrorMessage="*" ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Firma E-mail</td>
                    <td>
                        <asp:TextBox ID="TextBoxTilFirmaEmail" runat="server" TabIndex="44" 
                            Width="250px" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                       </td>
                    <td>
                       </td>
                    <td>
                        webside</td>
                    <td>
                        <asp:TextBox ID="TextBoxTilWebside" runat="server" Width="250px" TabIndex="47" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" 
                        ControlToValidate="TextBoxTilWebside" ErrorMessage="*" 
                        ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                <td>Kontaktperson</td>
                <td>
                    <asp:TextBox ID="TextBoxTilKontaktperson" runat="server" TabIndex="46" 
                        Width="250px" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" 
                        ControlToValidate="TextBoxTilKontaktperson" ErrorMessage="*" 
                        ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                <td>E-mail</td>
                <td>
                    <asp:TextBox ID="TextBoxTilEmail" runat="server" TabIndex="46" Width="250px" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" 
                            ControlToValidate="TextBoxTilEmail" ErrorMessage="*" ValidationGroup="30"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ButtonSendMedudstillere" runat="server" Height="21px" TabIndex="48" 
                        Text="Send" style="text-align: left" ValidationGroup="30" />
                       </td>
                    <td>
                       </td>
                    <td>
                       </td>
                    <td>
                        &nbsp;</td>
                    <td>
                       </td>
                    <td>
                        <br />
                    </td>
                </tr>
            </table>
    <br />
    Tryk send for <span class="auto-style2"><strong>hver</strong></span> medudstiller.<br />
    <br />
    <b>NB!</b> Under hensyn til messens særlige koncept – en fagmesse, arrangeret af fagfolk for fagfolk – med Danske Byggecentre som arrangør, vil deltagelse på BYGGERI&#39; 14 være betinget af Danske Byggecentres endelige accept, jf. <a href="http://">”Betingelser for deltagelse i BYGGERI&#39; 14”</a>.
    <br /><br />
    <a href="Default.aspx">Tilbage til forsiden</a>
</div>
         <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>