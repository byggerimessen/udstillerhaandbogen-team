﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Varegrupper.aspx.vb" Inherits="Udstillerhaandbog.Varegrupper" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Varegruppefortegnelse</title>
     <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
    <h2>Varegruppefortegnelse (varegrupper A-D)</h2>
    <br />
    I katalogets afdeling for varegrupper skal vi optages under følgende - max. 15. 
    varegrupper: Nederst på side 7 kan anføres forslag til yderligere varegrupper.
    Der er i alt 6 sider, klik på næste side for at komme videre. 
    Efter valg af grupperne, skal du godkende valget på side 7. Der kan højst vælges 
    15 varegrupper.<br />
    <br />
    <table class="style1">
        <tr>
            <td>
                <b>Varegruppefortegnelsen bedes venligst udfyldt og godkendt på side 7 senest 18. november.</b></td>
            <td align=right>
                <asp:LinkButton ID="LinkButton2" runat="server" style="text-align: right" PostBackUrl="~/VaregrupperE-H.aspx">Næste side</asp:LinkButton>
            </td>
        </tr>
    </table>
    <br />
  
        <br />
    <table>
        <tr>
            <td>
                Firmanavn</td>
            <td>
                <asp:TextBox ID="TextBoxFirmaNavn" runat="server" Width="220px" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
                &nbsp;Stand nummer</td>
            <td>
                <asp:TextBox ID="TextBoxStandNummer" runat="server" Width="107px" BorderStyle="Inset" BorderWidth="1px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
     
    </table>
       <hr />
   <br />
     
    <table class="style1">
    <tr>
    <td width=300><h4>&nbsp; A</h4></td>
    <td width=300><h4>&nbsp; B</h4></td>
    <td width=300><h4>&nbsp; C</h4></td>
    <td width=300><h4>&nbsp; D</h4></td>
    </tr>
        <tr valign=top>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListA" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListB" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListC" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListD" runat="server">
                </asp:CheckBoxList>
            </td>
        </tr>
    </table>
       <hr />
    <p align=right>&nbsp;Side1, <asp:LinkButton ID="LinkButton1" runat="server" style="text-align: right"
        PostBackUrl="~/VaregrupperE-H.aspx">Næste 
    side</asp:LinkButton></p>
    
    </div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
