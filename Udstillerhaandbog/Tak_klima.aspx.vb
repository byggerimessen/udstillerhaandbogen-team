﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports System.Data.OleDb
Imports APNSoft.WebControls
Imports System.Data.SqlClient

Public Class Tak_klima
    Inherits System.Web.UI.Page
    Function ConnectionStringbyge()
        Dim DB As New DB
        Dim Cstring As String = DB.ConnectionString
        Return (Cstring)
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Apply event-handling method
        AddHandler MyDatagrid.CellValueChanged, AddressOf myDataGrid_CellValueChanged
        Dim TBNummer As String = Web.HttpContext.Current.User.Identity.Name
        'Define SQL query
        Dim SqlQuery As String = "SELECT * FROM NomineringsPriser WHERE '" & TBNummer & "' = TBNummer order by dato"

        Dim conn As New SqlConnection()
        Dim myAdapter As New SqlDataAdapter()
        Dim myDataSource As New DataTable()

        conn.ConnectionString = ConnectionStringbyge()
        myAdapter.SelectCommand = New SqlCommand(SqlQuery, conn)


        Try
            conn.Open()
            myAdapter.Fill(myDataSource)
        Finally
            myAdapter.Dispose()
            conn.Close()
            conn = Nothing
        End Try

        'Set the data source
        MyDatagrid.KeyFieldName = "ID"
        MyDatagrid.DataSource = myDataSource
        MyDatagrid.DataBind()

        'Disable Cell Editing for the CustomerID
        MyDatagrid.Columns("ID").CellEditingEnabled = False
        MyDatagrid.Columns("Dato").CellEditingEnabled = False
        MyDatagrid.Columns("TBNummer").CellEditingEnabled = False
        MyDatagrid.Columns("PrisType").CellEditingEnabled = False
        MyDatagrid.Columns("Filnavn").CellEditingEnabled = False
        MyDatagrid.Columns("IndberetType").CellEditingEnabled = False
        MyDatagrid.CellEditingEnabled = True

    End Sub

    Private Sub myDataGrid_CellValueChanged(ByVal sender As Object, ByVal e As DataGridEventArgs)

        'Get Grid
        Dim myGrid As APNSoftDataGrid = CType(sender, APNSoftDataGrid)

        'Get Row
        Dim myGridRow As GridRow = e.GridRow
        If myGridRow Is Nothing Then
            Return
        End If

        'Get cell data
        Dim myGridColumn As GridColumn = e.GridColumn
        Dim CellValue As String = e.CellValue

        'Declare db objects
        Dim conn As New SqlConnection()
        conn.ConnectionString = ConnectionStringbyge()

        'Create SQL Query
        Dim SQL As String = ("UPDATE NomineringsPriser SET " + myGridColumn.ColumnName & " = @Column WHERE ") + myGrid.KeyFieldName & " = @RowID;"

        'Create command
        Dim cmd As New SqlCommand(SQL, conn)

        'Add parameters
        cmd.Parameters.Add("@Column", SqlDbType.VarChar).Value = CellValue
        cmd.Parameters.Add("@RowID", SqlDbType.VarChar).Value = myGridRow.RowID

        'Execute the query
        conn.Open()
        Try
            cmd.ExecuteNonQuery()
        Finally
            cmd.Dispose()
            conn.Close()
        End Try
    End Sub

End Class