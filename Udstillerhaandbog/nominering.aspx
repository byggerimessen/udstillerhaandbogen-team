﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="nominering.aspx.vb" Inherits="Udstillerhaandbog.nominering" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Nominering til Klimapriser</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="header">
    <div class="header-container">
        <div class="header-text">
            <br />
        <h1>Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div> 
<div class="page">
<div class="sitemap"></div>
    <br />
    <h2>Byggeriets Klimapris, Byggeriets Energipris og Byggeriets Miljøpris uddeles på BYGGERI ´14</h2>

    <br />
    
    <br />
   <h4> <strong>Er det DIT firma, der i 2014 skal modtage en af disse priser ?</strong></h4>
    Med uddeling af ovennævnte priser på BYGGERI ´14 ønsker Danske Byggecentre fortsat at:<br />
<ul class="start"><li>fremme udviklingen af byggevarer med bedre energi- og miljø¬mæssige egenskaber 
   til gavn for klimaet</li>
    <li>sætte fokus på virksomheder, der har iværksat en klima-, energi- og/eller miljøstrategi</li>
</ul>
    <h4><strong>Hvem kan søge om priserne ?</strong></h4>

Nominering til de tre priser er kun mulig for produkter og løsninger, der udstilles på BYGGERI ´14, og de skal være mindre end to år gamle. <br />
    Udstillere må gerne indsende flere ansøgninger, såfremt de har mere end et produkt, der opfylder nedenstående krav.
  
    <br />
    <br />
    <div style="text-align:left">
<button style="width: 450px; text-align:left;"  title="Krav til nominering" onclick="window.location.href='http://udstillerhaandbog.byggerimessen.dk/Krav_nominering.aspx'" type="button" value="Wide button"> 1. Se krav til nominering her</button><br />
<button style="width: 450px; text-align:left;" title="Afsendelse af ansøgning til klimapriser" onclick="window.location.href='http://udstillerhaandbog.byggerimessen.dk/Upload_nominering.aspx'" type="button" value="Wide button"> 2. Udarbejd og afsend/upload din ansøgning med dokumentation her</button><br />
        <button style="width: 450px; text-align:left;"  title="Ansøgningsvejledning" onclick="window.location.href='http://udstillerhaandbog.byggerimessen.dk/Tak_klima.aspx'" type="button" value="Wide button"> 3. Se tidligere ansøgninger</button><br />

    </div>
    <!--
      <ol class="krav">
        <li>Se krav til nominering <a href="Krav_nominering.aspx">her</a></li>
        <li>Se vejledning i ansøgning af klimapriser <a href="Vejledning_klimapriser.aspx">her</a></li>
        <li>Udarbejd og afsend/upload din ansøgning med dokumentation <a href="Upload_nominering.aspx">her</a></li>
    </ol>
    -->
    <h4><strong>Deadline for indsendelse af ansøgning er: 4. november 2013</strong></h4>
    <br />
       Skulle du have problemer med ansøgningen, er du velkommen til at kontakte Ingelise Alsø på <a href="mailto:ia@danskebyggecentre.dk">ia@danskebyggecentre.dk</a> eller 45 80 78 77.
<h4><strong>Nomineringskomité</strong></h4>
Nomineringskomitéen nominerer produkter og løsninger til de tre klimapriser samt udpeger modtagerne af priserne. Nomineringskomitéen består af repræsentanter fra samtlige byggeriets parter. 
    <br />
    <br />
    Se oversigt over komitéens repræsentanter <a href="Komite.aspx">her</a><br />
        </div>
    <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
      
        <p >ARRANGØR:<br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
