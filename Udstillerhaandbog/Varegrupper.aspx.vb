﻿Imports System.Data
Public Class Varegrupper
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '    If Not Page.IsPostBack Then
        FillCheckboxes()
        '        checked()
        '        Delete()
        '        GetFirmaNavn()
        '    End If
        'End Sub

        'Sub checked()
        '    Dim CheckBoxAlreadyChecked As New DBVareGruppe
        '    CheckBoxAlreadyChecked.AlreadyChecked(CheckBoxListA, CheckBoxListB, CheckBoxListC, CheckBoxListD)
        'End Sub
        'Sub GetFirmaNavn()
        '    'Dim GetData As New StamData
        '    Dim liste As New List(Of String)
        '    'liste = GetData.HentData
        '    TextBoxFirmaNavn.Text = liste.Item(0)
        '    TextBoxStandNummer.Text = liste.Item(7)
    End Sub
    Sub FillCheckboxes()

        Dim VareGruppeA = New DataSet
        VareGruppeA.ReadXml(MapPath("\varegrupper\varegrupperA.xml"))
        CheckBoxListA.DataSource = VareGruppeA
        CheckBoxListA.DataTextField = "textA"
        CheckBoxListA.DataBind()
        VareGruppeA.Dispose()

        Dim VareGruppeB = New DataSet
        VareGruppeB.ReadXml(MapPath("\varegrupper\varegrupperB.xml"))
        CheckBoxListB.DataSource = VareGruppeB
        CheckBoxListB.DataTextField = "text"
        CheckBoxListB.DataBind()
        VareGruppeB.Dispose()

        Dim VareGruppeC = New DataSet
        VareGruppeC.ReadXml(MapPath("\varegrupper\varegrupperC.xml"))
        CheckBoxListC.DataSource = VareGruppeC
        CheckBoxListC.DataTextField = "textC"
        CheckBoxListC.DataBind()

        Dim VareGruppeD = New DataSet
        VareGruppeD.ReadXml(MapPath("\varegrupper\varegrupperD.xml"))
        CheckBoxListD.DataSource = VareGruppeD
        CheckBoxListD.DataTextField = "text"
        CheckBoxListD.DataBind()

    End Sub

    'Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LinkButton1.Click
    '    NextPage()
    'End Sub

    'Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LinkButton2.Click
    '    NextPage()
    'End Sub

    'Sub Delete()
    '    Dim delete As New DBVareGruppe
    '    delete.DeleteCheckedTemp("1")
    'End Sub

    'Sub NextPage()
    '    Dim InsertDBTemp As New DBVareGruppe

    '    InsertDBTemp.AddToliste(CheckBoxListA, "1", False)
    '    InsertDBTemp.AddToliste(CheckBoxListB, "1", False)
    '    InsertDBTemp.AddToliste(CheckBoxListC, "1", False)
    '    InsertDBTemp.AddToliste(CheckBoxListD, "1", False)

    '    Response.Redirect("/varegrupperE-H.aspx")
    'End Sub
End Class
