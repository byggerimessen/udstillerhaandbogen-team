﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VaregrupperV-AA.aspx.vb" Inherits="Udstillerhaandbog.VaregrupperV_AA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Varegruppefortegnelse V-Å</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
   <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
     <h2>Varegruppefortegnelse (varegrupper V - Å)</h2>
    <br />
    <table class="style1">
        <tr>
            <td>
  <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/VaregrupperR-U.aspx">Forrige side</asp:LinkButton>
    &nbsp;Husk at godkende på side 7</td>
            <td align=right>
                <asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/VaregrupperTak.aspx">Næste side</asp:LinkButton>
            </td>
        </tr>
    </table>
 <br />
    <hr />
    <div>
    <table class="style1">
    <tr>
    <td width=300><h4>&nbsp; V</h4></td>
    <td width=300><h4>&nbsp; Y</h4></td>
    <td width=300><h4>&nbsp; Ø</h4></td>
    <td width=300><h4>&nbsp; Å</h4></td>
    </tr>                       
        <tr valign=top>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListV" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListY" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListØ" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListÅ" runat="server">
                </asp:CheckBoxList>
            </td>
        </tr>
    </table>
    </div>
    <hr />
        <table class="style1">
            <tr>
                <td>
  <asp:LinkButton ID="LinkButton5" runat="server" PostBackUrl="~/VaregrupperR-U.aspx">Forrige side</asp:LinkButton>
                </td>
                <td align=right>
                    Side 6, 
                <asp:LinkButton ID="LinkButton6" runat="server" PostBackUrl="~/VaregrupperTak.aspx">Næste side</asp:LinkButton>
            &nbsp;</td>
            </tr>
    </table>
    </div>
         <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
