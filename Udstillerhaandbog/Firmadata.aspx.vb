﻿Public Class Firmadata
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetData()
            '  AddOverskriftToTable()
            '  GridviewData()
        End If
    End Sub

    Sub GetData()
        Dim DB As New DBStamData
        Dim Liste As New List(Of GetFirmaData)
        Liste = DB.HentData
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        TextBoxKontraktNummer.Text = KontraktNummer

        For Each item As GetFirmaData In Liste
            TextBoxFirmaNavn.Text = item.Firma
            TextBoxAdresse.Text = item.Adresse
            TextBoxPostNummer.Text = item.PostNummer
            TextBoxTelefon.Text = item.Telefon
            TextBoxFirmaEmail.Text = item.FirmaEmail
            TextBoxUdfyldtAf.Text = item.Navn
            TextBoxKontaktEmail.Text = item.Email
            TextBoxStandNummer.Text = item.StandNummer
            TextBoxBy.Text = item.City
            TextBoxFax.Text = item.Fax
            TextBoxWebside.Text = item.FirmaWebside
        Next

    End Sub

    Protected Sub ButtonSend_Click(sender As Object, e As EventArgs) Handles ButtonSend.Click
        Dim DB As New DBStamData

        Dim Liste As New List(Of GetFirmaData)
        Liste.Add(New GetFirmaData(TextBoxFirmaNavn.Text, TextBoxAdresse.Text, TextBoxPostNummer.Text, TextBoxBy.Text, TextBoxUdfyldtAf.Text, TextBoxTelefon.Text, TextBoxKontaktEmail.Text, TextBoxStandNummer.Text, TextBoxKontraktNummer.Text, TextBoxWebside.Text, TextBoxFirmaEmail.Text, TextBoxFax.Text))

        Dim ListeKontakter As New List(Of KontaktPersoner)
        ListeKontakter.Add(New KontaktPersoner(TextBoxStandansvarlig.Text, TextBoxMarketingansvarlig.Text, TextBoxPresseansvarlig.Text))

    End Sub

    Protected Sub ButtonAdd_Click(sender As Object, e As EventArgs) Handles ButtonAdd.Click

        Dim Navn As String = TextBoxMesseNavn.Text
        Dim Email As String = TextBoxMesseEmail.Text

        '  AddRowToTable(Navn, Email)
        '   AddRowToTable(Navn, Email)
        LabelAdd.Text = Navn + " er tilføjet med email'en: " + Email
        TextBoxMesseNavn.Text = ""
        TextBoxMesseEmail.Text = ""

        Dim DB As New DBStamData

        DB.SendMesseNyt(Navn, Email)
        GridviewData()


    End Sub
    Sub GridviewData()
        Dim DB As New DBStamData

        Dim Liste As New List(Of MesseNyt)
        Liste = DB.HentMesseNyt

        GridView1.DataSource = Liste
        GridView1.DataBind()
    End Sub

    Sub AddRowToTable(ByVal Navn As String, ByVal Email As String)

        Dim row As New TableRow
        '  TableModtager.Rows.Add(row)

        Dim Cell As New TableCell
        Dim Cell2 As New TableCell

        Cell.Text = Navn
        Cell2.Text = Email

        row.Cells.Add(Cell)
        row.Cells.Add(Cell2)

    End Sub

    Sub AddOverskriftToTable()

        Dim row As New TableRow
        ' TableModtager.Rows.Add(row)

        Dim cell As New TableCell
        Dim Cell1 As New TableCell
        Dim Cell2 As New TableCell

        cell.Text = "Tilføjede Personer"
        Cell1.Text = "Navn"
        Cell2.Text = "Email"

        row.Cells.Add(cell)
        row.Cells.Add(Cell1)
        row.Cells.Add(Cell2)

    End Sub
End Class