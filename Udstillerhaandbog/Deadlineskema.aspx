﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Deadlineskema.aspx.vb" Inherits="Udstillerhaandbog.Deadlineskema" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Deadline skema</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
    <style type="text/css">
        .auto-style2 {
            width: 666px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
            </div> 
<div class="page">
    <h2>Deadline skema</h2>
    <br />
    <table class="deadline"">
        <tr>
            <td class="deadline"><h4>Aktivitet</h4></td>
            <td class="deadline"><h4>Deadline</h4></td>
        </tr>          
        <tr>           
            <td class="deadline"><strong>Ansøgning af klimapriser</strong> <br />- se <a href="Upload_nominering.aspx">afsnit 1</a></td>
            <td class="deadline">4. november 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Tilmelding af medudstiller(e) <br /> </strong>- se <a href="Tilmelding_medudstiller.aspx">afsnit 4</a></td>
            <td class="deadline">18. november 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Indsendelse af pressemateriale </strong> <br />- se <a href="Udarbejd_presse.aspx">afsnit 3</a></td>
            <td class="deadline">18. november 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Bestilling af PR-materiale, adgangskort, messeavis m.m </strong> <br />- se <a href="BestillingAdgangskortPR.aspx">afsnit 4</a></td>
            <td class="deadline">18. november 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Optagelse i messekataloget </strong> <br />- se <a href="Optagelse_messe.aspx">afsnit 4</a></td>
            <td class="deadline">18. november 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Bestilling af logo/annonce i messekataloget</strong> <br />- se <a href="http://">afsnit 5</a></td>
            <td class="deadline">18. november 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Bestilling af link til egen hjemmeside</strong> <br />- se <a href="http://">afsnit 5</a></td>
            <td class="deadline">18. november 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Bestilling af 1. sal </strong> <br />- se <a href="Bestilling_forstesal.aspx">afsnit 4</a></td>
            <td class="deadline">2. december 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Færdigt materiale til logo eller annonce i messekataloget</strong> <br />- se <a href="http://">afsnit 5 </a></td>
            <td class="deadline">11. december 2013</td>
            
        </tr>
        <tr>
            <td class="deadline"><strong>Konstruktionstegninger af 1. sal til MESSE C</strong> <br />- se <a href="Bestilling_forstesal.aspx">afsnit 4</a></td>
            <td class="deadline">16. december 2013</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Bestillingsskemaer og standskitse til MESSE C</strong> <br />- se <a href="http://">afsnit 6</a></td>
            <td class="deadline">20. januar 2014</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Start opbygning i MESSE C </strong> <br />- se <a href="http://">afsnit 6</a></td>
            <td class="deadline">17. februar 2014 kl. 8.00</td>
        </tr>
        <tr>
            <td class="deadline"><strong>Afsluttet nedtagning i MESSE C</strong> <br />- se <a href="http://">afsnit 6</a></td>
            <td class="deadline">4. marts 2014 kl. 21.00</td>
        </tr>
    </table>
    <br />
    <a href="Default.aspx">Tilbage til forsiden</a>
    </div></div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
