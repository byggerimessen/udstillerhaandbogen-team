﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Public Class DBVareGruppe
    Function ConnectionString()
        Dim GetCString As New DB
        Dim Cstring As String = GetCString.ConnectionString
        Return (Cstring)
    End Function

    Sub VareGruppeTemp(ByVal Liste As List(Of String), ByVal VareGruppeLetter As String, ByVal Approved As Boolean)

        Dim cmd As New SqlCommand
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Dato As DateTime = Date.Now
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        For Each VareGruppe As String In Liste

            cmd = New SqlCommand("INSERT INTO VareGruppeTemp (KontraktNummer, VareGruppeItem, VareGruppeLetter, Approved) VALUES (@KontraktNummer, @VareGruppeItem, @VareGruppeLetter, @Approved)", cn)

            Dim KontraktNummerParam As New SqlParameter("@KontraktNummer", SqlDbType.NVarChar, 50)
            Dim VareGruppeParam As New SqlParameter("@VareGruppeItem", SqlDbType.NVarChar, 500)
            Dim VareGruppeLetterParam As New SqlParameter("@VareGruppeLetter", SqlDbType.NChar, 10)
            Dim ApprovedParam As New SqlParameter("@Approved", SqlDbType.Bit)

            cmd.Parameters.Add(KontraktNummerParam)
            cmd.Parameters.Add(VareGruppeParam)
            cmd.Parameters.Add(VareGruppeLetterParam)
            cmd.Parameters.Add(ApprovedParam)

            KontraktNummerParam.Value = KontraktNummer
            VareGruppeParam.Value = VareGruppe
            VareGruppeLetterParam.Value = VareGruppeLetter
            ApprovedParam.Value = Approved

            Try
                cn.Open()
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                '  MsgBox(ex.ToString())
            Finally
                If Not cn Is Nothing Then
                    cn.Close()
                End If
            End Try
        Next

    End Sub

    Function GetVareGruppeTemp()
        Dim Liste As New List(Of String)
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        Dim cmd As New SqlCommand("SELECT * FROM VareGruppeTemp WHERE '" & KontraktNummer & "' = KontraktNummer", cn)
        Dim reader As SqlDataReader

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While reader.Read()
                Try
                    If Not reader.IsDBNull(reader.GetOrdinal("VareGruppeItem")) Then
                        Dim FaxNummer As String = CStr(reader("VareGruppeItem"))
                        Liste.Add(FaxNummer)
                    Else
                        Liste.Add("")
                    End If
                Catch ex As Exception
                End Try
            End While
        Catch ex As Exception
            ' MsgBox(ex.ToString())
        Finally
        End Try
        Return (Liste)
    End Function

    Sub AddToliste(ByVal CheckBoxListe As CheckBoxList, ByVal Bogstav As String, ByVal Approved As Boolean)

        Dim Liste As New List(Of String)
        Dim i As Integer

        For i = 0 To CheckBoxListe.Items.Count - 1
            If CheckBoxListe.Items(i).Selected Then
                Liste.Add(CheckBoxListe.Items(i).Value)
            Else
            End If
        Next

        VareGruppeTemp(Liste, Bogstav, Approved)
    End Sub

    Sub AlreadyChecked(ByVal CheckBoxList1 As CheckBoxList, ByVal CheckBoxList2 As CheckBoxList, _
                       ByVal CheckBoxList3 As CheckBoxList, ByVal CheckBoxList4 As CheckBoxList)

        Dim CheckedVarer As New DBVareGruppe
        Dim Liste As New List(Of String)
        Liste = GetVareGruppeTemp()

        For Each item As String In Liste
            Dim i As Integer
            For i = 0 To CheckBoxList1.Items.Count - 1
                Dim CheckBoxItem As String = CheckBoxList1.Items(i).Value.Trim
                If CheckBoxItem = item.Trim Then
                    '    MsgBox(item + " " + CheckBoxItem)
                    CheckBoxList1.Items(i).Selected = True
                End If
            Next
            For i = 0 To CheckBoxList2.Items.Count - 1
                Dim CheckBoxItem As String = CheckBoxList2.Items(i).Value.Trim
                If CheckBoxItem = item.Trim Then
                    '    MsgBox(item + " " + CheckBoxItem)
                    CheckBoxList2.Items(i).Selected = True
                End If
            Next
            For i = 0 To CheckBoxList3.Items.Count - 1
                Dim CheckBoxItem As String = CheckBoxList3.Items(i).Value.Trim
                If CheckBoxItem = item.Trim Then
                    '    MsgBox(item + " " + CheckBoxItem)
                    CheckBoxList3.Items(i).Selected = True
                End If
            Next
            For i = 0 To CheckBoxList4.Items.Count - 1
                Dim CheckBoxItem As String = CheckBoxList4.Items(i).Value.Trim
                If CheckBoxItem = item.Trim Then
                    '    MsgBox(item + " " + CheckBoxItem)
                    CheckBoxList4.Items(i).Selected = True
                End If
            Next
        Next
    End Sub

    Sub DeleteCheckedTemp(ByVal VareGruppeItem As String)

        Dim cmd As New SqlCommand
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Dato As DateTime = Date.Now
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        cmd = New SqlCommand("DELETE FROM VareGruppeTemp WHERE KontraktNummer = '" & KontraktNummer & "' AND VaregruppeLetter = '" & VareGruppeItem & "'", cn)

        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.ToString())
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try
    End Sub

    Sub ApprovedTrue(ByVal liste As List(Of String))

        Dim cmd As New SqlCommand
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        For Each VareGruppeItem As String In liste
            Dim Cstring As String = ConnectionString()
            Dim cn As New SqlConnection(Cstring)
            cmd = New SqlCommand("UPDATE VareGruppeTemp SET Approved = @Approved WHERE KontraktNummer = '" & KontraktNummer & "'  AND VaregruppeItem = '" & VareGruppeItem & "'", cn)

            Dim ApprovedParam As New SqlParameter("@Approved", SqlDbType.Bit)
            cmd.Parameters.Add(ApprovedParam)
            ApprovedParam.Value = True

            Try
                cn.Open()
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                '  MsgBox(ex.ToString())
            Finally
                If Not cn Is Nothing Then
                    cn.Close()
                End If
            End Try
        Next
    End Sub

    Sub Forslag(ByVal Forslag As String)
        DeleteVareGrupper()
        Dim cmd As New SqlCommand
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Dato As DateTime = Date.Now
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)

        cmd = New SqlCommand("INSERT INTO VareGruppeForslag (KontraktNummer, VareGruppeForslag) VALUES (@KontraktNummer, @VareGruppeForslag)", cn)

        Dim KontraktNummerParam As New SqlParameter("@KontraktNummer", SqlDbType.NVarChar, 50)
        Dim VareGruppeForslagParam As New SqlParameter("@VareGruppeForslag", SqlDbType.NVarChar, 500)

        cmd.Parameters.Add(KontraktNummerParam)
        cmd.Parameters.Add(VareGruppeForslagParam)

        KontraktNummerParam.Value = KontraktNummer
        VareGruppeForslagParam.Value = Forslag

        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            '  MsgBox(ex.ToString())
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try

    End Sub

    Sub DeleteNotTrue()
        Dim cmd As New SqlCommand
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Dato As DateTime = Date.Now
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        cmd = New SqlCommand("DELETE FROM VareGruppeTemp WHERE KontraktNummer = '" & KontraktNummer & "' AND Approved = 0", cn)

        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.ToString())
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try
    End Sub

    Sub AddTolisteGodkend(ByVal CheckBoxListe As CheckBoxList)

        Dim Liste As New List(Of String)
        Dim i As Integer
        For i = 0 To CheckBoxListe.Items.Count - 1
            Liste.Add(CheckBoxListe.Items(i).Value)
        Next
        ApprovedTrue(Liste)
    End Sub

    Sub DeleteVareGrupper()

        Dim cmd As New SqlCommand
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        cmd = New SqlCommand("DELETE FROM VareGruppeForslag WHERE KontraktNummer = '" & KontraktNummer & "'", cn)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.ToString())
        Finally
            If Not cn Is Nothing Then
                cn.Close()
            End If
        End Try

    End Sub

    Sub GetVareGruppeForslag(ByVal TextBoxForslag As TextBox)
        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim liste As New List(Of Integer)
        Dim Cstring As String = ConnectionString()
        Dim cn As New SqlConnection(Cstring)
        Dim cmd As New SqlCommand("SELECT * FROM VareGruppeForslag WHERE '" & KontraktNummer & "' = KontraktNummer", cn)
        Dim reader As SqlDataReader
        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While reader.Read()

                If Not reader.IsDBNull(reader.GetOrdinal("VareGruppeForslag")) Then
                    Dim VareGruppeForslag As String = CStr(reader("VareGruppeForslag"))
                    TextBoxForslag.Text = VareGruppeForslag
                End If
            End While
        Catch ex As Exception
            'MsgBox(ex.ToString())
        Finally
        End Try
    End Sub
End Class
