﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Firmadata.aspx.vb" Inherits="Udstillerhaandbog.Firmadata" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Firmadata</title>
     <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
    <style type="text/css">
        .auto-style1 {
            width: 78%;
        }
        .auto-style2 {
            width: 893px;
        } 
        .auto-style3 {
            width: 146px;
        }
        .auto-style11 {
            width: 112px;
        }
        .auto-style12 {
            width: 49%; 
        }
        .auto-style13 {
            width: 266px;
        }
        .auto-style14 {
            width: 6px;
        }
        .auto-style15 {
            width: 153px;
        }
        .auto-style16 {
            width: 23px;
        }
        .auto-style17 {
            height: 24px;
        }
        .auto-style18 {
            width: 28px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div> </div>
<div class="page">
    <h2>Oplysninger om firmaet - Check</h2>
    <p>Nedenstående er de firmadata, vi har registreret, og som vil fremgå i messekataloget, udstillerlister, produktomtaler m.v. Ret forkerte data.</p>
 <!-- her kommer bokse ind fra 2012 !-->
    <table>
    <tr>
        <td>
            Firmanavn</td>
        <td>
            <asp:TextBox ID="TextBoxFirmaNavn" runat="server" TabIndex="3" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
        <td class="auto-style11">
            Stand nummer</td>
        <td class="auto-style12">
            <asp:TextBox ID="TextBoxStandNummer" runat="server" ReadOnly="True" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Adresse</td>
        <td>
            <asp:TextBox ID="TextBoxAdresse" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
        <td class="auto-style11">
            &nbsp;</td>
        <td class="auto-style12">
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            Postnr.</td>
        <td>
            <asp:TextBox ID="TextBoxPostNummer" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
        <td class="auto-style11">
            By</td>
        <td class="auto-style12">
            <asp:TextBox ID="TextBoxBy" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Telefon</td>
        <td>
            <asp:TextBox ID="TextBoxTelefon" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
        <td class="auto-style11">
            Fax nr.</td>
        <td class="auto-style12">
            <asp:TextBox ID="TextBoxFax" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
    </tr>
   
         <tr>
        <td> Firma Email</td>
         <td>
            <asp:TextBox ID="TextBoxFirmaEmail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
       </td>
              <td class="auto-style11">
                  Firma webside</td>
        <td class="auto-style12">            
            <asp:TextBox ID="TextBoxWebside" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
        </td>
                        </tr>
       <tr>
       <td class="auto-style17">Udfyldt af</td>
       <td class="auto-style17">            
            <asp:TextBox ID="TextBoxUdfyldtAf" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
           </td>
           <td class="auto-style17">Kontrakt nummer</td>
           <td class="auto-style17">
                  <asp:TextBox ID="TextBoxKontraktNummer" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px" ReadOnly="True"></asp:TextBox>
           </td>
              </tr>
        <tr>
            <td>Kontakt Email</td>
            <td>            
            <asp:TextBox ID="TextBoxKontaktEmail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
        </tr>
      
</table>
    <h2>Oplysninger om firmaet - Udfyld</h2>
    <p>For at kunne målrette budskaber om BYGGERI ´14 til den rette person i dit firma, beder vi dig venligst oplyse følgende (kan godt være samme person):
       </p>
    <table class="auto-style1">
        <tr>
            <td class="auto-style15"><strong>Standansvarlig</strong>*</td>
            <td class="auto-style3">Navn:</td>
            <td class="auto-style13">
                <asp:TextBox ID="TextBoxStandansvarlig" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
            <td class="auto-style18">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxStandansvarlig" ErrorMessage="* udfyld" ForeColor="#CC0000" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
            <td class="auto-style14">
                Email:</td>
            <td class="auto-style16">
                <asp:TextBox ID="TextBoxStandansvarligMail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style15"><strong>Marketingansvarlig</strong>*</td>
            <td class="auto-style3">Navn:</td>
            <td class="auto-style13">
                <asp:TextBox ID="TextBoxMarketingansvarlig" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
            <td class="auto-style18">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxMarketingansvarlig" ErrorMessage="* udfyld" ForeColor="#CC0000" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
            <td class="auto-style14">
                Email:</td>
            <td class="auto-style16">
                <asp:TextBox ID="TextBoxMarketingansvarligMail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style15"><strong>Presseansvarlig</strong>*</td>
            <td class="auto-style3">Navn:</td>
            <td class="auto-style13">
                <asp:TextBox ID="TextBoxPresseansvarlig" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
            <td class="auto-style18">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxPresseansvarlig" ErrorMessage="* udfyld" ForeColor="#CC0000" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
            <td class="auto-style14">
                Email:</td>
            <td class="auto-style16">
                <asp:TextBox ID="TextBoxPresseansvarligMail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <h4>Modtagere af dagligt MESSENYT</h4>
    <p>MESSENYT udsendes i 2014 pr. mail under messen. <br />
        Venligst angiv navn og mail på de personer i jeres firma, som skal modtage den daglige udgave af MESSENYT.</p>
    

    Tilføj modtagere af MESSENYT:<br />
    <br />
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
          <ContentTemplate>
         
              Navn på modtageren:<br />
              <asp:TextBox ID="TextBoxMesseNavn" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
              <br />
              <br />
              Email på modtageren:<br />
              <asp:TextBox ID="TextBoxMesseEmail" runat="server" BorderStyle="Inset" BorderWidth="1px" Width="250px"></asp:TextBox>
              <br />
              <br />
              <asp:Button ID="ButtonAdd" runat="server" Height="21px" Text="Tilføj" />
         
    <br />
     <asp:Label ID="LabelAdd" runat="server"></asp:Label>
    
    <br />
    <asp:GridView ID="GridView1" runat="server" Width="257px">
    </asp:GridView>
                                 </ContentTemplate>
            </asp:UpdatePanel>
    

    <asp:Button ID="ButtonSend" runat="server" Text="Acceptér og send" ValidationGroup="1" />
   
         <br />
    <br />
   
         <br />
    <a href="Default.aspx">Tilbage til forsiden</a>   
</div>
    <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
