﻿Public Class Upload_nominering
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Labelprodukt.Text = " <h4>Ved produkt eller løsning til byggeriet</h4><ol class=""overskrift""><li>Giv en kort beskrivelse af egenskaberne ved din virksomheds produkt eller løsning</li><li>Giv en kort beskrivelse af, hvilke af de stillede krav dit produkt eller løsning opfylder</li><li><strong>Husk!</strong> Upload dokumentation for de krav, dit produkt eller løsning opfylder</li> </ol>"
        GetStamData()
    End Sub

    Protected Sub ButtonSend_Click(sender As Object, e As EventArgs) Handles ButtonSend.Click
        If RadioButtonProdukt.Checked = True Then
            If LabelDokumentation.Text = "Ingen dokumentation uploaded" Then
                LabelHusk.Text = "HUSK DOKUMENTATION"
            Else
                Dim PrisType As String = ""
                Dim TBNummer As String = Web.HttpContext.Current.User.Identity.Name
                Dim KontaktPerson As String = TextBoxKontakt.Text
                Dim Email As String = TextBoxEmail.Text
                Dim BeskrivelsePris As String = TextBoxPris.Text
                Dim Filnavn As String = LabelFilnavn.Text

                If RadioButtonEnergi.Checked = True Then PrisType = "Energi"
                If RadioButtonKLima.Checked = True Then PrisType = "Klima"
                If RadioButtonMiljo.Checked = True Then PrisType = "Miljø"

                Dim DB As New DB
                Dim Liste As New List(Of NomineringPris)
                Dim Indberettype As String = ""
                If RadioButtonProdukt.Checked = True Then Indberettype = "Produkt"
                If RadioButtonStrategi.Checked = True Then Indberettype = "Strategi"

                Liste.Add(New NomineringPris(KontaktPerson, Email, PrisType, BeskrivelsePris, Indberettype, Filnavn, Nothing))

                DB.SendPris(Liste)
                Dim Mail As New Mailer
                Dim Firma As String = TextBoxFirma.Text

                Dim Subject As String = "kvittering for modtaget ansøgning til Byggeriets " & PrisType & "pris fra " & Firma
                Dim Body1 As String = "Tak for din ansøgning til Byggeriets " & PrisType & "pris.<br>"
                Dim Body2 As String = "Du vil ultimo november 2013 modtage besked, om dit produkt / strategi er nomineret til prisen.<br>"
                Dim Framail As String = "nomineringer@danskebyggecentre.dk"

                Dim Body As String = Body1 + Body2
                Mail.SendMailPris(Subject, Body, Email, Framail, Framail)
                Response.Redirect("Tak_klima.aspx")
            End If
        Else
            Dim PrisType As String = ""
            Dim TBNummer As String = Web.HttpContext.Current.User.Identity.Name
            Dim KontaktPerson As String = TextBoxKontakt.Text
            Dim Email As String = TextBoxEmail.Text
            Dim BeskrivelsePris As String = TextBoxPris.Text
            Dim Filnavn As String = LabelFilnavn.Text

            If RadioButtonEnergi.Checked = True Then PrisType = "Energi"
            If RadioButtonKLima.Checked = True Then PrisType = "Klima"
            If RadioButtonMiljo.Checked = True Then PrisType = "Miljø"

            Dim DB As New DB
            Dim Liste As New List(Of NomineringPris)
            Dim Indberettype As String = ""
            If RadioButtonProdukt.Checked = True Then Indberettype = "Produkt"
            If RadioButtonStrategi.Checked = True Then Indberettype = "Strategi"

            Liste.Add(New NomineringPris(KontaktPerson, Email, PrisType, BeskrivelsePris, Indberettype, Filnavn, Nothing))

            DB.SendPris(Liste)
            Dim Mail As New Mailer
            Dim Firma As String = TextBoxFirma.Text

            Dim Subject As String = "kvittering for modtaget ansøgning til Byggeriets " & PrisType & "pris fra " & Firma
            Dim Body1 As String = "Tak for din ansøgning til Byggeriets " & PrisType & "pris.<br>"
            Dim Body2 As String = "Du vil ultimo november 2013 modtage besked, om dit produkt / strategi er nomineret til prisen.<br>"
            Dim Framail As String = "nomineringer@danskebyggecentre.dk"

            Dim Body As String = Body1 + Body2
            Mail.SendMailPris(Subject, Body, Email, Framail, Framail)
            Response.Redirect("Tak_klima.aspx")

        End If

    End Sub

    Sub GetStamData()

        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim DB As New DB
        Dim liste As New List(Of NomineringPris)

        liste = DB.GetFirmanavn(KontraktNummer)
        Dim Firma As String = ""
        Dim Standnr As String = ""

        For Each item As NomineringPris In liste
            Firma = item.KontaktPerson
            Standnr = item.Email
        Next

        TextBoxKontrakt.Text = KontraktNummer
        TextBoxFirma.Text = Firma
        TextBoxStandnr.Text = Standnr

    End Sub

    Private Function RandomNumber(ByVal min As Integer, ByVal max As Integer) As Integer
        Dim random As New Random()
        Return random.Next(min, max)
    End Function


    Protected Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButtonProdukt.CheckedChanged
        FileUpload1.Visible = True
        FileUpload1.Enabled = True
        LabelDokumentation.Visible = True
        Labelprodukt.Text = " <h4>Ved produkt eller løsning til byggeriet</h4><ol class=""overskrift""> <li>Giv en kort beskrivelse af egenskaberne ved din virksomheds produkt eller løsning</li><li>Giv en kort beskrivelse af, hvilke af de stillede krav dit produkt eller løsning opfylder</li><li><strong>Husk!</strong> Upload dokumentation for de krav, dit produkt eller løsning opfylder</li> </ol>     "
    End Sub

    Protected Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButtonStrategi.CheckedChanged
        Labelprodukt.Text = " <h4>Ved klima-, energi- eller miljøstrategi og/eller -indsats</h4><ol class=""overskrift""><li>Giv en kort beskrivelse af din virksomhed</li><li>Præsenter din irksomheds strategi og/eller indsats</li> <li> Giv en kort beskrivelse af opnåede resultater</li></ol>"
        FileUpload1.Visible = False
        FileUpload1.Enabled = False
        LabelDokumentation.Visible = False

    End Sub

    Protected Sub ButtonUpload_Click(sender As Object, e As EventArgs) Handles ButtonUpload.Click


        Dim KontraktNummer As String = Web.HttpContext.Current.User.Identity.Name
        Dim NumInt As Integer = RandomNumber(100, 999999)
        Dim NumString As String = CStr(NumInt)

        ' Specify the path on the server to
        ' save the uploaded file to.
        Dim savePath As String = "D:\Data\ByggeriMesse\Priser\"

        ' Before attempting to perform operations
        ' on the file, verify that the FileUpload 
        ' control contains a file.
        Dim GodkendtFil As Boolean = False

        If (FileUpload1.HasFile) Then
            ' Get the name of the file to upload.
            Dim fileName As String = FileUpload1.FileName
            Dim extension As String = IO.Path.GetExtension(fileName)
            extension = LCase(extension)

            If extension = ".pdf" Then GodkendtFil = True
            If extension = ".jpg" Then GodkendtFil = True
            If extension = ".tiff" Then GodkendtFil = True
            If extension = ".tif" Then GodkendtFil = True
            If extension = ".png" Then GodkendtFil = True
            If extension = ".tif" Then GodkendtFil = True
            If extension = ".doc" Then GodkendtFil = True
            If extension = ".docx" Then GodkendtFil = True

            If GodkendtFil = True Then
                Dim FilNavnMail As String = fileName
                fileName = KontraktNummer + "." + NumString + "." + fileName
                Dim FilnavnDB As String = LabelFilnavn.Text
                LabelFilnavn.Text = FilnavnDB + "," + fileName

                ' Append the name of the file to upload to the path.
                savePath += fileName
                ' Call the SaveAs method to save the 
                ' uploaded file to the specified path.
                ' This example does not perform all
                ' the necessary error checking.               
                ' If a file with the same name
                ' already exists in the specified path,  
                ' the uploaded file overwrites it.
                FileUpload1.SaveAs(savePath)
                LabelDokumentation.Text = ""
                LabelHusk.Text = "Du har uploaded dokumentation"
                LabelForkertformat.Visible = True
                LabelForkertformat.Text = "fil upload success"
                '     Notify the user of the name the file
                '     was saved under.
                '    MailUpload(FilNavnMail)
                Dim DB As New DB
                DB.InsertFiler(fileName)
            Else
                '    If filesize > 20000000 Then
                '        LabelForkertFormat.Visible = True
                LabelForkertformat.Visible = True
                LabelForkertformat.Text = "Filer må kun have formattet pdf, jpg, tiff, tif, png, doc,docx , filen ikke uploaded"
                '    End If
                '       Else
                '    LabelForkertFormat.Visible = True
                '    LabelForkertFormat.Text = "Der er ikke valgt nogen fil, klik på browse for at finde filen på din computer"
                '     Notify the user that a file was not uploaded.
            End If
        End If
      
    End Sub

End Class