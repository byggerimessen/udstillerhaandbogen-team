﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class BestillingAdgangskortPR

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''TextBoxAdgangsKort control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxAdgangsKort As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RequiredFieldValidator27 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator27 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RangeValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RangeValidator1 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''TextBoxPostLabels control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxPostLabels As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RequiredFieldValidator28 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator28 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RangeValidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RangeValidator2 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''TextBoxPlakater control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxPlakater As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RequiredFieldValidator29 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator29 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RangeValidator6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RangeValidator6 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''TextBoxUdstillerKort control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxUdstillerKort As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RequiredFieldValidator31 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator31 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RangeValidator4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RangeValidator4 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''ButtonSendAdgangsKort control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonSendAdgangsKort As Global.System.Web.UI.WebControls.Button
End Class
