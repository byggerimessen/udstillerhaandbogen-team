﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VaregrupperTak.aspx.vb" Inherits="Udstillerhaandbog.VaregrupperTak" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tak!</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
    
   <h2> Tak for de indtastede oplysninger</h2>
            <br />
            <br />
    
            Deadline er 18<b>. november</b>, hvis du ønsker at komme tilbage og tilføje/fjerne varegrupper.
    
            <br />
            <br />
            Du modtager en e-mail med dine kategorier sendt til:
            <asp:Label ID="LabelEmail" runat="server"></asp:Label>
&nbsp;. 
    <br />
    Hvis der ikke står en e-mail adresse, skal du udfylde det under indtastning af <a href="Firmadata.aspx">Firmadata</a>.<br />
            <br />
    </div>
         <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
