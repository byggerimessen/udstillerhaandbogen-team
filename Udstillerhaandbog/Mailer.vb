﻿Imports System.Net.Mail
Public Class Mailer
    Sub SendMailPris(ByVal Subject As String, ByVal Body As String, ByVal Til As String, ByVal Fra As String, ByVal BCC As String)

        Try
            Dim EMail As New MailMessage()
            EMail.SubjectEncoding = System.Text.Encoding.Default
            EMail.BodyEncoding = System.Text.Encoding.Default
            EMail.IsBodyHtml = True
            EMail.To.Add(Til)
            EMail.Bcc.Add(BCC)

            EMail.Subject = Subject
            EMail.From = New MailAddress(Fra)

            EMail.Body = Body + "<br><br>" + "Med venlig hilsen" + "<br>" + "Danske Byggecentre"
            EMail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            EMail.BodyEncoding = System.Text.Encoding.Default

            Dim smtp As New SmtpClient("172.16.60.1")
            smtp.Send(EMail)
            EMail.Dispose()

        Finally
        End Try

        'send the email 

    End Sub
End Class
