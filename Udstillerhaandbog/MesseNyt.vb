﻿Public Class MesseNyt
    Dim _Navn As String
    Dim _Email As String
    Public Sub New(ByVal Navn As String, ByVal Email As String)
        _Navn = Navn
        _Email = Email
    End Sub
    Public ReadOnly Property Navn As String
        Get
            Return _Navn
        End Get
    End Property
    Public ReadOnly Property Email As String
        Get
            Return _Email
        End Get
    End Property
End Class
