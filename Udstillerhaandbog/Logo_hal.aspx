﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Logo_hal.aspx.vb" Inherits="Udstillerhaandbog.Logo_hal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BYGGERI' 14 logo & halplaner</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
            </div> 
<div class="page">
    <h2>BYGGERI' 14 logo samt halplaner</h2>
    <br />

    Her kan der downloades BYGGERI' 14 logo samt halplaner:
    <br />
    <a href="http://byggerimessen.dk/wp-content/uploads/2013/09/byggeri14-med-dato.png">BYGGERI' 14 logo med dato </a> <br />
    <a href="http://byggerimessen.dk/wp-content/uploads/2013/09/byggeri14-uden-dato.png">BYGGERI' 14 logo uden dato</a> <br />
    <a href="http://byggerimessen.dk/wp-content/uploads/2013/03/BYGGERI-14-HAL-AC-03.10.13.pdf">Hal AC</a><br />
    <a href="http://byggerimessen.dk/wp-content/uploads/2013/03/BYGGERI-14-HAL-DE-08.10.13.pdf">Hal DE</a><br />
    <br />
    <a href="Default.aspx">Tilbage til forsiden</a>
    </div>
   <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
