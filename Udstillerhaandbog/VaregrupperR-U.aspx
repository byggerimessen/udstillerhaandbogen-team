﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VaregrupperR-U.aspx.vb" Inherits="Udstillerhaandbog.VaregrupperR_U" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Varegruppefortegnelse R-U</title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
        </div>
<div class="page">
    <h2>Varegruppefortegnelse (varegrupper R - U)</h2>
    <br />
    <table class="style1">
        <tr>
            <td>
   <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/VaregrupperM-P.aspx">Forrige side</asp:LinkButton>
    &nbsp;Husk at godkende på side 7</td>
            <td align=right>
                <asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/VaregrupperV-AA.aspx">Næste 
            side</asp:LinkButton>
            </td>
        </tr>
    </table>
   <br />
    <hr />
    <div>
     <table class="style1">
     <tr>
    <td width=300><h4>&nbsp; R</h4></td>
    <td width=300><h4>&nbsp; S</h4></td>
    <td width=300><h4>&nbsp; T</h4></td>
    <td width=300><h4>&nbsp; U</h4></td>
     </tr>
        <tr valign=top>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListR" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListS" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListT" runat="server">
                </asp:CheckBoxList>
            </td>
            <td width=300>
                <asp:CheckBoxList ID="CheckBoxListU" runat="server">
                </asp:CheckBoxList>
            </td>
        </tr>
    </table><hr />
        </div>
       <table class="style1">
           <tr><td>
   <asp:LinkButton ID="LinkButton4" runat="server" PostBackUrl="~/VaregrupperM-P.aspx">Forrige side</asp:LinkButton>
               </td><td align="right">Side 5, <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/VaregrupperV-AA.aspx">Næste 
            side</asp:LinkButton>
            </td>
        </tr>
    </table>
    </div>
         <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
