﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Komite.aspx.vb" Inherits="Udstillerhaandbog.Komite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Komitéen</title>
      <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
</head>
<body>
      <form id="form1" runat="server">
     <div>
         <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red"></div> </div>
<div class="page">
<div class="sitemap"></div>
   <h2>Nomineringskomité</h2>
    <br />
    Nomineringskomitéen nominerer produkter og løsninger til de tre klimapriser samt udpeger modtagerne af priserne. Nomineringskomitéen består af repræsentanter fra samtlige byggeriets parter. 
    <br />
    <br />
    <strong>Komitéen består af:</strong><br />
    Klima- og Energiministeriet, Energistyrelsen direktør Ib Larsen <br />
    Dansk Byggeri
    direktør Lars Storr-Hansen 
    <br />
    Håndværksrådet
    adm. direktør Ane Buch 
    <br />
    Bygherreforeningen
    direktør Henrik L. Bang 
    <br />
    Akademisk Arkitektforening
    direktør Jane Sandberg<br />
    Danske Arkitektvirksomheder
    direktør Christian Lerche 
    <br />
    Konstruktørforeningen
    sekretariatsleder Kim Benzon Knudsen 
    <br />
    F.R.I
    direktør Henrik Garver 
    <br />
    Dansk Indeklima Mærkning
    Thomas Witterseh 
    <br />
    PEFC
    formand Niels Peter Dalsgaard Jensen 
    <br />
    SBI - Statens Byggeforskningsinstitut
    direktør Thorkild Ærø 
    <br />
    DI Byg
    branchedirektør Elly Kjems Hove<br />
    Danske Byggecentre, adm. direktør Palle Thomsen<br />
        <br />
    <br />
 <a href="nominering.aspx">Tilbage til nomineringsforsiden</a>
     </div>
           <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
     
         <p >ARRANGØR:<br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>         
    </form>
</body>
</html>
