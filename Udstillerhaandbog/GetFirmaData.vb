﻿Public Class GetFirmaData
    Dim _Firma As String = ""
    Dim _Adresse As String = ""
    Dim _PostNummer As String = ""
    Dim _City As String = ""
    Dim _Navn As String = ""
    Dim _Telefon As String = ""
    Dim _Email As String = ""
    Dim _StandNummer As String = ""
    Dim _KontraktNummer As String = ""
    Dim _FirmaWebside As String = ""
    Dim _Fax As String = ""
    Dim _FirmaEmail As String = ""

    Public Sub New(ByVal Firma As String, ByVal Adresse As String, ByVal PostNummer As String, ByVal City As String, ByVal Navn As String, _
            ByVal Telefon As String, ByVal Email As String, ByVal StandNummer As String, ByVal KontraktNummer As String, ByVal FirmaWebside As String, _
            ByVal FirmaEmail As String, ByVal Fax As String)
        _Firma = Firma
        _Adresse = Adresse
        _PostNummer = PostNummer
        _City = City
        _Navn = Navn
        _Telefon = Telefon
        _Email = Email
        _StandNummer = StandNummer
        _KontraktNummer = KontraktNummer
        _FirmaWebside = FirmaWebside
        _Fax = Fax
        _FirmaEmail = FirmaEmail
    End Sub
    Public ReadOnly Property Firma() As String
        Get
            Return _Firma
        End Get
    End Property
    Public ReadOnly Property Adresse() As String
        Get
            Return _Adresse
        End Get
    End Property
    Public ReadOnly Property PostNummer() As String
        Get
            Return _PostNummer
        End Get
    End Property
    Public ReadOnly Property City() As String
        Get
            Return _City
        End Get
    End Property
    Public ReadOnly Property Navn() As String
        Get
            Return _Navn
        End Get
    End Property
    Public ReadOnly Property Telefon() As String
        Get
            Return _Telefon
        End Get
    End Property
    Public ReadOnly Property Email() As String
        Get
            Return _Email
        End Get
    End Property
    Public ReadOnly Property StandNummer() As String
        Get
            Return _StandNummer
        End Get
    End Property
    Public ReadOnly Property KontraktNummer() As String
        Get
            Return _KontraktNummer
        End Get
    End Property
    Public ReadOnly Property FirmaWebside() As String
        Get
            Return _FirmaWebside
        End Get
    End Property
    Public ReadOnly Property Fax() As String
        Get
            Return _Fax
        End Get
    End Property
    Public ReadOnly Property FirmaEmail() As String
        Get
            Return _FirmaEmail
        End Get
    End Property
End Class
