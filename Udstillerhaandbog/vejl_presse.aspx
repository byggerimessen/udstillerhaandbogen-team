﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="vejl_presse.aspx.vb" Inherits="Udstillerhaandbog.vejl_presse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
    </head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
            </div> 
<div class="page">
    <h2>Få dit firmas messenyheder i relevante fag- og dagblade !</h2>
    <br />

Tiden nærmer sig for Skandinaviens førende fagmesse for byggevarer – BYGGERI ´14.

    <br />
    <br />
    Erhvervsrettede aviser, fagblade og magasiner vil i den kommende tid, i lighed med tidligere år, bringe foromtaler, artikler, specielle temasider, særtillæg m.v. Disse foromtaler bidrager stærkt til at få så mange kvalificerede besøgende til messen som muligt.

    <br />
    <br />
    Messens pressechef, Mikkel Luplau Schmidt, HOLM Kommunikation, og vi vil gøre alt, hvad vi kan, for at få den maksimale foromtale af BYGGERI ´14, blandt andet ved et fyldigt pressemateriale til medierne, der løbende følges op med nyhedsomtaler og artikler helt op til messen.

    <br />
    <br />
    Som noget NYT giver vi på BYGGERI ´14 alle udstillere mulighed for at uploade deres produktomtaler direkte på messens hjemmeside – og så tit de vil. 
    <br />
    <br />
    <h2>Vejledning for udarbejdelse af pressemateriale</h2>
				
    <br />
    <strong>Udvælg</strong>
    <br />
    Udvælg det eller de produkt(er), som er mest interessante blandt de nyheder, du vil vise på firmaets stand.

    <br />
    <br />
    <strong>Beskriv kort</strong>
    <br />
    Beskriv produktet kort – <strong>Hvad</strong> er det for et produkt, <strong>hvori</strong> består nyheden,<strong> hvorfor</strong> og <strong>hvordan.</strong>
    <br />
    <br />

Har du en<strong> god case</strong>, er princippet det samme:<strong> Hvad er historien, hvad er nyheden.</strong>
    <br />
    <br />

En hjælp til at komme i gang kan være at besvare spørgsmålene:

    <br />
    <ul><li>Hvad er nyt ved produktet ?</li>
    <li>Hvilke problemer løser det ?</li>
    <li>Og hvis det er en videreudvikling, hvordan adskiller det sig fra tidligere eller andre produkter ?</li>
</ul>
    <br />
    <strong>Hovedregel: Hold omtalen inden for ét enkelt A4-ark</strong>
    <br />
En god produktomtale og/eller pressemeddelelse slutter, når der ikke er mere konkret og kontant at fortælle. Undgå opsummeringer og afrundinger.

    <br />
    <br />
    Ved flere produkter, skal du udarbejde en separat produktomtale for hvert produkt.

    <br />
    <br />
    <strong>Tekst: Almindelig word, arial, punkt 11, sort skrift</strong>
    <br />
Undlad at fremhæve med fed, kursiv eller lignende, ligesom du bør spare på alt for farverige beskrivelser.<br />
    <br />
    <strong>Fotos er vigtige!</strong>
    <br />
Formater:
    <br />
    Af hensyn til trykprocessen skal billedfiler være i jpg-, .tiff-, pdf- eller .eps-format.
    <br />
    Fotos må <strong>ikke</strong> indsættes i Word-dokumentet.
    <br />
    Fotos uploades med produktomtalen. Husk billedtekst. 

    <br />
    <br />
    <strong>Billedkvalitet</strong>
    <br />
Skal pressen kunne benytte de medsendte billeder, <strong>skal</strong> billederne have en høj opløsning på min.  300 dpi. 

    <br />
    <br />
    <strong>Produktomtalen udarbejdes online og uploades direkte på messens hjemmeside.
  
    <br /><br />
    </strong><a href="Udarbejd_presse.aspx">Gå til udarbjdelse/upload</a><br />
    <br /><br />
    <a href="Default.aspx">Tilbage til forsiden</a></div></div>
&nbsp;<div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
        <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
