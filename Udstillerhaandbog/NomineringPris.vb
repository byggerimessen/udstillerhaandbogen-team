﻿Public Class NomineringPris
    Dim _KontaktPerson As String

    Dim _Email As String
    Dim _PrisType As String
    Dim _BeskrivelsePris As String
    Dim _IndberetType As String
    Dim _FilNavn As String
    Dim _Standnr As String
    Public Sub New(ByVal KontaktPerson As String, ByVal Email As String, ByVal PrisType As String, ByVal BeskrivelsePris As String, ByVal IndberetType As String, _
                   ByVal FilNavn As String, ByVal Standnr As String)
        _KontaktPerson = KontaktPerson
        _Email = Email
        _PrisType = PrisType
        _BeskrivelsePris = BeskrivelsePris
        _IndberetType = IndberetType
        _FilNavn = FilNavn
        _Standnr = Standnr
    End Sub
    Public ReadOnly Property KontaktPerson As String
        Get
            Return _KontaktPerson
        End Get
    End Property
    Public ReadOnly Property Email As String
        Get
            Return _Email
        End Get
    End Property
    Public ReadOnly Property PrisType As String
        Get
            Return _PrisType
        End Get
    End Property
    Public ReadOnly Property BeskrivelsePris As String
        Get
            Return _BeskrivelsePris
        End Get
    End Property
    Public ReadOnly Property IndberetType As String
        Get
            Return _IndberetType
        End Get
    End Property
    Public ReadOnly Property Filnavn As String
        Get
            Return _FilNavn
        End Get
    End Property
    Public ReadOnly Property Standnr As String
        Get
            Return _Standnr
        End Get
    End Property
End Class
