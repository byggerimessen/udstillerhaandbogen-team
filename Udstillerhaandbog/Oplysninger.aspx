﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Oplysninger.aspx.vb" Inherits="Udstillerhaandbog.Oplysninger" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Oplysninger om BYGGERI' 14 og MESSE C</title>
       <link rel="stylesheet" href="Site.css" type="text/css" media="screen" />
    <style type="text/css">
        .auto-style1 {
            text-decoration: underline;
        }
        .auto-style2 {
            color: #009933;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
     <div class="header">
    <div class="header-container">
        <div class="header-text">
        <h1 class="auto-style2">Udstillerhåndbogen</h1>
        <br/> 
        </div>
    <img src="billeder/logo.png" width="240px" height="191px" alt="byggerimessen'14"/>
</div>
</div>
<div class="red">
    <br />
    <br />
    <br />
        </div> 
<div class="page">
    <br /><h2>Information og praktiske oplysninger om BYGGERI´14 og MESSE C, Fredericia</h2>
    <div class="info"> 
        <h4>ADGANGSKORT</h4>
        Adgangskort til besøgende på messen kan rekvireres uden beregning og i ubegrænset antal af udstillere til levering efter den 16. december 2013. Bestil under <a href="BestillingAdgangskortPR.aspx">afsnit 4</a>.
Se også under ”Besøgende/Elektronisk registrering”.
        <br />
        <br />
        <strong>Vigtigt!</strong>
        <br />
Benyt disse gratis adgangskort i alle udsendelser fra firmaet, lad sælgerne få en stak med på turen rundt til kunder og byggepladser, så du på alle måder gør opmærksom på dit firmas deltagelse på BYGGERI ´14!
        <br />
        <h4>AFLÆSNING</h4>
Der er porte i alle haller, som du kan køre direkte til. Der er ingen trin eller forhindringer, så du kan rulle alt materiel direkte ind på standen. Bemærk hvor portene er ind til den hal, din stand er placeret, så du kan køre direkte til porten tættest på din stand. Se oversigtsplan under <a href="http://">afsnit 6</a>, der også angiver portenes højde og bredde. 
Se også ”Kørsel i messecentret”. 

<h4>AFSKÆRMNING AF MASKINER</h4>
Udstillere er forpligtede til at overholde samtlige lovgivningsmæssige krav, herunder at forsyne de udstillede genstande (maskiner, anlæg og apparater af enhver art) med de i henhold til lovgivningen foreskrevne sikringsdele, uanset om det udstillede vises i drift eller ej. 

<!--”NYT”!--> <h4>ANNONCERING / MARKEDSFØRING – TILBUD TIL UDSTILLERE</h4> 
Der vil være en lang række nye tilbud til udstillere om annoncering/markedsføring i forbindelse med BYGGERI ´14, som øger firmaets eksponering før og under messen.

F.eks. er der mulighed for bannerannoncering på messens hjemmeside og app, logo og annonce i messekataloget, links til egen hjemmeside, eksponering af events under messen og meget mere. Læs mere i <a href="http://">afsnit 5</a>.

<!--”NYT”!--> <h4>APP</h4>
BYGGERI ´14 vil have sin egen app, som kan downloades ved registrering og udskrivning af adgangskort, via QR koder i annoncer, via messens hjemmeside m.v. App´en vil indeholde udstillerliste, interaktive halplaner, varegruppeoversigt og meget mere. 

Det vil desuden være muligt for udstillere at få deres events under messen omtalt i app´en. Se <a href="http://">afsnit 5</a>. 

<!--”NYT”!--> <h4>BANNERANNONCERING</h4>
Udstillere på messen har mulighed for at annoncere på BYGGERI ´14 app´en via bannere. Se <a href="http://">afsnit 5</a>.

<h4>ARBEJDSKORT</h4>
Der benyttes ikke arbejdskort i MESSE C under opbygning og nedtagning.
Der benyttes Udstillerkort i selve messeperioden, som giver adgang til messen for standpersonale – se bestillingsskema i <a href="BestillingAdgangskortPR.aspx">afsnit 4</a>. 

<h4>ARRANGØRKONTOR</h4>
Arrangørkontoret er beliggende i indgangspartiet ved hovedindgangen. Se oversigtsplan under <a href="http://">afsnit 6</a>. Her vil repræsentanter fra Danske Byggecentre stå til rådighed i hele messeperioden. 

<h4>AV-UDSTYR</h4>
Du kan leje professionelt AV-udstyr i MESSE C. Se skema 11 i <a href="http://">afsnit 6</a>. Kontakt Teknisk Afdeling på tlf. 75 92 25 66 og få et tilbud.  

<h4>BESTILLINGSSKEMAER</h4>

<strong>Adgangskort, PR-materiale og optagelse i messekatalog</strong>
        <br />
Bestillinger til Danske Byggecentre, herunder adgangskort og andet PR-materiale, samt optagelse i messekataloget, udstillerkort m.v. – se <a href="Default.aspx">afsnit 4</a> – bedes venligst sendt til Danske Byggecentre: 

        <br />

        <br />
        <strong> Snarest og SENEST 18. november 2013 
        <br />
        <br />
        Ydelser i MESSE C 
        <br />
        </strong>

Samtlige bestillingsskemaer på serviceydelser i messecentret samt standskitse – <a href="http://">afsnit 6</a> – bedes venligst sendt til MESSE C: 

<strong>
        <br />
        Snarest og SENEST 20. januar 2014 
        <br />
        <br />
        NB! Bestil til tiden og spar penge
Foretages bestillingerne INDEN ovennævnte dato fratrækkes der 10% fra fakturaen.</strong> 

<strong>
        <br />
        <br />
        For ydelser, der først bestilles efter den officielle indrykningsdag, beregner messecentret sig et tillæg på 10% på alle tekniske ydelser.</strong>
        <br />
Se i øvrigt MESSE C´s Generelle Regler i <a href="http://">afsnit 6</a>.  

For yderligere information om opbygning, teknik, messecentret, regler og standskitse se <a href="http://">afsnit 6</a>.

<h4>BESØGENDE - ELEKTRONISK REGISTRERING</h4>
På messens hjemmeside, www.byggerimessen.dk, vil det være muligt for besøgende at registrere sig elektronisk og udskrive deres personlige adgangskort til messen. Besøgende, som er registreret elektronisk, vil have nem og hurtig adgang til messen via egne indgange. 

<!--”NYT”!--> <h4>BESØGSREGISTRERING PÅ STANDEN</h4>
BYGGERI ´14 tilbyder udstillere adgang via scanning til de besøgendes data. Det gælder de besøgende på den enkelte stand, der har registreret sig elektronisk og dermed har en stregkode på navneskiltet. Læs mere i <a href="http://">afsnit 5</a>.

<h4>BLOMSTER</h4>
Du kan købe eller leje grønne planter og blomster til udsmykning af din stand i messecentret. Se bestillingsskema og prisliste i <a href="http://">afsnit 6</a>. 

<h4>BRANDVÆSENET</h4>
Brandvæsenet kræver, at flaskegasanlæg på standen skal godkendes, og at der ikke må anvendes papir, lærred, hessian eller andre letantændelige eller brandbare stoffer, med mindre disse er imprægnerede mod brand samt godkendt af Brandvæsenet. Brandfarlige væsker og celluloidvarer må kun forefindes efter Brandvæsenets særlige godkendelse. 
Godkendelse og yderligere oplysninger kan fås ved henvendelse til Teknisk Afdeling i messecentret eller på tlf. 75 92 25 66. 

<h4>CAFETERIER/RESTAURANTER</h4>
        <span class="auto-style1"><strong>For udstillere:</strong></span>
        <br />


        <br /><strong>
        Udstillercaféer:</strong>
        <br />

Der findes to udstillercaféer på BYGGERI ´14. 
        <br />
        Udstillercaféerne er <span class="auto-style1"><strong>udelukkende</strong></span> forbeholdt udstillere og deres standpersonale. Adgang til Udstillercaféerne er <span class="auto-style1"><strong>kun mulig mod forevisning af udfyldt udstillerkort</strong></span>. 

        <br />
        <br />
        <strong>Udstillercaféerne er beliggende med indgang i øverste venstre del af Hal A (som tidligere) samt i hal C.</strong>
        <br />
        <br />

I Udstillercaféerne serveres en buffet med fisk, pålæg, tre lune retter, der veksler hver dag, samt ost og kage til en pris af <strong>kr. 108,00 (ex. moms) pr. person</strong>, inkl. 1 øl eller vand og kaffe/te, <strong>såfremt</strong> spisebilletten hertil er købt på forhånd. Købes spisebilletten under messen koster den <strong>kr. 116,00 (ex. moms) pr. person</strong>. 
        <br />
        Se bestillingsskema i <a href="http://">afsnit 6</a>. 

        <br />
        <br />
        <strong>Spisebilletterne udfærdiges pr. dag og gælder kun til den pågældende dag</strong>.
Spisebilletterne faktureres efter messen, og vi gør venligst opmærksom på, at <strong>ubenyttede spisebilletter ikke refunderes</strong>. 

        <br />
        <br />
        <strong>Udstillercaféerne har åbent:

</strong>
        <br />
        25. – 28. februar kl. 11.30 – 16.00 

        <br />
        I opbygnings- og nedtagningsperioden vil der være åbent i Cafeteriet Panorama på 1. sal i indgangspartiet i hovedindgangen. Se oversigtsplan under <a href="http://">afsnit 6</a>.
        <br />
        <br />
        <strong>Cafeteriet Panorama har åbent:</strong>
        <br />
17. – 23. februar: kl. 8.30 - 15.00 
        <br />
        24. februar: kl. 8.30 - 19.00 (aftensmad) 
        <br />
        25. – 28. februar: kl. 8.30 - 16.00 (messeperioden) 
        <br />
        28. februar desuden: kl. 17.00 - 19.00 (aftensmad) 
        <br />
        1. – 4. marts: kl. 8.30 - 15.00 

        <br />
        <br />
        Uden for ovennævnte åbningstider kan udstillere altid henvende sig i Teknisk Afdeling for oplysning om dagens spisemuligheder. 
        <br />
        I Messeperioden kan udstillere frit benytte hallens bistroer, cafeterier, restaurant m.v. – se oversigtsplan under <a href="http://">afsnit 6</a>.
        <br />
        <br />
        Yderligere oplysninger om MESSE C's mange bespisningsmuligheder finder du under <a href="http://">afsnit 6</a>. Har du spørgsmål til messecentrets cafeterier, restaurant, spisebilletter m.v., kan disse rettes til restaurantchef Bettina Andersen på tlf. 75 92 25 66. 

        <br />
        <br />
        <span class="auto-style1"><strong>For besøgende:</strong></span>
        <br />

Åbningstider i messeperioden 25. – 28. februar: kl. 09.30 - 17.00
        <br />
        (Se oversigtsplanen over spisesteder under <a href="http://">afsnit 6</a>)

        <br />
        <br />
        <strong>Restauranten</strong>
        <br />
I stueplan i konferencecentret: kl. 11.30 - 15.00 

        <br />
        <br />
        <strong>Cafeteriet Panorama</strong>
        <br />
På 1. sPå 1. sal i indgangspartiet ved hovedindgangen: kl. 8.30 - 16.00 

        <br />
        <br />
        <strong>Bistro 1, Lille C, Snackbaren og Carl´s Snack</strong>
        <br />
Kl. 9.30 - 17.00<br />
        <br />
        <strong>Carl´s Corner</strong>
        <br />
Kl. 8.30 - 17.00 

        <br />
        <br />
        <strong>Quick Lunch</strong>
        <br />
I indgangspartiet ved hovedindgangen: kl. 11.30 - 15.00 

        <br />
        <br />
        <strong>”Håndværker Spisevogn”</strong>
        <br />
Udendørs ved Busindgangen mellem Hal A og D: kl. 11.30 - 15.00 

        <br />
        <br />
        
        <h4>DEMONSTRATIONER</h4>
Jf. §7 i standkontrakten er udstillere pligtige til, efter evt. demonstrationer der efterlader affald, grundigt og efter hver demonstration at fjerne dette, så medudstillere og deres stande og besøgende ikke genereres heraf. Udstillere skal også være opmærksomme på at begrænse støj og støv både under og efter eventuelle demonstrationer af maskiner, udstyr m.v. Udvikles der støv, spåner eller lignende ved demonstrationen, skal maskinen forsynes med udsugning.

<h4>EL</h4>
Er overalt 400/230 volt. Se bestillingsskema i <a href="http://">afsnit 6</a>. 

<h4>EMBALLAGE/TOMGODS</h4>
Emballage og tomgods kan mod betaling opbevares i hallen, der påtager sig bortkørsel og tilbagelevering af emballage. Kontakt Dueholm Messespedition på tlf. 22 60 81 02 / 70 20 42 55 – se bestillingsskema i <a href="http://">afsnit 6</a>. Priserne for opbevaring af emballage er inkl. afhentning og udbringning. Benyttes truck, betales der separat for denne.<br />
        <br />
        <strong>OBS! 
Emballage skal mærkes med udfyldt emballagemærke.</strong>
        <br />
Mærkerne kan afhentes hos Messespeditøren ved Teknisk Afdeling og i Informationen i indgangspartiet ved hovedindgangen. 

<!--NY -->
        <br />
        <h4>EVENTS INFO!</h4>
Har du events på standen, som du gerne vil gøre de besøgende opmærksom på, kan 
du kan du gratis oploade tekst herom i <a href="http://">afsnit 5</a>. Dine events vil så blive præsenteret på messens app samt hjemmeside.

<h4>FADØLSANLÆG</h4>
Bestilling af fadølsanlæg bedes foretaget forud for messen. 
Se bestillingsskema i <a href="http://">afsnit 6</a>. 

<h4>FAX/TELEFON</h4>
Du kan under messen modtage besked via MESSE C's tlf.: 75 92 25 66 eller fax 75 93 21 49. 
Besked til udstillere videregives hver hele time af messecentrets servicepersonale. 

<h4>FIRMADATA</h4>
Udstillere bedes checke deres firmadata i <a href="Firmadata.aspx">afsnit 1</a>, således at det sikres, at alle oplysninger om firmaet er korrekte.

<!--NYT --> 
        <br />
        <br />
        Vi beder desuden om, at udstillere angiver <strong>den standansvarlige, den presseansvarlige og den markedsføringsansvarlige</strong> i firmaet, såfremt det ikke er den samme person som den kontaktperson, der er oplyst til Danske Byggecentre. Dette sker for at kunne målrette forskellige budskaber omkring messen direkte til de ansvarlige personer.<br />
&nbsp;<h4>FORSENDELSER</h4>
Forsendelser til din stand på messen bedes mærket/adresseret, som følger: 

        <br />

        <br />
        Standnr. Firmanavn 
        <br />
        BYGGERI ´14 
        <br />
        MESSE C 
        <br />
        Vestre Ringvej 101 
        <br />
        7000 Fredericia 

        <br />
        <br />
        <strong>Forsendelser i løbet af messeperioden kan afhentes i Informationen i indgangspartiet</strong> ved hovedindgangen. Ved større forsendelser anvises fragtføreren til den pågældende stand. Vi gør opmærksom på, at MESSE C <strong>ikke</strong> kvitterer for modtagelsen. 

<h4>FORSIKRING</h4>
Vi gør opmærksom på, at hverken Danske Byggecentre eller MESSE C påtager sig noget ansvar – hverken direkte eller via forsikring – for det udstillede materiel. 

        <br />
        <br />
        Vi henviser i denne forbindelse venligst til standkontraktens §9 og §12 og anbefaler dig at tage kontakt til dit firmas forsikringsselskab med hensyn til forsikring af egne eller lejede udstillingsgenstande, møbler og andet standinventar. 

        <br />
        <br />
        <strong>Transport og simpelt tyveri under messen</strong>
        <br />
Vi gør opmærksom på, at det enkelte firmas erhvervsansvars-, virksomheds- og transportforsikringer <strong>kun efter særlig aftale</strong> dækker skader under transport, af- og pålæsning samt simpelt tyveri under messen. 

        <br />
        <br />
        Vær desuden opmærksom på, at det er et krav i alle erhvervsansvarsforsikringer, at der foretages en særlig afskærmning af arbejdende (farlige) udstillingsgenstande, jf. standkontraktens §7. Se også ”Afskærmning af maskiner”. 

<h4>FOTOGRAF</h4>
Der er mulighed for at bestille professionel fotografering af din stand. Du skal blot henvende dig i Informationen i indgangspartiet ved hovedindgangen, der kan formidle kontakt til en fotograf. 

        <br />
        <br />
        <strong>NB!</strong> Det er <strong>ikke</strong> tilladt at fotografere andre stande end din egen, uden forudgående aftale med den pågældende udstiller.  

<h4>FRI HØJDE I HALLERNE</h4>
Hal A:
        <br />
        9 m. Mellem dragerne op til 12,5 m. 
        <br />
        Hal D og E:
        <br />
        6,5 m. Mellem dragerne op til 7,5 m.
        <br />
        <strong>NB!</strong> Kun 5,5 m på langs af hallen mellem søjlerne. 

<!--NY --><h4>FROKOST PÅ STANDEN</h4>
Da der er tryk på besøget på standen det meste af dagen, har messeudvalget ønsket at udbuddet af frokost levereret på standen blev udvidet. Der er således på BYGGERI ´14 mulighed for at bestille mange andre spændende ting end smørrebrød til levering på standen, f.eks. en ”Box” til en person med lidt lækkert fisk, kød og ost eller en ”Sandwich box” med et varieret udbud af sandwich til 8-10 personer. Nemt at spise på standen!<br />
Læs mere i <a href="http://">afsnit 6</a>.

<h4>FØRSTE SAL, ETABLERING AF</h4>
        <strong>1. sal kan kun etableres på fritliggende stande på min 54 kvm</strong>. 
        <br />
        Se bestillingsskema i <a href="Bestilling_forstesal.aspx">afsnit 4</a> samt regler for opbygning i <a href="http://">afsnit 6</a>. 

<h4>GENERELLE REGLER I MESSE C</h4>
MESSE C's Generelle Regler for messedeltagelse skal overholdes. Se venligst <a href="http://">afsnit 6</a>.

<h4>GODS</h4>
Gods til stande uden opbygning kan modtages i MESSE C: 

        <br />
        <strong>fra 12. februar 2014 kl. 8.00 - 16.00</strong>
        <br />
        <br />

Til stande med opbygning: 

<br />
        <strong>fra 17. februar 2014 kl. 8.00 - 17.00</strong>
        <br />
        <br />

Har du behov for levering tidligere, skal dette aftales med Teknisk Afdeling, som du kan kontakte på tlf. 75 92 25 66 for at afdække mulighederne herfor. <h4>HALGULVET</h4>
Halgulvet i alle haller er betongulv. 
        <br />
        <strong>NB!</strong> Det er ikke tilladt at bore, sømme eller på anden måde beskadige gulvet. På grund af gulvets beskaffenhed må udstillere kun benytte speciel dobbeltklæbende tape, som kan købes i Teknisk Afdeling. Eventuel overtrædelse medfører en regning på rengøring eller istandsættelse af gulvet.  

<h4>HOTELLER</h4>
Der er 5.000 sengepladser inden for max. 20 minutters kørsel fra messecentret, men husk alligevel at booke dit hotelværelse i god tid. Se hotellisten i <a href="http://">afsnit 2</a>. 

<h4>INTERN TRANSPORT</h4>
Der er både sækkevogne og palleløftere, som udstillere kan benytte til den interne transport, men ikke nok til alle på én gang. Derfor opfordrer vi udstillere til – så vidt muligt – selv at medbringe sækkevogne og palleløftere.  

<h4>HJEMMESIDE</h4>
BYGGERI ´14 har egen hjemmeside, som løbende opdateres med de seneste pressemeddelelser, produktomtaler, udstillerliste, nye tiltag, events under messen m.v. Alle udstillende firmaer vil være at finde her, og man kan indhente informationer om blandt andet transport- og overnatningsmuligheder i området, samt registrere sig og udskrive eget adgangskort.  

        <br />
        <br />
        I forbindelse med opstart af annoncering og markedsføring af messen målrettes messens hjemmeside primo december 2013 de besøgende og aktiviteten på hjemmesiden intensiveres.
        <br />
        <br />
        Adressen på BYGGERI 14´s hjemmeside er: <a href="http://byggerimessen.dk/">www.byggerimessen.dk</a>&nbsp; 
        
       <!--NY --> <h4>BANNERANNONCERING</h4>
Udstillere på BYGGERI ´14 har mulighed for at annoncere på messens hjemmeside via bannere. Se <a href="http://">afsnit 5</a>.

<h4>INTERNET</h4>
Med adgangskode - der udleveres i MESSE C´s Tekniske Afdeling - kan udstillerne frit koble sig på messecentrets trådløse netværk. For en 100% sikker forbindelse anbefales en kablet opkobling. Se bestillingsskema i <a href="http://">afsnit 6</a>. 

<h4>KLIMAPRISER</h4>
Byggeriets Klimapris, Byggeriets Energipris og Byggeriets Miljøpris vil blive uddelt under BYGGERI ´14. 

        <br />
        <br />
        Materiale om nominering samt krav hertil er udsendt til samtlige udstillere ultimo september 2013.
        <br />
        Deadline for indsendelse af ansøgning er: <strong>4. november 2013</strong>
        <br />
        <br />
        <strong>Skal dit firma modtage en af de tre klimapriser ?</strong>
        <br />
Læs om krav til nominering samt udfyld ansøgningsskemaet i <a href="nominering.aspx">afsnit 1</a>. 

<h4>KONTAKTPERSONER</h4>
Se <a href="http://byggerimessen.dk/">www.byggerimessen.dk</a> under ”Kontakt”.
        <br />
<h4>KOPI/FAX SERVICE</h4>
Er mulig i Arrangørkontoret eller Teknisk Afdeling. 
Se oversigtsplan i <a href="http://">afsnit 6</a>.&nbsp;

<h4>KRANARBEJDE</h4>
For at sikre et ordentligt arbejdsmiljø for personalet, der arbejder med standopbygning, skal biler/kraner, der arbejder med aflæsning, etablere aftræksslange til det fri – venligst vær opmærksom på afstanden fra din stand til nærmeste portåbning. 
        <br />
        Se portoversigt i <a href="http://">afsnit 6</a>.
<h4>KØBMANDSBUTIK</h4>
Købmandsbutikken findes i mellemgangen mellem Hal A og D. Her har du mulighed for at købe diverse købmandsvarer. 
        <br />
        Se oversigtsplan og bestillingsskema i <a href="http://">afsnit 6</a>.<br />
&nbsp;<br />
        I messeperioden leveres bestillingerne inden for 2 timer ud på standene. Bestillinger afleveret efter kl. 16.00 leveres på standene næste dag. 

        <br />
        <br />
        Daglige åbningstider: 
        <br />
        24. februar: kl. 8.00 - 18.00 
        <br />
        25. – 28. februar: kl. 8.30 - 11.00 
        <br />
        28. februar desuden: kl. 15.00 - 17.00 

        <br />
        <br />
        Uden for ovennævnte åbningstider afleveres bestillingsskemaerne i brevsprækken ved købmandsbutikken
 
<h4>KØRSEL I MESSECENTRET</h4>
Kørsel med bil/lastbil i messecentret kan kun ske efter aftale med hallen. Ved henvendelse til Teknisk Afdeling kan udstillere få udleveret indkørselsmærker, som skal placeres tydeligt i bilens forrude påtegnet firmaets standnr. og mobilnummer til bilens fører. 

        <br />
        <br />
        Parkering i hallerne er absolut forbudt. Kun kortvarig af- og pålæsning er tilladt.

<h4>LINK TIL EGEN HJEMMESIDE</h4>
I udstillerlisten på BYGGERI ´14´s hjemmeside er det muligt for udstillere på messen gratis at bestille link til egen hjemmeside – se <a href="http://">afsnit 5</a>.
        <br />
        <br />
        Vi opfordrer desuden alle udstillere til at benytte messens logo som bannerlogo på egen hjemmeside, således potentielle besøgende her kan linke sig videre til BYGGERI 14´s  hjemmeside. På den måde supplerer vi hinanden i markedsføringen af messen til gavn for alle. 

        <br />
        <br />
        Adressen til messens hjemmeside er: <a href="http://byggerimessen.dk/">www.byggerimessen.dk</a> 


<h4>LOGO og MESSEBANNER</h4>
På messens hjemmeside <a href="http://byggerimessen.dk/">www.byggerimessen.dk</a> finder du messens logo samt digitale banner, som du frit kan downloade og benytte i egen markedsføring, mails m.v. 
        <br />
        Du kan også finde messens logo i <a href="Logo_hal.aspx">afsnit 2</a>. 
        <br />
        Læs også ”Link til egen hjemmeside”. 
 

<h4>MANDSKABSASSISTANCE</h4>
Det er muligt at bestille hjælp til af- og pålæsning, udpakning/pakning m.v. 
        <br />
        Se priser og bestillingsskema i <a href="http://">afsnit 6</a>.

<h4>MEDUDSTILLERE AGENTURER/PRODUKTER</h4>
Der må <strong>ikke</strong> reklameres på standen for firmaer eller firmaers agenturer, produkter og løsninger, som ikke er tilmeldt som udstillere/medudstillere og godkendt som sådanne af Danske Byggecentre. Du kan tilmelde medudstillere i <a href="Tilmelding_medudstiller.aspx">afsnit 4</a>. 

        <br />
        <br />
        <strong>MESSE C</strong>
        <br />
Adresse: 
        <br />
        Vestre Ringvej 101 
        <br />
        7000 Fredericia 
        <br />
        Tlf. 75 92 25 66 
        <br />
        Fax 76 20 26 98 (Teknisk Afdeling) / 75 93 21 49 
        <br />
        Mail messec@messec.dk / teknik@messec.dk <!-- laves til link !--> 

<h4>MESSEKATALOG</h4>
Messekataloget udarbejdes, så det foreligger færdigtrykt senest en uge før messen, ligesom en digital udgave af messekataloget vil være at finde på messens hjemmeside.

        <br />
        <br />
        Optagelse i messekataloget er gratis og omfatter firmafortegnelse og alfabetisk varegruppefortegnelse. Katalogoplysningerne udfyldes i <a href="Optagelse_messe.aspx">afsnit 4</a>.
        <br />
        <br />
        <h4>MESSEKATALOG/FIRMALOGO</h4>
Der er mulighed for udstillere at indrykke firmalogo i messekataloget. Firmalogoerne placeres umiddelbart over firmanavnene i den alfabetiske firmafortegnelse. 
        <br />
        Se <a href="http://">afsnit 5</a>. 

<!--NY --> <h4>MESSENYT GØRES DIGITALT</h4>
Som udstiller vil du under messen blive informeret om aktiviteter, nyheder m.v. i ”Messenyt”. Messenyt vil I 2014 blive udsendt pr. mail, hvorfor vi beder om mailadresser på de personer, som skal modtage Messenyt under messen.  Venligst angive disse under ”Firmadata” i <a href="Firmadata.aspx">afsnit 1</a>.

<h4>NATTEVAGT</h4>
I messeperioden vil der uden for åbningstiderne være nattevagt i MESSE C.  

<h4>NEDTAGNNG</h4>
Nedtagning af standene skal være afsluttet: 

        <br />
        <br />
        <strong>Senest 4. marts 2014 kl. 21.00</strong>
        <br />
        <br />

Den dag messen lukker, fredag den 28. februar, kan der arbejdes til kl. 24.00. Herefter er åbningstiderne i nedtagningsperioden: 
        <br />
        1. – 2. marts: kl. 8.00 – 17.00 
        <br />
        3. – 4. marts: kl. 8.00 – 21.00 

        <br />
        <br />
        Materialer, der ikke er fjernet på det angivne tidspunkt, vil blive fjernet af messeledelsen for udstillerens egen regning. MESSE C tilbyder efter aftale at opbevare maskiner, gods m.v. Kontakt venligst Teknisk Afdeling.
        <br />
        <br />
        <strong>NB!</strong>
        <br />
Jf. standkontrakten må fjernelse af udstillede varer, sammenpakning og nedtagning ikke ske før <strong>efter lukketid</strong> den sidste messedag. 

<h4>NOMINERING TIL DE TRE KLIMAPRISER</h4>
Alle udstillende firmaer er indbudt til at ansøge om en af de tre klimapriser: Byggeriets Klimapris, Byggeriets Energipris og Byggeriets Miljøpris, der vil blive uddelt under BYGGERI ´14. 

        <br />
        <br />
        Materiale om nominering samt krav hertil er udsendt til samtlige udstillere ultimo september 2013.

        <br />
        <br />
        Deadline for indsendelse af ansøgning er: <strong>4. november 2013</strong>
        <br />
        <br />
        <strong>Skal dit firma modtage en af klimapriserne ?
</strong>
        <br />
        Læs om krav til nominering samt udfyld ansøgningsskemaet i <a href="nominering.aspx">afsnit 1</a>.&nbsp;

<h4>NOMINERINGSKOMITÉ</h4>
Nomineringskomitéen nominerer produkter og løsninger til de tre klimapriser samt udpeger modtagerne af priserne. Nomineringskomitéen består af repræsentanter fra samtlige byggeriets parter. Læs mere under <a href="Komite.aspx">afsnit 1</a>.

<h4>OPBYGNING</h4>
Firmaer med uopbyggede stande kan <span class="auto-style1"><strong>efter forudgående skriftlig tilmelding</strong></span> starte opbygningen: 

        <br />
        <br />
        <strong>12. februar 2014 kl. 8.00</strong>
        <br />
        <br />

Der kan 12. til 16. februar arbejdes kl. 8.00-16.00

        <br />
        <br />
        <strong>Samtlige udstillere kan påbegynde opbygningen: 

        <br />
        Mandag den 17. februar 2014 kl. 8.00</strong>
        <br />
        <br />

Der kan arbejdes alle dage kl. 8.00 - 17.00. Sidste aften inden messen åbner, kan der arbejdes i døgndrift, efter kl. 18.00 dog kun med lettere montage og opstilling på standen og ikke arbejde, der sviner og støver omkringliggende stande. Dit firma vil i givet fald blive opkrævet for rengøring af disse. 

        <br />
        <br />
        <strong>OBS!</strong>
        <br />
Sidste aften før messen åbner, skal alle gangarealer være ryddede senest kl. 18.00 af hensyn til tæppepålægning. Alle vogne skal derfor være ude af messecentret før dette tidspunkt. Der kan vederlagsfrit arbejdes med lettere montage og opstilling efter dette tidspunkt <span class="auto-style1"><strong>på</strong></span> standen. 

        <br />
        <br />
        Hvis du ønsker at arbejde på tidspunkter før kl. 8.00 eller efter kl. 17.00, kan dette aftales <strong>mod betaling</strong> med Teknisk Afdeling, der træffes dagligt i messecentret eller på tlf. 75 92 25 66. 

        <br />
        <br />
        Jf. standkontraktens bestemmelser §6 gør vi opmærksom på, at de udstillede genstande skal være opstillede, og standene være bragt fuldstændig i orden <strong>før åbningen af messen, tirsdag den 25. februar 2014 kl. 9.30</strong>. 

        <br />
        <br />
        Desuden skal de udstillede genstande forblive på plads i den tid, messen varer. Udstillere på messen er ligeledes pligtige til, jf. §5, at holde standen åben samt have denne forsvarligt bemandet i hele messeperioden.
 
        <br />
        <br />
        <strong>1. sal kan kun etableres på fritliggende på min. 54 kvm</strong>. 
        <br />
        Se bestillingsskema i <a href="Bestilling_forstesal.aspx">afsnit 4</a> samt ”Generel information” vedrørende overdækning af stande / sprinkling i <a href="http://">afsnit 6</a>. 

<h4>OPBYGNINGSREGLER</h4>
Af hensyn til andre udstillere, de besøgende samt myndighedernes krav, er der visse regler for opbygningen af standen, der skal overholdes på BYGGERI ´14. Se venligst ”Generel information”, ”Regler for standopbygning”, ”50% regel” og ”45°regel” i <a href="http://">afsnit 6</a>. 

<h4>PARKERING FOR UDSTILLERE (KUN FOR BILER UNDER 3.500 KG)</h4>
Alle udstillere modtager automatisk et <strong>P-kort</strong>. der skal benyttes ved parkering bag ved messecentret. P-kortet skal påføres standnr. og mobilnummer til bilens fører for at være gyldigt og bedes venligst placeret synligt i bilens forrude. P-kortet er eneste gyldige adgangstegn til udstillerparkeringen. 

        <br />
        <br />
        Alle udstillere bliver tildelt min. ét P-Kort, som tilsendes senest 14 dage, før messen åbner. Samtlige P-kort bliver fordelt blandt udstillere. Antallet af P-kort beregnes ud fra standens størrelse. 

        <br />
        <br />
        <strong>Udvidet Udstillerparkering</strong>
        <br />
Arealet til Udstillerparkeringen er udvidet med den tidligere busparkering, således at udstillere kan disponere over samtlige 800 parkeringspladser bag ved messecentret. Der vil således være rigeligt med plads til at efterkomme den store efterspørgsel på parkeringspladser på Udstillerparkeringen. 
        <br />
        Busparkeringen vil være at finde på en parkeringsplads til højre for messecentret. Se oversigtsplan i <a href="http://">afsnit 6</a>.
        <br />
        <br />
        <strong>Politihjemmeværnet styrer trafik og parkering om morgenen</strong>
        <br />
For at undgå kø ved indkørslen til MESSE C om morgenen vil politihjemmeværnet stå klar til at styre trafik og parkering. Det indebærer bl.a., at besøgende ledes til parkeringspladser og ad andre veje end indkørslen til hovedindgangen. Denne indkørsel vil mellem kl. 8.30 og 9.30 i videst mulige omfang være forbeholdt udstillere. 

        <br />
        <br />
        Vi gør dog samtidig opmærksom på, at udstillere har adgang til BYGGERI ´14 fra kl. 8.30 og opfordrer fortsat alle udstillere at møde tidligt, såfremt de ønsker at benytte denne indkørsel til messen, da det ikke kan undgås, at der er kø her kort før kl. 9.30. 

        <br />
        <br />
        <strong>Vil du undgå kø om morgenen, så benyt indkørslen <span class="auto-style1">bagved</span> messecentret</strong>
        <br />
For at undgå kø og ventetid til messen om morgenen anbefaler vi, at udstillere benytter ”bagindgangen” til Udstillerparkeringen <span class="auto-style1">bag ved</span> MESSE C. Se oversigtsplan i <a href="http://">afsnit 6</a>. Har du GPS, så tast adressen: Fredericia, Stenhøjvej 66. 

        <br />
        <br />
        <strong>OBS!</strong>
        <br />
Sidste messedag, fredag den 28. februar 2014, skal alle personbiler, der er parkeret bag ved messecentret flyttes til P-pladserne foran hallen senest <span class="auto-style2">kl. 17.00</span> for at give plads til vare- og lastbiler ved nedtagningen. Vi gør desuden opmærksom på, at tæpperne i messecentret skal være fjernede, før portene bliver åbnet for indkørsel. 
Læs venligst også ”Varebiler, trailere og lastvogne”.  

<h4>PLAKATER</h4>
Plakater med information om messen til ophængning i f.eks. receptionen, mødelokaler m.m. som markedsføring for messen, kan rekvireres uden beregning til levering efter 6. januar 2014. 
        <br />
        Se bestillingsskema i <a href="BestillingAdgangskortPR.aspx">afsnit 4</a>.

<h4>PORTE OG DØRE</h4>
Porte og døre i messecentret må ikke spærres. Se oversigt i <a href="http://">afsnit 6</a>.

<h4>PORT HØJDE/PORT BREDDE</h4>
Hal A 
        <br />
        Port 1: 3,9 m høj – 3,9 m bred 
        <br />
        Port 2: 5,9 m høj – 3,9 m bred 
        <br />
        Port 3: 5,9 m høj – 5,9 m bred 
        <br />
        Port 4: 5,9 m høj – 3,9 m bred 
        <br />
        Port 5: 3,9 m høj – 3,9 m bred 
        <br />
        Port 8: 5,9 m høj – 5,9 m bred 

        <br />
        <br />
        Porte i mellemgang mellem Hal A og D: 
        <br />
        Alle porte: 3,9 m høje – 3,9 m brede
        <br />
        <br />
        Hal D og E: 
<br />
        Port 9: 5,9 m høj – 5,9 m bred<br />
Port 10: 3,9 m høj – 3,9 m bred 
        <br />
        Port 11: 5,9 m høj – 5,9 m bred 
        <br />
        Port 12: 3,9 m høj – 3,9 m bred 

        <br />
        <br />
        Se desuden oversigtsplan i <a href="http://">afsnit 6</a>.
<h4>PORTVAGT</h4>
Der vil være vagt ved porten ind til messeområdet.  

<h4>POSTLABELS</h4>
Postlabels med information om messen, som kan anvendes af udstillere på al udgående post m.v. kan uden beregning rekvireres i ubegrænset antal til levering efter den 6. januar 2014. 
        <br />
        Se bestillingsskema i <a href="BestillingAdgangskortPR.aspx">afsnit 4</a>.

<h4>PRESSE</h4>
Danske Byggecentre og messens pressechef, Mikkel Luplau Schmidt, HOLM Kommunikation, vil løbende holde kontakt til alle relevante fag- og dagblade og foranledige, at repræsentanter fra pressen modtager alt relevant information og materiale vedrørende messen og udstillere. 

        <br />
        <br />
        Vi opfordrer derfor <strong>alle udstillere</strong> til at informere messens pressesekretariat om de spændende produkter og løsninger, der præsenteres på standene, ved at udarbejde pressemeddelelser og produktomtaler og uploade disse på messens hjemmeside:
<a href="http://byggerimessen.dk/">www.byggerimessen.dk</a>  

<!--”NYT” !-->
        <br />
        <br />
Læs mere om pressemateriale i <a href="vejl_presse.aspx">afsnit 3</a>, hvor du finder en vejledning og en beskrivelse af, hvordan du uploader din omtale direkte på messens hjemmeside og kravene hertil. Du kan skrive din produktomtale online. 

        <br />
        <br />
        Vil du og dit firma være med fra start er: 

        <br />
        <br />
        <strong>Deadline for indsendelse af pressemateriale er: 18. november 2013</strong>
        <br />
        <br />
        Kan du ikke overholde denne deadline, så kontakt os på tlf. 45 80 78 77 
        <br />
        <strong>Det er vigtigt, at vi hører fra dig !</strong> 

<h4>PRESSEKONTOR</h4>
Messens pressekontor er beliggende i indgangspartiet ved hovedindgangen – se oversigtsplan i <a href="http://">afsnit 6</a>. 

        <br />
        <br />
        Her vil messens pressechef, Mikkel Luplau Schmidt, fra den 24. februar 2014 og i hele messeperioden stå til rådighed for både pressen og udstillere. Det er samtidig samlingspunktet for pressens repræsentanter, som her vil blive informeret om de udstillede produkter og aktiviteter på messen. 

        <br />
        <br />
        <strong>Har du en god historie fra messen, en speciel aktivitet på standen, vi ikke allerede har hørt om, så kom ind på Pressekontoret og fortæl om det. 

        </strong> 

        <br />
        <br />
        Det vil desuden være muligt på Pressekontoret at fremlægge nye pressemeddelelser, der ikke kunne offentliggøres inden messen. 

<h4>PRESSEMATERIALE</h4>
Vi opfordrer derfor <strong>alle udstillere</strong> til at informere messens pressesekretariat om de spændende produkter og løsninger, der præsenteres på standene, ved at udarbejde pressemeddelelser og produktomtaler og uploade disse på messens hjemmeside:
<a href="http://byggerimessen.dk/">www.byggerimessen.dk</a>  

<!--”NYT” !-->
        <br />
        <br />
Læs mere om pressemateriale i forbindelse med messen i <a href="vejl_presse.aspx">afsnit 3</a>, hvor du finder en vejledning og en beskrivelse af, hvordan du uploader din omtale direkte på messens hjemmeside og kravene hertil. Du kan skrive din produktomtale on-line. 

        <br />
        <br />
        Vil du og dit firma være med fra start er: 

        <br />
        <br />
        <strong>Deadline for indsendelse af pressemateriale er: 18. november 2013</strong>

<h4><!--”NYT” !--> PRESSEPAKKER</h4>
For at give udstilleres presseansvarlige de bedste muligheder for et godt udbytte af messen i form af omtale af deres firmas produkter og services, tilbyder Danske Byggecentre i samarbejde med HOLM Kommunikation en række ”pressepakker”, der kan styrke det enkelte firmas eksponering i såvel trykte som on-line medier. Læs mere i <a href="http://">afsnit 5</a>.

<h4>PRISUDDELING</h4>
Byggeriets Klimapris, Byggeriets Energipris og Byggeriets Miljøpris vil blive uddelt under BYGGERI ´14. Nærmere information om hvor og hvornår samt invitation til at overvære prisuddelingen vil tilgå alle udstillere direkte. 

<h4>REKLAMER</h4>
Vi gør venligst opmærksom på, at udstillere, iflg. standkontraktens bestemmelser §6 <strong>under ingen omstændigheder</strong> må disponere over andet end den tildelte stand, hverken med reklamer, sandwichstandere, borde eller på anden måde. 

        <br />
        <br />
        Det er således <strong>ikke</strong> tilladt at omdele reklamer uden for standen eller at anbringe produkter, dekorationer, inventar eller lignende på gang- og/eller friarealer. 

        <br />
        <br />
        Forbuddet gælder såvel indendørs i messecentret som på alt udendørs areal samt parkeringspladsen og <strong>omfatter dermed også parkering af reklamebiler</strong> på parkeringspladsen foran hallen. 

        <br />
        <br />
        Vi gør i den forbindelse opmærksom på, at der vil være mandskab på parkeringen foran hallen, der checker biler for reklameflyers i bilruderne m.v.

        <br />
        <br />
        Brug af højttalere, forevisning af film, lysbilleder, lysshow m.v. må kun finde sted, såfremt det ikke er til gene for omkringliggende stande. Se i øvrigt Regler for standopbygning i <a href="http://">afsnit 6</a>.
        <br />
        <strong>Læs også ”Reklamebiler”</strong>. 

<h4>REKLAMEBILER</h4>
Som nævnt under ”Reklamer” er <span class="auto-style1"><strong>al</strong></span> tænkelig PR-aktivitet uden for standen <strong>ikke</strong> tilladt. Det gælder også på alt udendørs areal og på parkeringspladsen foran hallen. 

        <br />
        <br />
        Vi gør i den forbindelse opmærksom på, at der vil være mandskab på parkeringen foran hallen i morgentimerne – samt check i løbet af dagen – for at holde øje med ulovlige parkeringer på græsarealer og lignende. 

<h4>RYGNING</h4>
Med henvisning til Rygeloven, gør vi venligst opmærksom på, at de indendørs udstillings- og restaurantområder på BYGGERI ´14 vil være <strong>røgfri</strong>, og al rygning skal finde sted udendørs. 

        <br />
        <br />
        <strong>Udendørs rygeområder</strong>
        <br />
Primært rygeområde for besøgende vil være udenfor ved mellemgangen mellem Hal A og Hal D. Endvidere vil der ved udgangen fra Udstillercaféen ved Hal A og ved hovedindgangen blive etableret rygeområder. 
Alle steder vil der blive holdt opsyn med de udendørs askebægre og tilknyttet rengøring. 

        <br />
        <br />
        <strong>NB!</strong> Porte og branddøre i hallen må kun anvendes i tilfælde af brand og vil derfor blive dækket af med afspærringsbånd.  

<h4>SKILTE</h4>
Det er muligt at bestille tekst til friser, skilte m.v. 
        <br />
        Se bestillingsskema i <a href="http://">afsnit 6</a>.
        <br />
        I Teknisk Afdeling er det desuden muligt at købe diverse dekorationsmaterialer til almindelige butikspriser. 

    <h4>SPEDITION</h4>
Har du behov for assistance ved fragtspedition, kan du kontakte nedenstående speditionsfirma, som er bekendte med forholdene i Messe C og har kontor i centret: 

    <br />
    <br />
    Dueholm Messespedition 
    <br />
    Treldevej 177 
    <br />
    7000 Fredericia 
    <br />
    Tlf. 22 60 81 02 / 70 20 42 55 
    <br />
    Fax 75 93 41 44 

<h4>SPISE- OG DRIKKEVARER</h4>
Udlevering af spise- og drikkevarer fra standen må kun finde sted efter skriftlig tilladelse fra MESSE C. Se venligst de Generelle Regler i MESSE C samt information om bespisningsmuligheder i centret i <a href="http://">afsnit 6</a>. 

<h4>SPISEBILLETTER</h4>
Der kan købes spisebilletter til de to Udstillercaféer på messen. I Udstillercaféerne serveres en buffet med fisk, pålæg og tre lune retter, der veksler hver dag, samt ost og kage til en pris af kr. 108,00 (ex. moms) pr. person, inkl. 1 øl eller vand og kaffe/te, <strong>såfremt</strong> spisebilletten hertil er købt på forhånd. Købes spisebilletten under messen koster den kr. 116,00 (ex. moms) pr. person.
    <br />
    Se bestillingsskema under <a href="http://">afsnit 6</a>.  

    <br />
    <br />
    <strong>Spisebilletterne udfærdiges pr. dag og gælder kun til den pågældende dag</strong>.
    <br />
    Spisebilletterne faktureres efter messen, og vi gør venligst opmærksom på, at ubenyttede spisebilletter ikke refunderes.
    <br />
    <br />
    Adgang til Udstillercaféerne er kun mulig mod forevisning af <strong>korrekt udfyldt udstillerkort</strong>.&nbsp;
<h4>STANDOPBYGNING</h4>
    <strong>Alle stande under 54 kvm. leveres med opbygning</strong>. 
    <br />
    Opbygningen består af hvide 250 cm høje side- og bagvægge og en 30 cm høj skiltefrise i standens forkant(er) 220 cm over gulv. 
<strong>Ønsker du ikke denne opbygning, bedes dette anført på standskitsen.</strong> Se <a href="http://">afsnit 6</a>.
    <br />
    <br />
    Stande langs hallens ydervægge leveres med hvidmalet bagvæg (spånplade). 

    <br />
    <br />
    <strong>Stande lejet uden opbygning er markeret alene med standnr. samt markering af areal på gulvet.</strong>
    <br />
    <br />
    Ønsker du opbygning, kan side- og bagvægge til nabostand samt skiltefrise m.m. bestilles hos MESSE C. Se bestillingsskema i <a href="http://">afsnit 6</a>.<br />
    <br />
    <strong>NB!</strong> Vær opmærksom på, at de lejede standvægge og aluminiumsprofiler skal afleveres i samme stand som ved levering. Såfremt vægge eller profiler beskadiges, males, påklæbes tape og lignende, vil du blive faktureret for rengøring og udbedring. 

    <br />
    <br />
    <strong>NB!</strong> Opbygningen af standene skal være i overensstemmelse med reglerne for standopbygning på BYGGERI ´14 samt MESSE C´s Generelle Regler. Se venligst <a href="http://">afsnit 6</a>.
    <br />
    <br />
    <strong>Demonstrationer på standen</strong>
    <br />
    Jf. §7 er udstillere pligtige til, efter evt. demonstrationer der efterlader affald, grundigt og efter hver demonstration at fjerne dette, så medudstillere og besøgende ikke genereres heraf. Udstillere skal også være opmærksomme på, at begrænse støj og støv både under og efter eventuelle demonstrationer af maskiner, udstyr m.v. Udvikles der støv, spåner eller lignende ved demonstrationen, skal maskinen forsynes med udsugning. 
    <h4><!--”NYT”!--> STANDNUMMER-SKILTE TIL SAMTLIGE STANDE</h4>
MESSE C monterer på gulvet foran standens primære facade et A4 skilt med firmanavn og standnummer. Endvidere leveres der til samtlige stande til egen montering et aftageligt A4 skilt som kan placeres på væg, skranke el. lign. efter eget valg.
    <br />
    <strong>Bemærk</strong> at disse skilte ikke må forveksles med tekst til skiltefrisen.
<h4>STANDSKITSE</h4>
I Udstillerhåndbogen finder du under <a href="http://">afsnit 6</a>/Bestillinger til MESSE C en standskitse, hvor din stand er indtegnet. Alle monteringsønsker, herunder også specifikationer for el. m.v., bedes venligst angivet på <strong>standskitsen, som skal returneres/uploades til MESSE C sammen med bestillingsskemaerne SENEST 20. januar 2014. </strong>

<h4>STORE UDSTILLINGSGENSTANDE</h4>
Store udstillingsgenstande, der ikke kan transporteres ad de på halplanen indtegnede gange, skal anmeldes til MESSE C <strong>senest 3 uger før messens afholdelse.</strong> 

<h4>SØJLERNE I MESSECENTRET</h4>
Søjlerne i hallerne er dimensionerede, som følger: 
    <br />
    I Hal A: 70 cm x 70 cm 
    <br />
    I Hal D og E: 48 cm x 48 cm 

<h4>TEKNISK AFDELING</h4>
Teknisk Afdeling vil være dig behjælpelig med alle spørgsmål og ønsker i forbindelse med opbygning og afvikling af messen. 
    <br />
    Teknisk Afdeling er placeret i Hal A ved Bistro 1 – se oversigtsplan i <a href="http://">afsnit 6</a>. 

    <br />
    <br />
    <strong>Åbningstider:</strong>
    <br />

12. – 16. februar: kl. 8.00 – 16.00 
    <br />
    17. – 23. februar: kl. 8.00 – 17.00 
    <br />
    24. februar: kl. 8.00 – 20.00 
25. – 27. februar: kl. 8.00 – 18.00 
    <br />
    28. februar: kl. 8.00 – 20.00 1. – 4. marts: kl. 8.00 – 17.00&nbsp;
<h4>TRYKFLASKER</h4>
Der må <strong>ikke</strong> forefindes nogen form for trykflasker på standene uden forudgående aftale med Teknisk Afdeling i MESSE C. 
 
<h4>UDSKÆNKNING</h4>
Vi gør opmærksom på følgende regler for udskænkning af alkohol på BYGGERI ´14, som gælder for alle udstillere: 

    <br />
    <br />
&nbsp;&nbsp; <strong>&nbsp;- Der må ikke forefinde rene ”barstande”. Standene skal på seriøs vis præsentere  
  leverandørernes produkter. 

    <br />
    <br />
&nbsp;&nbsp;&nbsp; - Udskænkning af alkohol må kun foregå bag aflukke, på en eventuel 1. sal eller 
  andet sted, hvor de besøgende ”inviteres&nbsp;&nbsp;&nbsp;&nbsp; ind”.<br />
    <br />
&nbsp;&nbsp;&nbsp; - Umådeholden udskænkning af alkohol på standen vil blive stoppet omgående. 

    <br />
    <br />
&nbsp;&nbsp;&nbsp; - Udskænkning af øl og spiritus til unge under 18 år er naturligvis ikke tilladt. 

</strong>
    <br />
    <br />
    Vi henstiller samtidig til alle udstillere at begrænse udskænkningen på standene og i videst muligt omfang at udskænke ikke-alkoholiske drikke som f.eks. lyst fadøl. 

    <br />
    <br />
    Messeudvalget har desuden besluttet, at der på messen kun vil blive udleveret 0,25 liter bægre sammen med bestilte fadølsanlæg, ligesom det vil være den eneste type bæger, der kan købes i Købmandsbutikken. 

    <br />
    <br />
    Reglerne skal ses som et udtryk for, at arrangøren af messen ønsker at fastholde, at:

             <br />
    <strong>BYGGERI-messen i Fredericia er en seriøs fagmesse for fagfolk

</strong>hvilket alle udstillere på messen kun kan være interesserede i.
    <br />
    <br />
    Vi gør samtidig opmærksom på §8 på bagsiden af standkontrakten samt MESSE C's Generelle Regler. Se i <a href="http://">afsnit 6</a>. 
    <br />
    Se desuden punktet ”Spise- og drikkevarer”. <h4>UDSTILLERKORT</h4>
Et udstillerkort er eneste gyldige adgangskort for standpersonale under messen. Udstillerkortet skal være udfyldt for at være gyldigt. 
Udstillerkortet giver ligeledes adgang til Udstillercaféerne.
    <br />
    Se bestillingsskema under <a href="BestillingAdgangskortPR.aspx">afsnit 4</a>. 

    <br />
    <br />
    Der benyttes ikke arbejdskort under opbygning og nedtagning. 

<h4>VAREBILER, TRAILERE OG LASTVOGNE M.V.</h4>
Varebiler, trailere og lastvogne m.v. må kun befinde sig bagved messecentret i opbygnings- og nedtagningsperioden. Disse skal derfor fjernes herfra senest mandag den 24. februar kl. 22.00 og vil først få adgang igen fredag den 28. februar kl. ca. 17.45. 

<h4>ÅBNING AF MESSEN</h4>
Åbningen af messen sker: 

    <br />
    <br />
    <strong>Tirsdag den 25. februar 2014 kl. 8.45 

</strong>
    <br />
    <br />
    ved en højtidelighed i Cafeteriet Panorama på 1. sal i indgangspartiet ved hovedindgangen. Se oversigtsplan i <a href="http://">afsnit 6</a>. 

    <br />
    <br />
    Samtlige udstillere vil blive inviteret til at deltage i åbningen sammen med fremtrædende repræsentanter fra byggebranchen. Invitation vil tilgå direkte. 

<h4>ÅBNINGSTIDER</h4>
    <strong>Opbygning:</strong>
    <br />
12. – 16. februar: kl. 8.00-16.00 - kun efter skriftlig aftale og kun for uopbyggede stande
    <br />
    17. – 23. februar: kl. 8.00 – 17.00 
    <br />
    24. februar: fra kl. 8.00 

    <br />
    <br />
    <strong>NB!</strong> Mandag den 24. februar, sidste opbygningsdag, <strong>skal alle gangarealer være ryddede fra kl. 18.00</strong>, herefter kan der arbejdes videre med lettere montage på standene. 

    <br />
    <br />
    <strong>BYGGERI ´14´s åbningstider er:</strong>
    <br />
Tirsdag den 25. februar til fredag den 28. februar 2014 kl. 9.30 – 17.00. 

    <br />
    <br />
    <strong>Vi gør opmærksom på, at der er adgang for udstillere til udstillingsarealet en time før og efter messens åbningstid. 

    <br />
    </strong>
    <br />
    <strong>Nedtagning:</strong>
    <br />
Fredag den 28. februar kan der efter messens lukning arbejdes til kl. 24.00 
    <br />
    Lørdag den 1. marts og søndag den 2. marts kl. 8.00 – 17.00 
    <br />
    Mandag den 3. marts og tirsdag den 4. marts kl. 8.00 – 21.00 


    </div><br />
    <a href="Default.aspx">Tilbage til forsiden</a></div>
        <div class="footer">
    <div class="footer-container">
        <img style="float:right;" height="106px" width="190px" src="billeder/DB_logo.png" alt="Danske Byggecentre" />
       <p >ARRANGØR:
            <br />DANSKE BYGGECENTRE &middot; EGEBÆKVEJ 98 &middot; 2850 NÆRUM
        <br /> TLF. 45 80 78 77 &middot; FAX 45 80 78 87
        <br /> MAIL: INFO@BYGGERIMESSEN.DK
        </p>
    </div>
        </div>
    </form>
</body>
</html>
